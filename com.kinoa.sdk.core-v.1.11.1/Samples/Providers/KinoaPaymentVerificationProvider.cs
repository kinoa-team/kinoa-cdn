using System.Threading;
using Kinoa.Data.State;
using UnityEngine;

/// <summary>
///     Kinoa payment verification provider sample.
/// </summary>
public class KinoaPaymentVerificationProvider
{
    /// <summary>
    ///     Verifies purchase.
    /// </summary>
    public async void Verify(string orderId, string packageName, string purchaseToken, string productId)
    {
        var purchase = new Purchase(new PaymentReceipt(orderId, packageName, purchaseToken, productId));
        var cts = new CancellationTokenSource();
        cts.CancelAfter(2000);
        var response = await Kinoa.PaymentVerification.VerifyPurchase(purchase, cts.Token);
        if (response.IsSuccessful())
        {
            Debug.Log($"{nameof(KinoaPaymentVerificationProvider)}.{nameof(Verify)} request finished." +
                      $"\nVerified: {response.Data.Result}. {response.Data.Error}");
        }
        else
        {
            Debug.Log($"{nameof(KinoaPaymentVerificationProvider)}.{nameof(Verify)} " +
                      $"request status: {response.Status.ToString()}" +
                      $"\nwith error: {response.Error.Code}");
        }
    }
}