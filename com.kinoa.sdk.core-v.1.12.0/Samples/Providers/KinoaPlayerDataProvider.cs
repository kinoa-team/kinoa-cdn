﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Kinoa.Data;
using Kinoa.Data.State;
using UnityEngine;

/// <summary>
///     Kinoa player data provider sample.
/// </summary>
public class KinoaPlayerDataProvider
{
    /// <summary>
    ///     Gets and sets an active Kinoa player identifier.
    /// </summary>
    public string ActivePlayerID
    {
        get => Kinoa.Player.ID;
        set => Kinoa.Player.ID = value;
    }

    /// <summary>
    ///     The current player state.
    ///     To set current player state on game start <see cref="Kinoa.Player.GetState{T}"/>
    ///     <seealso cref="GetPlayerState"/>.
    /// </summary>
    public static CustomPlayerState PlayerState { get; private set; }

    /// <summary>
    ///     Sets player state changed by operator handler.
    /// </summary>
    public void SetStateChangedByOperatorHandler() =>
        Kinoa.Player.SetStateChangedByOperatorHandler(OnPlayerStateChangedByOperator);

    /// <summary>
    ///     Initialize the new instance of <see cref="KinoaPlayerDataProvider"/>.
    /// </summary>
    public KinoaPlayerDataProvider()
    {
        SetStateChangedByOperatorHandler();
    }

    /// <summary>
    ///     Creates the new Kinoa player using passed identifiers.
    /// </summary>
    public void CreateNewPlayer() => Kinoa.Player.Create(
        new PlayerCreateIDs("mock-native-player-id", "mock-fb-id"),
        OnNewPlayerCreated);

    /// <summary>
    ///     Creates the new Kinoa Tester player using passed identifiers.
    /// </summary>
    public void CreateNewTesterPlayer() => Kinoa.Player.Create(
        new PlayerCreateIDs("tester-native-player-id", "tester-fb-id", true),
        OnNewPlayerCreated);

    /// <summary>
    ///     Gets all player-related accounts.
    /// </summary>
    public void GetRelatedAccounts() => Kinoa.Player.GetRelatedAccounts(
        new PlayerSearchIDs(ActivePlayerID, "mock-native-player-id", "mock-fb-id"),
        OnRelatedAccountsReceived);

    /// <summary>
    ///     Gets the actual player state.
    /// </summary>
    public void GetPlayerState() => Kinoa.Player.GetState<CustomPlayerState>(OnPlayerStateReceived);

    /// <summary>
    ///     Resets the server Player State <seealso cref="Kinoa.Player.ResetState"/> to the passed PS object.
    ///     Accepts merged, changed, or any other updated Player State.
    ///     Updates reached level.
    /// </summary>
    public Task ResetState()
    {
        PlayerState?.SetLevel(0);
        return Kinoa.Player.ResetState(PlayerState);
    }

    /// <summary>
    ///     Approves player state relevance, marks incoming changes as known.
    /// </summary>
    public void ApproveStateChanges() => Kinoa.Player.ApproveStateChanges(OnPlayerStateApproved);

    /// <summary>
    ///     Callback with the new Kinoa player ID handled by an application.
    /// </summary>
    /// <param name="response">Processed API response with the new created Kinoa player ID.</param>
    private void OnNewPlayerCreated(Response<string> response)
    {
        var newPlayerID = response.Data;
        Debug.Log($"The new Kinoa player was created successfully: {response.IsSuccessful()}.\n" +
                  $"The new Kinoa player ID: {newPlayerID}");
        ActivePlayerID = newPlayerID;
    }

    /// <summary>
    ///     On the player-related accounts received handler.
    ///     Use cases: - realize custom player login system and start the game session with the appropriate player;
    ///     - in-app business logic;
    /// </summary>
    /// <param name="response">Processed API response with the all player-related accounts.</param>
    private static void OnRelatedAccountsReceived(Response<List<PlayerRelatedAccount>> response) =>
        Debug.Log($"All player-related accounts received successfully: {response.IsSuccessful()}.\n" +
                  $"Related accounts: {response.Data?.Count}");

    /// <summary>
    ///     On the actual player state received handler.
    /// </summary>
    /// <param name="response">Processed API response with the Player State generic type object.</param>
    private static void OnPlayerStateReceived(Response<CustomPlayerState> response)
    {
        if (response.IsSuccessful())
        {
            PlayerState = response.Data;
            if (PlayerState is CustomPlayerState customPlayerState)
                customPlayerState.SetUnityProperties();
        }

        Debug.Log($"The actual player state received successfully: {response.IsSuccessful()}.\n" +
                  $"Player ID: \"{response.Data?.PlayerIdentifiers.PlayerID}\". " +
                  $"Native ID: \"{response.Data?.PlayerIdentifiers.NativeID}\"." +
                  $"Facebook ID: \"{response.Data?.PlayerIdentifiers.FacebookID}\".");
    }

    /// <summary>
    ///     Triggered on changing player state by an operator.
    ///     You should approve the state changes <see cref="Kinoa.Player.ApproveStateChanges"/> before sending new events.
    /// </summary>
    /// <param name="changed">Is the player state changed.</param>
    private static void OnPlayerStateChangedByOperator(bool changed) =>
        Debug.Log($"The player state changed by an operator: {changed}.");

    /// <summary>
    ///     On the player state, incoming changes approve.
    /// </summary>
    /// <param name="response">Processed API response.</param>
    private static void OnPlayerStateApproved(Response response) =>
        Debug.Log($"The player state changes approved and marked as known: {response.IsSuccessful()}.");
}