﻿using System.Collections.Generic;
using Kinoa.Data.State;
using UnityEngine;
using SystemInfo = UnityEngine.Device.SystemInfo;

/// <summary>
///     The custom Player State <see cref="PlayerState"/> object extended with game-specific properties.
/// </summary>
public class CustomPlayerState : PlayerState
{
    /// <summary>
    ///     The custom Foo property.
    /// </summary>
    public string Foo { get; set; } = "Foo";

    /// <summary>
    ///     The custom Bar property.
    /// </summary>
    public List<string> Bar { get; set; } = new() {"Bar"};

    /// <summary>
    ///     The custom DeviceID property.
    /// </summary>
    public string DeviceID { get; set; }

    /// <summary>
    ///     Sets Unity properties that can only be called from the main thread.
    ///     Kinoa uses a thread pool to get player state asynchronously and avoid game freezing.
    ///     If you inherit from the "PlayerState" class and use Unity-dependent properties e.g. <see cref="DeviceID"/>
    ///     in the constructor or field initializers, please move them to a separate method <see cref="SetUnityProperties"/>
    ///     and call it after the SDK returns the PlayerState object.
    /// </summary>
    public void SetUnityProperties()
    {
        DeviceID = SystemInfo.deviceUniqueIdentifier;
        Debug.Log($"The Unity properties is successfully set in the main thread: {DeviceID}.");
    }
}