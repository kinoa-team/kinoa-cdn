﻿using System.Threading.Tasks;
using Kinoa.Core.Network.Retry;
using Kinoa.Data.Enum;
using Kinoa.Data.Network;

/// <summary>
///     Kinoa SDK basic methods and properties provider sample.
/// </summary>
public class KinoaBaseProvider
{
    /// <summary>
    ///    Kinoa Game unique identifier.
    /// </summary>
    private const string GameID = "TYPE_YOUR_GAME_ID_HERE";

    /// <summary>
    ///    Kinoa game secret token .
    /// </summary>
    private const string GameToken = "TYPE_YOUR_GAME_TOKEN_HERE";

    /// <summary>
    ///     Custom network requests timeout in seconds.
    /// </summary>
    public const int NetworkRequestsTimeout = 30;

    /// <summary>
    ///     Gets the SDK version number.
    /// </summary>
    public static string SDKVersion => Kinoa.SDK.Version;

    /// <summary>
    ///     Initialize and configure the SDK
    /// </summary>
    public async Task Initialize()
    {
        var gameSecrets = new GameSecrets(GameID, GameToken, true);
        var retryConfig = new RetryConfiguration(RetryReason.ConnectionError, RetryStrategy.Exponential, 3);
        var networkConfig = new NetworkConfiguration(NetworkRequestsTimeout, retryConfig);

        Kinoa.SDK.SetLogLevel(LogLevel.Trace);
        await Kinoa.SDK.Initialize(gameSecrets, networkConfig);
        await Kinoa.Messaging.Initialize();
    }
}