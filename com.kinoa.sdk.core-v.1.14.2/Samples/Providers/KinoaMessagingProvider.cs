﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kinoa.Core.Network;
using Kinoa.Data.Enum;
using Kinoa.Data.Messaging.Command;
using Kinoa.Data.Messaging.InApp;
using Kinoa.Data.Messaging.InApp.CreationParams;
using Kinoa.Data.Messaging.InApp.Templates.Custom;
using Kinoa.Data.Messaging.InApp.Templates.Simple;
using Kinoa.Data.ResourceManagement;
using UnityEngine;

/// <summary>
///     Kinoa messaging provider sample.
/// </summary>
public class KinoaMessagingProvider
{
    /// <summary>
    ///     Local inbox storage.
    /// </summary>
    public List<InAppMessage> LocalInboxStorage { get; set; } = new List<InAppMessage>();

    /// <summary>
    ///     Initialize the new instance of <see cref="KinoaMessagingProvider"/>
    /// </summary>
    public KinoaMessagingProvider()
    {
        SetInAppCreationRequestedHandler()
    }
	
	/// <summary>
    ///     Sets InAppCreationRequested handler.
    /// </summary>
    public void SetInAppCreationRequestedHandler()
	{
		Kinoa.Messaging.InAppCreationRequested += OnInAppCreationRequested;
	}

    /// <summary>
    ///     Event handler of in-app creation requests.
    /// </summary>
    private async void OnInAppCreationRequested(InAppCreationParams inAppCreationParams)
    {
        switch (inAppCreationParams)
        {
            case InAppByPushCreationParams inAppByPushCreationParams:
            {
                await CreateInAppMessageAsync(inAppByPushCreationParams);
                break;
            }
            default:
                Log($"Skipping InApp creation request with unknown {nameof(InAppCreationParams)} type.");
                break;
        }
    }

    /// <summary>
    ///     Sets application handled messaging callbacks.
    /// </summary>
    public void SetMessagingHandler() => Kinoa.Messaging.SetMessageHandlers(OnCommandReceived, OnInnAppReceived);

    /// <summary>
    ///     On command message received.
    /// </summary>
    /// <param name="message">Command message.</param>
    private static void OnCommandReceived(CommandMessage message)
    {
        switch (message)
        {
            case NewResourceCommand cmd:
                Log($"Command type of {nameof(NewResourceCommand)} is received:" +
                    $"\nInbox ID: '{cmd.Id}', Message UUID:'{cmd.Uuid}'");
                break;
            case ReloadP2PCommand cmd:
                Log($"Command type of {nameof(ReloadP2PCommand)} is received:" +
                    $"\nMessage UUID:'{cmd.Uuid}'");
                break;
            case UpdateEconomyCommand cmd:
                Log($"Command type of {nameof(UpdateEconomyCommand)} is received:" +
                    $"\nMessage UUID:'{cmd.Uuid}'");
                break;
            case PlayerStateChangedByOperatorCommand cmd:
                Log($"Command type of {nameof(PlayerStateChangedByOperatorCommand)} is received:" +
                    $"\nMessage UUID:'{cmd.Uuid}'");
                break;
            case RemovedInboxInAppsCommand cmd:
                Log($"Command type of {nameof(RemovedInboxInAppsCommand)} is received:" +
                    $"\n{string.Join(",\n", cmd.InApps.Select(o => $"Inbox UUID: {o.Uuid.ToString()}").ToArray())}'");
                break;
            default:
                Log($"Message type of {message.Type} is received: {message.Action}." +
                    $"\nID: {message.Uuid}");
                break;
        }
    }

    /// <summary>
    ///     Handles the received In-app message and logs the first message in the received messages collection.
    /// </summary>
    /// <param name="messages">In-app messages.</param>
    private void OnInnAppReceived(InAppMessages messages)
    {
        Log($"Messages type of {messages.Type} are received ({messages.Data?.Count}):\n" +
            $"{(messages.Data?.Count > 0 ? string.Join(",\n", messages.Data.Select(o => $"{o.Uuid}").ToArray()) : string.Empty)}.",
            messages);

        if (messages.Data == null) return;

        //The In-app message properties access on the example of logging.
        foreach (var inApp in messages.Data)
            Log(inApp);
    }

    /// <summary>
    ///     Gets the list of all inbox messages async.
    /// </summary>
    public async void GetInboxMessagesAsync()
    {
        var logBuilder = new StringBuilder();
        var response = await Kinoa.Messaging.GetInboxMessagesAsync();
        if (response.Status == ResponseState.Success && response.Data != null)
        {
            var simpleInAppMessages =
                response.Data.Where(x => x.Data.Template == InAppDataTemplate.Simple).ToList();
            var customInAppMessages =
                response.Data.Where(x => x.Data.Template == InAppDataTemplate.Custom).ToList();

            logBuilder.Append($"Inbox messages received successfully: ({response.Data.Count})." +
                              $"\nSimple In-app messages ({simpleInAppMessages.Count})" +
                              $"\nCustom In-app messages ({customInAppMessages.Count})");

            LocalInboxStorage = response.Data;
        }
        else logBuilder.Append($"Get inbox messages request status: {response.Status.ToString()}.");

        Log(logBuilder.ToString());
    }

    /// <summary>
    ///     Updates inbox In-app message async.
    /// </summary>
    /// <param name="message">Inbox In-app message.</param>
    /// <returns>Processed API response.</returns>
    public async void UpdateInboxMessageAsync(InAppMessage message)
    {
        var customParams = new Dictionary<string, object>
        {
            ["custom_key_1"] = 1.0,
            ["custom_key_2"] = "custom_value_2",
            ["custom_key_3"] = true
        };
        message
            .SetCustomParameters(customParams)
            .SetViewsMetrics(message.InboxStats.Views + 1)
            .SetUsageMetrics(message.InboxStats.Usage + 1)
            .SetEligibility(2);

        var response = await Kinoa.Messaging.UpdateInboxMessageAsync(message);
        Log($"{nameof(UpdateInboxMessageAsync)} request status: {response.Status.ToString()}.");
    }

    /// <summary>
    ///     Updates multiple inbox In-app messages async.
    /// </summary>
    /// <param name="messages">Multiple inbox In-app messages.</param>
    /// <returns>Processed API response.</returns>
    public async void UpdateInboxMessagesAsync(List<InAppMessage> messages)
    {
        var customParams = new Dictionary<string, object>
        {
            ["ID"] = 1.0,
            ["custom_key_2"] = "custom_value_2",
            ["camelCase"] = true
        };
        foreach (var message in messages)
        {
            message
                .SetCustomParameters(customParams)
                .SetViewsMetrics(message.InboxStats.Views + 1)
                .SetUsageMetrics(message.InboxStats.Usage + 1)
                .SetEligibility(2);
        }

        var response = await Kinoa.Messaging.UpdateInboxMessagesAsync(messages);
        Log($"{nameof(UpdateInboxMessagesAsync)} request status: {response.Status.ToString()}.");
    }

    /// <summary>
    ///     Deletes all inbox messages.
    /// </summary>
    public async void DeleteAllInboxMessagesAsync()
    {
        var response = await Kinoa.Messaging.DeleteAllInboxMessagesAsync();
        Log($"{nameof(DeleteAllInboxMessagesAsync)} request status: {response.Status.ToString()}.");
    }

    /// <summary>
    ///     Deletes inbox message by uuid.
    /// </summary>
    /// <param name="message">Inbox message.</param>
    public async void DeleteInboxMessageAsync(InAppMessage message)
    {
        var response = await Kinoa.Messaging.DeleteInboxMessageAsync(message);
        Log($"{nameof(DeleteInboxMessageAsync)} request status: {response.Status.ToString()}.");
    }

    /// <summary>
    ///     Deletes multiple inbox In-app messages async.
    /// </summary>
    /// <param name="messages">The In-app messages.</param>
    /// <returns>Processed API response.</returns>
    public async void DeleteInboxMessagesAsync(IEnumerable<InAppMessage> messages)
    {
        var response = await Kinoa.Messaging.DeleteInboxMessagesAsync(messages);
        Log($"{nameof(DeleteInboxMessagesAsync)} request status: {response.Status.ToString()}.");
    }

    /// <summary>
    ///     Creates new In-app message by External Link.
    /// </summary>
    /// <param name="externalLink">Unique In-app message external link.</param>
    public async void CreateInAppMessageAsync(string externalLink)
    {
        var response = await Kinoa.Messaging.CreateInAppMessageAsync(externalLink);
        if (response.IsSuccessful())
            Log($"The In-app message was successfully generated by External Link: {externalLink}." +
                $"\nMessage ID: {response.Data.Uuid}");
        else
            Log($"{nameof(CreateInAppMessageAsync)} request status: {response.Status.ToString()}." +
                $"\nError code: {(response.Error != null ? response.Error.Code : "Error object is null.")}");
    }

    /// <summary>
    ///     Creates new In-app message by Push Notification.
    /// </summary>
    /// <param name="inAppCreationParams">The In-app message creation parameters.</param>
    public async Task CreateInAppMessageAsync(InAppByPushCreationParams inAppCreationParams)
    {
        var response = await Kinoa.Messaging.CreateInAppMessageAsync(inAppCreationParams);
        if (response.IsSuccessful())
            Log($"The In-app message '{inAppCreationParams.ID}' " +
                "was successfully generated by Push Notification." +
                $"\nMessage ID: {response.Data.Uuid}");
        else
            Log($"{nameof(CreateInAppMessageAsync)} request status: {response.Status.ToString()}." +
                $"\nError code: {(response.Error != null ? response.Error.Code : "Error object is null.")}");
    }

    /// <summary>
    ///     Shows inbox messages on UI (for demonstration purposes only).
    ///     Use your custom UI interface and client notification mechanism.
    /// </summary>
    private static void Log(string message, InAppMessages inApps = null)
    {
        Debug.Log(message);
        DialogController.Instance.Log(inApps, message);
    }

    /// <summary>
    ///     Debug logs string builder.
    /// </summary>
    private readonly StringBuilder log = new StringBuilder();

    /// <summary>
    ///     Logs In-app message properties.
    /// </summary>
    /// <param name="inApp">The In-app message.</param>
    private void Log(InAppMessage inApp)
    {
        log.AppendFormat("In-app message properties access:");
        log.AppendFormat($"\n\nUuid: {inApp.Uuid}");
        log.AppendFormat($"\nMessageId: {inApp.MessageId}");
        log.AppendFormat($"\nName: {inApp.Name}");
        log.AppendFormat($"\nOrder: {inApp.Order}");
        log.AppendFormat($"\nSentTime: {inApp.SentTime}");
        log.AppendFormat($"\nIsInboxMessage: {inApp.IsInboxMessage}");
        log.AppendFormat($"\nIsTriggeredOffline: {inApp.IsTriggeredOffline}");

        Log(inApp.Command, inApp.Uuid);
        Log(inApp.Data);

        if (inApp.LobbyIcon != null)
        {
            log.AppendFormat("\n\nLobbyIcon: ");
            Log(inApp.LobbyIcon);
            log.AppendFormat($"\n\tPlacement: {Convert.ToString(inApp.LobbyIcon.Placement)}");
            log.AppendFormat($"\n\tIsInAppTrigger: {inApp.LobbyIcon.IsInAppTrigger.ToString()}");
            log.AppendFormat($"\n\tText: {inApp.LobbyIcon.Text}");
        }

        if (inApp.CountdownTimer != null)
        {
            log.AppendFormat("\n\nCountdownTimer: ");
            log.AppendFormat($"\n\tEndTimestamp: {inApp.CountdownTimer.EndTimestamp}");
            log.AppendFormat($"\n\tIsVisible: {inApp.CountdownTimer.IsVisible.ToString()}");
        }

        if (inApp.Capping != null)
        {
            log.AppendFormat($"\n\nCapping.TotalLimit: {inApp.Capping.TotalLimit}");
            if (inApp.Capping.EligibilityLimit != null)
            {
                log.AppendFormat("\nCapping.EligibilityLimit: ");
                log.AppendFormat(
                    $"\n\tOriginal: {inApp.Capping.EligibilityLimit.Original}" +
                    $"\n\tActual: {inApp.Capping.EligibilityLimit.Actual}" +
                    $"\n\tIsEligibilityUsed: {inApp.Capping.EligibilityLimit.IsEligibilityUsed}");
            }

            if (inApp.Capping.RecurrentLimit != null)
            {
                log.AppendFormat("\nCapping.RecurrentLimit: ");
                log.AppendFormat(
                    $"\n\tAmount: {inApp.Capping.RecurrentLimit.Amount}" +
                    $"\n\tPeriod: {inApp.Capping.RecurrentLimit.Period}");
            }

            if (inApp.Capping.Cooldown != null)
            {
                log.AppendFormat("\nCapping.CoolDown: ");
                log.AppendFormat(
                    $"\n\tPeriod: {inApp.Capping.Cooldown.Period}");
            }
        }

        if (inApp.InboxStats != null)
        {
            log.AppendFormat($"\n\nInboxStats: ");
            log.AppendFormat($"\n\tViews: {inApp.InboxStats.Views}");
            log.AppendFormat($"\n\tUsage: {inApp.InboxStats.Usage}");
        }

        if (inApp.Extra != null)
        {
            log.AppendFormat("\n\nExtra: ");
            log.AppendFormat(
                string.Join(",\n\t", inApp.Extra.Select(o => $"{o.Name}: {o.Value}").ToArray()));
        }

        if (inApp.CustomParams != null)
        {
            log.AppendFormat("\n\nCustomParams: ");
            log.AppendFormat(
                string.Join(",\n\t", inApp.CustomParams.Select(o => $"{o.Key}: {o.Value}").ToArray()));
        }

        log.AppendFormat("\n");
        Debug.Log(log.ToString());
        log.Clear();
    }

    /// <summary>
    ///     Logs the command for the game to execute after the current In-app is received.
    /// </summary>
    /// <param name="inAppCommand">The In-app command.</param>
    /// <param name="uuid">The In-app inbox uuid.</param>
    private void Log(InAppCommand inAppCommand, string uuid)
    {
        if (inAppCommand == null) return;

        log.AppendFormat("\n\nCommand: ");
        switch (inAppCommand)
        {
            case InAppReplacedCommand cmd:
                log.AppendFormat($"\n\tAction: {inAppCommand.Action.ToString()}" +
                                 $"\n\tReplaced UUID: '{cmd.ReplacedUuid}', Message UUID:'{uuid}'");
                break;
            case InAppReminderCommand:
                log.AppendFormat($"\n\tAction: {inAppCommand.Action.ToString()}" +
                                 $"\n\tThe reminder that an In-app message with UUID: '{uuid}' is waiting for you in the inbox.");
                break;
            default:
                log.AppendFormat(
                    $"\n\tCommand with {inAppCommand.Action.ToString()} action cannot be processed.");
                break;
        }
    }

    /// <summary>
    ///     Logs the In-app message template data.
    /// </summary>
    /// <param name="inAppData">The In-app message template data.</param>
    private void Log(InAppMessageData inAppData)
    {
        if (inAppData == null) return;

        log.AppendFormat("\n\nData: ");
        log.AppendFormat($"\n\tTemplate: {inAppData.Template.ToString()}");
        switch (inAppData)
        {
            case InAppSimpleTemplateData data:
                Log(data);
                break;
            case InAppCustomTemplateData data:
                Log(data);
                break;
            default:
                log.AppendFormat(
                    $"\n\tData with {inAppData.Template.ToString()} template cannot be processed.");
                break;
        }
    }

    /// <summary>
    ///     Logs the In-app message data type of Simple template <see cref="InAppDataTemplate.Simple"/>.
    /// </summary>
    /// <param name="inAppData">The In-app message template data.</param>
    private void Log(InAppSimpleTemplateData inAppData)
    {
        if (inAppData.MainPortraitImage != null)
        {
            log.AppendFormat("\n\n\tMainPortraitImage: ");
            Log(inAppData.MainPortraitImage);
        }

        if (inAppData.MainLandscapeImage != null)
        {
            log.AppendFormat("\n\n\tMainLandscapeImage: ");
            Log(inAppData.MainLandscapeImage);
        }

        if (inAppData.ClickConfig != null)
        {
            log.AppendFormat("\n\n\tClickConfig: ");
            Log(inAppData.ClickConfig);
        }

        if (inAppData.Resources != null)
        {
            log.AppendFormat("\n\n\tResources: ");
            Log(inAppData.Resources);
        }

        if (inAppData.Packages != null)
        {
            log.AppendFormat("\n\n\tPackages: ");
            Log(inAppData.Packages);
        }
    }

    /// <summary>
    ///     Logs the In-app message data type of Custom template <see cref="InAppDataTemplate.Custom"/>.
    /// </summary>
    /// <param name="inAppData">The In-app message template data.</param>
    private void Log(InAppCustomTemplateData inAppData)
    {
        log.AppendFormat($"\n\tTemplateKey: {inAppData.TemplateKey}");
        if (inAppData.Buttons != null)
        {
            log.AppendFormat("\n\n\tButtons: ");
            Log(inAppData.Buttons);
        }

        if (inAppData.Images != null)
        {
            log.AppendFormat("\n\n\tImages: ");
            Log(inAppData.Images);
        }

        if (inAppData.Texts != null)
        {
            log.AppendFormat("\n\n\tTexts: ");
            Log(inAppData.Texts);
        }
    }

    /// <summary>
    ///     Logs the collection of In-app attached resources.
    /// </summary>
    /// <param name="resources">The resources collection.</param>
    private void Log(IEnumerable<Resource> resources)
    {
        foreach (var resource in resources)
        {
            log.AppendFormat($"\n\tResourceKey: {resource.ResourceKey}");
            log.AppendFormat($"\n\tAmount: {resource.Amount}");
            log.AppendFormat($"\n\tExpiration: {resource.Expiration}");
            log.AppendFormat("\n\tBody: {0}", resource.Body);
            log.AppendFormat("\n");
        }
    }

    /// <summary>
    ///     Logs the collection In-app message buttons. Key - button key, Value - button data.
    /// </summary>
    /// <param name="inAppButtons">The buttons collection.</param>
    private void Log(Dictionary<string, InAppMessageButton> inAppButtons)
    {
        foreach (var (key, value) in inAppButtons)
        {
            log.AppendFormat($"\n\tKey: {key}");
            log.AppendFormat("\n\tLabel: {0}", value.Label);

            if (value.BackgroundImage != null)
            {
                log.AppendFormat("\n\tBackgroundImage: ");
                Log(value.BackgroundImage);
            }

            if (value.ClickConfig != null)
            {
                log.AppendFormat("\n\tClickConfig: ");
                Log(value.ClickConfig);
            }

            if (value.Resources != null)
            {
                log.AppendFormat("\n\tResources: ");
                Log(value.Resources);
            }

            if (value.Packages != null)
            {
                log.AppendFormat("\n\tPackages: ");
                Log(value.Packages);
            }

            log.AppendFormat("\n");
        }
    }

    /// <summary>
    ///     Logs the collection In-app message images. Key - image key, Value - image data.
    /// </summary>
    /// <param name="inAppImages">The images collection.</param>
    private void Log(Dictionary<string, InAppImage> inAppImages)
    {
        foreach (var image in inAppImages)
        {
            log.AppendFormat($"\n\tKey: {image.Key}");
            Log(image.Value);
            log.AppendFormat("\n");
        }
    }

    /// <summary>
    ///     Logs the collection of In-app message text objects. Key - text object key, Value - text object data.
    /// </summary>
    /// <param name="inAppTexts">The texts collection.</param>
    private void Log(Dictionary<string, InAppContent> inAppTexts)
    {
        foreach (var (key, value) in inAppTexts)
        {
            log.AppendFormat($"\n\tKey: {key}");
            log.AppendFormat("\n\tContent: {0}", value.Content);
            log.AppendFormat("\n");
        }
    }

    /// <summary>
    ///     Logs the In-app message image.
    /// </summary>
    /// <param name="inAppImage">The In-app message image.</param>
    private void Log(InAppImage inAppImage)
    {
        log.AppendFormat("\n\tContent: {0}", inAppImage.Content);
        log.AppendFormat($"\n\tContentType: {inAppImage.ContentType}");
    }

    /// <summary>
    ///     Logs the collection of In-app attached store packages.
    /// </summary>
    /// <param name="inAppStorePackages">The store packages.</param>
    private void Log(InAppStorePackages inAppStorePackages)
    {
        log.AppendFormat($"\n\tAndroidPackageID: {inAppStorePackages.AndroidPackageID}");
        log.AppendFormat($"\n\tAndroidDiscountPackageID: {inAppStorePackages.AndroidDiscountPackageID}");
        log.AppendFormat($"\n\tIosDiscountPackageID: {inAppStorePackages.IosDiscountPackageID}");
        log.AppendFormat($"\n\tIosPackageID: {inAppStorePackages.IosPackageID}");
    }

    /// <summary>
    ///     Logs the In-app click action.
    /// </summary>
    /// <param name="inAppClickConfig">The In-app click configuration.</param>
    private void Log(InAppClickConfiguration inAppClickConfig)
    {
        switch (inAppClickConfig)
        {
            case InAppLinkClickConfiguration linkClickConfig:
            {
                switch (linkClickConfig.Action)
                {
                    case InAppClickAction.DeepLink:
                    case InAppClickAction.WebLink:
                        log.AppendFormat($"\n\tAction: {linkClickConfig.Action.ToString()}");
                        log.AppendFormat($"\n\tLink: {linkClickConfig.Link}");
                        break;
                    default:
                        Debug.Log($"The '{linkClickConfig.Link}' cannot be opened.");
                        break;
                }

                break;
            }
            case { } clickConfig:
            {
                switch (clickConfig.Action)
                {
                    case InAppClickAction.Close:
                    case InAppClickAction.CollectResource:
                    case InAppClickAction.Billing:
                    case InAppClickAction.ShowAd:
                    case InAppClickAction.PromiseRewards:
                    case InAppClickAction.UpdateAppVersion:
                        log.AppendFormat($"\n\tAction: {clickConfig.Action.ToString()}");
                        break;
                    default:
                        Debug.Log($"The In-app {clickConfig.Action.ToString()} click action cannot be invoked.");
                        break;
                }

                break;
            }
        }
    }
}