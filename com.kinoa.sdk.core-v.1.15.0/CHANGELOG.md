# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/), and this project adheres
to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.15.0]

- Get Player State by any Player ID
- A CalculatedFields property inside the PlayerState object - calculated fields from your Google Bucket file
- Using of SocialConnectEventData, SocialDisconnectEventData, and SocialPostEventData instead of SocialEventData (see samples)
- Logging the response data inside the error event on throwing the ParseException
- Logging of requests that take more than 1 second

## [1.14.0]

### Added
- A Tick event description
- A Tick event configuration (see also Initialize SDK method)
- A possibility to enable/disable the Personally Identifiable Information (PII) usage by the SDK (see also Initialize SDK method)
- Player State property - Is a blocked Player / SetIsBlocked Player State method (see OnCheatingEvent Integration samples)
- Player State property - Is a cheater Player / SetIsCheater Player State method (see OnCheatingEvent Integration samples)
- The In-app message lobby icon text

### Changed
- GameSecrets constructor is extended with the AllowPii flag
- Kinoa.SDK.Initialize method is extended with the TickEventsConfiguration
- UnityEngine.SystemInfo.deviceUniqueIdentifier  is used by the SDK automatically only if the AllowPii is enabled
- Secured WebSocket connection with a Game-Auth header

## [1.13.0]

### Added
- Mobile Traffic decreased by the Player State Diff
- JsonDiffPatch.Net NuGet package (see Dependencies)
- Additional Player identifiers (GoogleId, AppleId, ExtraIds) on:
	- New Player Creation
	- New Test Player Creation
	- Getting Player accounts
	- Getting Player State
	- Player State updating (see OnSocialConnect and OnSocialDisconnect integration samples)

- Getting/Setting the localization language of the active Player

### Changed
- Updated minimum Unity version from 2020.2 to 2021.2 to support .netstandard2.1

## [1.12.0]

### Added
- In-app message from Push

## [1.11.0]

### Added

- Settings Service
- Fixed performance issues

## [1.10.0] 

### Added

- WebGL build support
- Game-Auth security layer for the Web API requests
- In-app messages ver. 2
- The In-app integration samples
- The global network configuration of SDK requests
- The network configuration of the session start request
- A WebSocket command to indicate the removal of In-app messages from the inbox
- A sample of initializing the Player State properties that can only be called from the main thread

### Changed

- In-app message structure to the ver. 2
- Single client handler for both optional and mandatory In-app messages
- Asynchronous Kinoa.GameEvents.SendStartSessionEvent method
- Asynchronous Kinoa.Player.ResetState method

### Removed

- NewInboxMessageCommand and NewInboxMessagesCommand WebSocket Commands.


## [1.9.0] 

### Added

- Kinoa.FeaturesSettings.NotifyWhenChecksumChanged method
- In-app message fields:
	- The “Trigger In-app only by Lobby Icon” field (ShowOnTrigger)
	- The UUID of the replaced In-app message (ReplacedUuid)
	- The placement number/ID of the lobby icon (LobbyIconPlacement)
	- Button fields in the custom template:
		- The background image type (Kind)
		- The background image link (BackgroundImage)

### Changed

- Fixed incorrect deserialization of a string property that contains a DateTime value

## [1.8.2] 

### Added

- Payment verification error codes
- In-app message fields:
	- The In-app message name (Name)
	- The timestamp when the In-app message was sent (SentTime)
	- The In-app message eligibility:
		- The original eligibility limit (Capping.EligibilityLimit)
		- The actual eligibility limit (Capping.ActualEligibility)
		- Is the ActualEligibility limit equal to 0 (Capping.IsEligibilityUsed)
	- The In-app message inbox statistics metrics:
		- The inbox message views counter (InboxStats.Views)
		- The inbox message usage counter (InboxStats.Usage)

- In-app message update methods:
	- Sets the actual eligibility limit (SetEligibility)
	- Sets the number of times the inbox message has been viewed (SetViewsMetrics)
	- Sets the number of times an inbox message is used (SetUsageMetrics)
	- Sets In-app message client custom parameters (SetCustomParameters)
	- Adds In-app message custom parameters (AddCustomParameters)

-Multiple In-app messages delete method

### Removed

- Kinoa.GameEvents.SendInAppEligibilityEvent method

## [1.8.1]

### Changed
- Asynchronous WebSocket messaging initialization
- PaymentEventData singature

## [1.8.0] 

### Added

- Get Resources from Bundles by bundle key
- In-App message countdown timer hide option
- Enum of In-app message image types

### Changed

- The C# version increased to 8.0. Minimal Unity version: 2020.2
- Asynchronous read/write operations of local files: Features Settings, Game Events storage, P2P Events storage, Players storage
- Asynchronous read/write operations of built-in Streaming Assets
- Asynchronous REST API calls pre-processing and post-processing
- Asynchronous JSON serialization and deserialization
- Asynchronous GZip compression and decompression
- Unblocked the main UI thread during SDK services use
- Other performance improvements

## [1.7.4] - 2023-04-01

### Changed

- Async read/write of local storages
- Async SDK initialization


## [1.7.3] - 2022-08-12

### Added

- Kinoa.FeaturesSettings.GetLastCachedOrBuiltIn method
- Kinoa.FeaturesSettings.GetBuiltIn method
- Kinoa.FeaturesSettings.GetCached method
- In-App message Eligibility event

### Changed

- Extended Kinoa.GameEvents.OnGameSessionStarted callback
- Extended Kinoa.GameEvents.OnGameSessionStartFailed callback
- How to process the WebSocket Command message

- Updated In-app events integration samples:
	- In-app message click
	- In-app message close
	- In-app message impression

- Bug fixes and other improvements:
	- Fixed outdated cache issue
	- More informative logs for the events

## [1.7.2] - 2022-22-11

### Added

- Configurable network requests timeout
- Features Settings download cancellation token
- In-app messages creation by External Link
- PromiseRewards action on In-app message click
- UpdateAppVersion action on In-app message click
- PlayerStateChangedByOperator WebSocket command
- A field indicating the In-app message is triggered on an offline event

### Changed

- SDK initialize method
- Extended callbacks of Features Settings methods 
	- Download Features Settings

- Extended callbacks of P2P events methods 
	- Get P2P Event
	- Delete P2P Event

- Extended callbacks of Player methods 
	- Create new Player
	- Get Player accounts
	- Get Player State
	- Approve state changes

- Bug fixes
	- Game callbacks invocation when the game session is closed
	- Custom Player State deserialization issues
	- Enums deserialization issues

## [1.7.1.0] - 2022-09-11

### Added

- Beta: Purchase validation for iOS and Android.
- In-app generation by token.
- Bad response error codes.

### Changed

- Resource management methods signatures.
- In-app messaging methods signatures.

## [1.7.0.0] - 2022-01-11

### Added

- Resource Management.
- Game session implementation.
- In-app messages support (Simple / Custom templates).
- Get inbox messages.
- New inbox message web socket command.
- Delete inbox message by ID.
- Delete all inbox messages.
- In-app click event.
- In-app impression event.
- Create new Tester Player.

### Changed

- Bug fixes, refactoring, and other improvements.

## [1.6.0.0] - 2022-09-14

### Added

- GameID support.
- Game-Auth security layer.
- Async UnityWebRequest client.
- Unstable Internet connection handling. Retries implementation.
- Get Feature Settings directly from cache.

### Changed

- Global SDK project structure updates.
- Bug fixes, refactoring, and other improvements.

## [1.5.5.0] - 2022-06-22

### Added

- Logging severity levels.
- Predefined state fields:
  - The "PlayerState.SessionData" field includes important session data.
  - The "PlayerState.Devices" field includes player devices data.
  - Other important state fields.
  
- Get/Set player country method (Countries enum).
- Get/Set player language method (Languages enum).

### Changed

- Built-in features settings mobile platforms fix.
- The "PlayerState.TimeZone" updates.
- The "GameEventData" fields updates.
- Reset Player State business logic updates.
- Bug fixes, refactoring, and other improvements.

## [1.5.0] - 2022-05-24

### Added

- In-app messages.
- In-app close event.
- Get Features Settings by key.

### Changed

- Bug fixes, refactoring, and other improvements.

## [1.4.5] - 2022-04-19

### Added

- Get player state request.
- Update player state request.
- Create the new player request.
- Compatibility with the player state derided types.
- An ActivePlayerID setter moved on the game side.
- Get all player-related accounts request.
- Approve player state changes request.
- Handling player state-changing by an operator.
- Country and language code support by the SetPlayerInfo player state extension method.
- P2P events:
  - Get P2P events request.
  - Send P2P events request.
  - Delete P2P events request.
- Messaging & WebSocket protocol support:
  - Command messages.

### Changed

- Bug fixes, refactoring, and other improvements.

## [1.4.0] - 2022-02-18

### Added

- An "Error" event:
    - SDK internal critical errors reporting.
    - Client-side errors reporting.

### Changed

- Transition to the UUID's identifiers.
- Transition to the Single-economy (features settings).
- Transition from "Economy" to "Features Settings" component:
    - Updated namespaces and signatures: "Kinoa.Economy" to "Kinoa.FeaturesSettings"
    - Updated built-in features settings file path

- Third-party dependencies:
    - Used Unity compatible newtonsoft.json package: "jillejr.newtonsoft.json-for-unity" to "
      com.unity.nuget.newtonsoft-json"

- Bug fixes and other improvements.

## [1.3.5] - 2022-01-14

### Added

- Compressed events data-transfer.
- The updated economies compression algorithm.

- The new events signatures.
- Billing "platform" field setter.
- "Install" event automatic raising on the first game launch.

- "KinoaBaseProvider" - SDK basic methods and properties provider sample.
- "KinoaEventsProvider" - SDK events provider sample.

### Changed

- SDK configuration is moved from "KinoaAnalyticsProvider" to the new "KinoBaseProvider" component.
- Events are moved from "KinoaAnalyticsProvider" to the new "KinoaEventsProvider" component.
- Updated SDK namespaces:
    - "Kinoa.Analytics" was renamed to "Kinoa.Events".
    - "Kinoa.SDK" - contains SDK basic methods and properties.

- Bug fixes and other minor improvements.

### Removed

- "Install" event manual invocation possibility.
- "KinoaAnalyticsProvider" was removed.

## [1.3.0] - 2021-11-08

### Added

- One-click integration – just import the Kinoa samples from Unity PM to your game and that's it.

- Kinoa Offline gaming:
    - Offline and built-in economies are available now. See the documentation for more
      details: "https://docs.google.com/document/d/10no1lT7n-3IG_IRFoeF8C-6-XYQ0sOtJ/edit#heading=h.9cy9xioexqo4"
    - Offline local events storage were implemented.

- Сommon SDK files storage mechanism developed.
- Multiple players events storage supporting. Sending, failed, and actual events storage for each player.
- Events processing behaviour was changed.

- "Kinoa.Analytics.SetPlayerChangedHandler" method added – returns an active player identifier when it was changed. Use
  cases:
    - Download Economy for the new Player
    - In-app business logic on Active Player changed

- "start_session" / "install" events were updated:
    - SetSocialNetworks possibility added
    - Device information was extended with the new extra fields:
      "time_zone", "device_model", "screen_resolution", "screen_dpi", "locale"

- "social_connect" / "social_disconnect" events were updated:
    - Player state – progress and player balance is required now

### Changed

- Kinoa CDN is moved to the Kinoa repositories. The new Kinoa SDK package URL:
  "https://bitbucket.org/kinoa-team/kinoa-cdn/src/master/kinoa-sdk-v.1.3.0"

- "Kinoa.Economy.Checksum" / "Kinoa.Economy.Download" signature updates

- Bug fixes and other minor improvements.

### Removed

- "start_session" event – application handled callback was removed

## [1.2.1] - 2021-09-30

### Added

- Social disconnect event. Social connect and disconnect states added.

### Changed

- Event data custom parameters for all events.
- Bug fixes and other minor improvements.

### Removed

- ThirdPartyServices field from PlayerState.

## [1.2.0] - 2021-08-30

### Added

- Multiple economies caching system.
- Economies filters: by name, by category.
- SDK testing solution.
- Bug fixes.

## [1.1.0] - 2021-08-09

### Added

- Economies checksum request.
- Economies downloading request.
- Economies caching system.
- Compressed economies files implementation (20kB instead of 7mB).
- Bug fixes.

## [1.0.0] - 2021-07-30

### Added

- The first version of SDK developed.
- All game events are sent and processed via the API and Kinoa SDK for Unity.
- Event data aggregated by ClickHouse.
- Kinoa API analyzes data and calculates the audiences dynamically.

### Changed

- SDK is moved to a separate repository to provide the possibility to instal the SDK through the Unity Package Manager.

### Removed

- SDK is removed from C&F repository.

### Fixed

- All the major and minor bugs were fixed.