﻿using System.Collections.Generic;
using System.Threading;
using Kinoa.Data;
using Kinoa.Data.FeaturesSettings;
using UnityEngine;

/// <summary>
///     Kinoa Features Settings provider sample.
/// </summary>
public class KinoaFeaturesSettingsProvider
{
    /// <summary>
    ///     Initializes the new instance of <see cref="KinoaFeaturesSettingsProvider"/>
    ///     and subscribes to the SDK events.
    /// </summary>
    public KinoaFeaturesSettingsProvider()
    {
        Kinoa.FeaturesSettings.OnChecksumChanged += OnChecksumChanged;
    }

    /// <summary>
    ///     Gets the newest Features Settings checksum.
    /// </summary>
    public void Checksum() => Kinoa.FeaturesSettings.GetChecksum(OnChecksumReceived);

    /// <summary>
    ///     Gets cached or built-in player Features Settings file by the newest timestamp.
    /// </summary>
    /// <param name="keys">Filters Features Settings by keys.</param>
    /// <returns>Cached or built-in Features Settings.</returns>
    public async void GetLastCachedOrBuiltIn(IEnumerable<string> keys = null)
    {
        var response = await Kinoa.FeaturesSettings.GetLastCachedOrBuiltIn(keys);
        Debug.Log($"Last cached or built-in Features Settings loaded successfully: {response.IsSuccessful()}." +
                  $"\nChecksum: {response.Data?.Checksum}");
    }

    /// <summary>
    ///     Gets built-in player Features Settings file.
    /// </summary>
    /// <param name="keys">Filters Features Settings by keys.</param>
    /// <returns>Built-in Feature Settings file.</returns>
    public async void GetBuiltIn(IEnumerable<string> keys = null)
    {
        var response = await Kinoa.FeaturesSettings.GetBuiltIn(keys);
        Debug.Log($"Built-in Features Settings loaded successfully: {response.IsSuccessful()}." +
                  $"\nChecksum: {response.Data?.Checksum}");
    }

    /// <summary>
    ///     Gets cached player Features Settings file.
    /// </summary>
    /// <param name="keys">Filters Features Settings by keys.</param>
    /// <returns>Cached Feature Settings file.</returns>
    public async void GetCached(IEnumerable<string> keys = null)
    {
        var response = await Kinoa.FeaturesSettings.GetCached(keys);
        Debug.Log($"Cached Features Settings loaded successfully: {response.IsSuccessful()}." +
                  $"\nChecksum: {response.Data?.Checksum}");
    }

    /// <summary>
    ///     Gets the newest available Features Settings file.
    /// </summary>
    public void Download()
    {
        var cts = new CancellationTokenSource();
        cts.CancelAfter(10000);
        Kinoa.FeaturesSettings.Download(OnFeaturesSettingsDownloaded, OnProgressChanged, null, cts.Token);
    }

    /// <summary>
    ///     Gets the newest available Features Settings file by keys.
    /// </summary>
    public void Download(IEnumerable<string> keys)
    {
        var cts = new CancellationTokenSource();
        cts.CancelAfter(10000);
        Kinoa.FeaturesSettings.Download(response => OnFeaturesSettingsDownloaded(response, keys),
            OnProgressChanged, keys, cts.Token);
    }

    /// <summary>
    ///     Notifies when the checksum of Features Settings file is changed on the server
    ///     and the new file is ready for download.
    ///
    ///     Subscribe to the <see cref="Kinoa.FeaturesSettings.OnChecksumChanged"/> event
    ///     to receive change notifications.
    /// </summary>
    /// <param name="checksum">The Feature Settings checksum to track.</param>
    /// <param name="keys">The Feature Settings keys to track.</param>
    private static void NotifyWhenChecksumChanged(string checksum, IEnumerable<string> keys)
    {
        const int tickTimeoutMs = 3000;
        var cts = new CancellationTokenSource();
        cts.CancelAfter(10000);

        Kinoa.FeaturesSettings.NotifyWhenChecksumChanged(checksum, keys, tickTimeoutMs, cts.Token);
    }

    /// <summary>
    ///     On the Features Settings checksum received handler.
    /// </summary>
    /// <param name="response">Processed API response with the newest Features Settings checksum.</param>
    private static void OnChecksumReceived(Response<FeaturesSettingsChecksum> response) =>
        Debug.Log($"Features settings checksum received successfully: {response.IsSuccessful()}." +
                  $"\nChecksum: {response.Data?.Checksum}");

    /// <summary>
    ///     On Features Settings downloaded handler.
    /// </summary>
    /// <param name="response">Processed get Features Settings response with the FeaturesSettingsFile.</param>
    private static void OnFeaturesSettingsDownloaded(Response<FeaturesSettingsFile> response) =>
        OnFeaturesSettingsDownloaded(response, null);

    /// <summary>
    ///     On Features Settings downloaded handler.
    /// </summary>
    /// <param name="response">Processed get Features Settings response with the FeaturesSettingsFile.</param>
    /// <param name="keys">The Features Settings keys filter.</param>
    private static void OnFeaturesSettingsDownloaded(Response<FeaturesSettingsFile> response, IEnumerable<string> keys)
    {
        Debug.Log($"Features settings downloaded successfully: {response.IsSuccessful()}." +
                  $"\nChecksum: {response.Data?.Checksum}");

        if (response.IsSuccessful())
            NotifyWhenChecksumChanged(response.Data?.Checksum, keys);
    }

    /// <summary>
    ///     On Features Settings loading progress changed handler.
    /// </summary>
    /// <param name="progress">Content loading progress.</param>
    private static void OnProgressChanged(decimal progress) =>
        Debug.Log($"Features settings loading progress: {progress}");

    /// <summary>
    ///     On the checksum of tracked Features Settings file is changed on the server
    ///     and the new file is ready for download.
    /// </summary>
    /// <param name="changedChecksum">Information about the changed checksum of the tracked Features Settings file.</param>
    private static void OnChecksumChanged(ChangedChecksum changedChecksum) =>
        Debug.Log("The tracked Feature Settings file checksum is changed: " +
                  $"'{changedChecksum.NewChecksum}'." +
                  $"\nThe old checksum: '{changedChecksum.OldChecksum}'.");
}