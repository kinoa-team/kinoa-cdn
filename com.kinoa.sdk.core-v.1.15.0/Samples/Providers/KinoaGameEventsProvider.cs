using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Kinoa.Data;
using Kinoa.Data.Enum;
using Kinoa.Data.Events;
using Kinoa.Data.Messaging.InApp;
using Kinoa.Data.Network;
using Kinoa.Data.State;
using UnityEngine;

/// <summary>
///     Kinoa events provider sample on the example of casino card game.
/// </summary>
public class KinoaGameEventsProvider
{
    /// <summary>
    ///     The current player state.
    ///     To set current player state on game start <see cref="Kinoa.Player.GetState{T}"/>
    ///     <seealso cref="KinoaPlayerDataProvider.GetPlayerState"/>.
    /// </summary>
    private CustomPlayerState PlayerState => KinoaPlayerDataProvider.PlayerState;

    /// <summary>
    ///     Bind the actualized player progress with the real one.
    /// </summary>
    private readonly Dictionary<string, int> actualizedProgress = new Dictionary<string, int>
        {["Poker"] = 9, ["Blackjack"] = 3};

    /// <summary>
    ///     Bind the actualized player balance with the real one.
    /// </summary>
    private readonly Dictionary<string, decimal> actualizedBalance = new Dictionary<string, decimal>
        {["Coins"] = 1000000};

    /// <summary>
    ///     Initializes the new instance of <see cref="KinoaGameEventsProvider"/>.
    ///     Subscribes to the SDK game session events.
    /// </summary>
    public KinoaGameEventsProvider()
    {
        Kinoa.GameEvents.OnGameSessionStarted += OnGameSessionStarted;
        Kinoa.GameEvents.OnGameSessionStartFailed += OnGameSessionStartFailed;
    }

    /// <summary>
    ///     On game session start handler.
    /// </summary>
    public async Task OnGameSessionStart()
    {
        var e = new StartSessionEventData();
        PlayerState?.SessionData.SetDeepLink("SomeDeepLink");

        const int timeout = 10;
        var networkConfig = new NetworkConfiguration(timeout);

        await Kinoa.GameEvents.SendStartSessionEvent(e, PlayerState, networkConfig);
    }

    /// <summary>
    ///     On the new game session successfully started and registered on Kinoa backend.
    /// </summary>
    /// <param name="response">Response of successful session start request.</param>
    private void OnGameSessionStarted(Response<string> response) =>
        Debug.Log($"The new game session is successfully started: {response.IsSuccessful()}.");

    /// <summary>
    ///     On failed to start the new game session.
    /// </summary>
    /// <param name="response">Response of unsuccessful session start request.</param>
    private void OnGameSessionStartFailed(Response<string> response) =>
        Debug.Log($"The new game session is open is failed: {response.Data}. " +
                  $"\nResponse status: {response.Status.ToString()}");

    /// <summary>
    ///     On trying to improve/increase the player's in-game progress handler.
    /// </summary>
    public void OnProgressionImprove()
    {
        PlayerState?.SetBalance(actualizedBalance);

        var e = new ProgressionEventData();
        e.SetPlace("Poker");
        e.SetSpent(new Dictionary<string, decimal> {["Coins"] = 9});
        e.SetReceived(new Dictionary<string, decimal> {["Coins"] = 999});
        e.SetDuration(60L);

        Kinoa.GameEvents.SendProgressionEvent(e, PlayerState);
    }

    /// <summary>
    ///     On player closes the In-app message.
    ///     Sends the In-app close event with the <see cref="InAppMessage.MessageId"/>.
    /// </summary>
    /// <param name="inAppMessage">The closed In-app message.</param>
    /// <param name="inAppMessageID">The closed In-app message identifier.</param>
    public void OnCloseInApp(InAppMessage inAppMessage, string inAppMessageID = null)
    {
        PlayerState?.SetBalance(actualizedBalance);

        var e = new InAppCloseEventData(inAppMessage?.MessageId ??
                                        (!string.IsNullOrEmpty(inAppMessageID) ? inAppMessageID : "SomeInAppID"));
        e.SetPlace("Poker");
        e.SetSpent(new Dictionary<string, decimal> {["Coins"] = 9});
        e.SetReceived(new Dictionary<string, decimal> {["Coins"] = 999});

        Kinoa.GameEvents.SendInAppCloseEvent(e, PlayerState);
    }

    /// <summary>
    ///     On player clicks the In-app message.
    ///     Sends the In-app click event with the <see cref="InAppMessage.MessageId"/>.
    /// </summary>
    /// <param name="inAppMessage">The clicked In-app message.</param>
    /// <param name="inAppMessageID">The clicked In-app message identifier.</param>
    public void OnClickInApp(InAppMessage inAppMessage, string inAppMessageID = null)
    {
        PlayerState?.SetBalance(actualizedBalance);

        var e = new InAppClickEventData(inAppMessage?.MessageId ??
                                        (!string.IsNullOrEmpty(inAppMessageID) ? inAppMessageID : "SomeInAppID"));
        e.SetPlace("Poker");
        e.SetSpent(new Dictionary<string, decimal> {["Coins"] = 9});
        e.SetReceived(new Dictionary<string, decimal> {["Coins"] = 999});

        Kinoa.GameEvents.SendInAppClickEvent(e, PlayerState);
    }

    /// <summary>
    ///     On player impresses the In-app message.
    ///     Sends the In-app impression event with the <see cref="InAppMessage.MessageId"/>.
    /// </summary>
    /// <param name="inAppMessage">The impressed In-app message.</param>
    /// <param name="inAppMessageID">The impressed In-app message identifier.</param>
    public void OnInAppImpression(InAppMessage inAppMessage, string inAppMessageID = null)
    {
        var e = new InAppImpressionEventData(inAppMessage?.MessageId ??
                                             (!string.IsNullOrEmpty(inAppMessageID) ? inAppMessageID : "SomeInAppID"));
        Kinoa.GameEvents.SendInAppImpressionEvent(e);
    }

    /// <summary>
    ///     On player set InApp impression behaviour.
    /// </summary>
    public void OnInAppSetImpressionBehaviour()
    {
        Kinoa.GameEvents.SetInAppImpressionBehaviour(InAppImpressionBehaviour.Custom);
    }

    /// <summary>
    ///     On player level up handler.
    /// </summary>
    public void OnLevelUp()
    {
        var newLevel = PlayerState?.Level == null ? 5 : (int) PlayerState.Level + 1;
        PlayerState?
            .SetProgress(actualizedProgress)
            .SetBalance(actualizedBalance)
            .SetLevel(newLevel);

        var e = new LevelUpEventData();
        e.SetLevel(newLevel);
        e.SetPlace("Poker");
        e.SetSpent(new Dictionary<string, decimal> {["Coins"] = 9});
        e.SetReceived(new Dictionary<string, decimal> {["Coins"] = 999});
        e.SetDuration(60L);

        Kinoa.GameEvents.SendLevelUpEvent(e, PlayerState);
    }

    /// <summary>
    ///     On purchase for the real money handler.
    /// </summary>
    /// <param name="success">Is payment completed successfully.</param>
    /// <param name="productId">Shop product identifier in market.</param>
    /// <param name="spent">Spent real money.</param>
    /// <param name="currency">Real money currency.</param>
    /// <param name="received">Received in-game resources.</param>
    public void OnPayment(bool success = true,
        string productId = "play-market-product-id",
        decimal spent = 0.99m,
        Currency currency = Currency.USD,
        Dictionary<string, decimal> received = null)
    {
        PlayerState?.SetBalance(actualizedBalance);

        var e = new PaymentEventData(success, productId, spent, currency, received);
        e.SetPlace("Blackjack");
        e.AddSpent(new Dictionary<string, decimal>() {["resource_key"] = 10});
        if (PlayerState?.Level != null)
            e.SetLevel((int) PlayerState.Level);

        Kinoa.GameEvents.SendPaymentEvent(e, PlayerState);
    }

    /// <summary>
    ///     On in-game purchase handler.
    /// </summary>
    /// <param name="spent">Spent in-game resources.</param>
    /// <param name="received">Received in-game resources.</param>
    public void OnInGamePurchase(Dictionary<string, decimal> spent = null, Dictionary<string, decimal> received = null)
    {
        PlayerState?.SetBalance(actualizedBalance);

        var e = new InGamePurchaseEventData(spent, received);
        e.SetPlace("Poker");
        if (PlayerState?.Level != null)
            e.SetLevel((int) PlayerState.Level);

        Kinoa.GameEvents.SendInGamePurchaseEvent(e, PlayerState);
    }

    /// <summary>
    ///     On tutorial completed handler.
    /// </summary>
    /// <param name="action">Tutorial action.</param>
    public void OnTutorial(TutorialAction action = TutorialAction.Finish)
    {
        PlayerState?.SetBalance(actualizedBalance);

        var e = new TutorialEventData(action);
        e.SetStep(1);
        e.SetPlace("Poker");
        e.SetReceived(new Dictionary<string, decimal> {["Coins"] = 100});
        if (PlayerState?.Level != null)
            e.SetLevel((int) PlayerState.Level);

        Kinoa.GameEvents.SendTutorialEvent(e, PlayerState);
    }

    /// <summary>
    ///     On player social network connect handler.
    ///     Sets player social networks with mocked IDs.
    /// </summary>
    public void OnSocialConnect()
    {
        PlayerState?.PlayerIdentifiers
            .SetFacebookId("mock-fb-id-x")
            .SetGoogleId("mock-google-id-x")
            .SetAppleId("mock-apple-id-x");
        PlayerState?
            .SetBalance(actualizedBalance);

        var e = new SocialConnectEventData();
        e.SetReceived(new Dictionary<string, decimal> {["Coins"] = 100});

        Kinoa.GameEvents.SendSocialConnectEvent(e, PlayerState);
    }

    /// <summary>
    ///     On player social network disconnect handler.
    ///     Clears information about the connected player social networks.
    /// </summary>
    public void OnSocialDisconnect()
    {
        PlayerState?.PlayerIdentifiers
            .SetFacebookId(null)
            .SetGoogleId(null)
            .SetAppleId(null);

        /*PlayerState?.PlayerIdentifiers.SetIdentifiers(new Dictionary<PlayersIds, string>
        {
            [PlayersIds.FacebookId] = null,
            [PlayersIds.NativeId] = null,
            [PlayersIds.GoogleId] = null,
            [PlayersIds.AppleId] = null,
            [PlayersIds.ExtraId1] = null,
            [PlayersIds.ExtraId2] = null,
            [PlayersIds.ExtraId3] = null
        });*/

        var e = new SocialDisconnectEventData();
        e.SetReceived(new Dictionary<string, decimal> {["Coins"] = 100});

        Kinoa.GameEvents.SendSocialDisconnectEvent(e, PlayerState);
    }

    /// <summary>
    ///     On post in social networks handler.
    /// </summary>
    public void OnSocialPost()
    {
        PlayerState?.SetBalance(actualizedBalance);

        var e = new SocialPostEventData();
        e.SetPlace("Poker");
        e.SetReceived(new Dictionary<string, decimal> {["Coins"] = 100});
        if (PlayerState?.Level != null)
            e.SetLevel((int) PlayerState.Level);

        Kinoa.GameEvents.SendSocialPostEvent(e, PlayerState);
    }

    /// <summary>
    ///     On custom event handler.
    /// </summary>
    /// <param name="name">Custom event name.</param>
    public void OnCustomEvent(string name = "custom_event")
    {
        PlayerState?.PersonalInfo
            .SetCountry(Country.Ukraine)
            .SetCity("Kyiv");

        var e = new CustomEventData(name);
        e.SetPlace("profile_menu");
        e.SetCustomParams(new Dictionary<string, object>
        {
            ["ID"] = 1.0,
            ["custom_key_2"] = "custom_value_2",
            ["camelCase"] = true
        });
        if (PlayerState?.Level != null)
            e.SetLevel((int) PlayerState.Level);

        Kinoa.GameEvents.SendCustomEvent(e, PlayerState);
    }

    /// <summary>
    ///     On the Player cheating.
    ///     Marks the Player as a cheater and blocks the Player.
    /// </summary>
    public void OnCheatingEvent()
    {
        if (PlayerState == null)
            return;

        var isCheater = !PlayerState.IsCheater;
        PlayerState?
            .SetIsCheater(isCheater)
            .SetIsBlocked(isCheater);

        var e = new CustomEventData("cheating_event");
        Kinoa.GameEvents.SendCustomEvent(e, PlayerState);
    }

    /// <summary>
    ///     On client-side error handler.
    /// </summary>
    /// <param name="exception">Thrown exception.</param>
    public void OnError(Exception exception)
    {
        var e = new ErrorEventData(exception);
        e.SetCustomParams(new Dictionary<string, object>
            {["additional_message"] = "Something went wrong during e.g. SDK response deserialization."});

        Kinoa.GameEvents.SendErrorEvent(e);
    }
}