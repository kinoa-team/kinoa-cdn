﻿using Kinoa.Data.Enum;

/// <summary>
///     Kinoa SDK basic methods and properties provider sample.
/// </summary>
public class KinoaBaseProvider
{
    /// <summary>
    ///    Kinoa Game unique identifier.
    /// </summary>
    private const string GameID = "TYPE_YOUR_GAME_ID_HERE";

    /// <summary>
    ///    Kinoa game secret token .
    /// </summary>
    private const string GameToken = "TYPE_YOUR_GAME_TOKEN_HERE";

    /// <summary>
    ///     Kinoa game API base URL. Use game staging or production environment URL address.
    /// </summary>
    private const string KinoaApiUrl = "https://TYPE_YOYR_ENVIROMENT_URL_HERE/api/v3";

    /// <summary>
    ///     Kinoa game WebSocket channel base URL. Use game staging or production environment URL address.
    /// </summary>
    private const string KinoaWebSocketUrl = "wss://TYPE_YOYR_ENVIROMENT_URL_HERE/messaging/subscribe";

    /// <summary>
    ///     Gets the SDK version number.
    /// </summary>
    public static string SDKVersion => Kinoa.SDK.Version;

    /// <summary>
    ///     Initialize the new instance of <see cref="KinoaBaseProvider"/>
    /// </summary>
    public KinoaBaseProvider() => Init();

    /// <summary>
    ///     Initialize and configure the SDK
    /// </summary>
    private static void Init()
    {
        Kinoa.SDK.SetLogLevel(LogLevel.Trace);
        Kinoa.SDK.Initialize(GameID, GameToken, KinoaApiUrl);
        Kinoa.Messaging.Initialize(KinoaWebSocketUrl);
    }
}