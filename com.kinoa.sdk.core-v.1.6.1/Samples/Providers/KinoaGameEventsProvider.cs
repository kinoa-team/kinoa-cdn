using System;
using System.Collections.Generic;
using Kinoa.Core.Network;
using Kinoa.Data.Enum;
using Kinoa.Data.Events;
using UnityEngine;

/// <summary>
///     Kinoa events provider sample on the example of casino card game.
/// </summary>
public class KinoaGameEventsProvider
{
    /// <summary>
    ///     The current player state.
    ///     To set current player state on game start <see cref="Kinoa.Player.GetState{T}"/>
    ///     <seealso cref="KinoaPlayerDataProvider.GetPlayerState"/>.
    /// </summary>
    private CustomPlayerState PlayerState => KinoaPlayerDataProvider.PlayerState;

    /// <summary>
    ///     Bind the actualized player progress with the real one.
    /// </summary>
    private readonly Dictionary<string, int> actualizedProgress = new Dictionary<string, int>
        {["Poker"] = 9, ["Blackjack"] = 3};

    /// <summary>
    ///     Bind the actualized player balance with the real one.
    /// </summary>
    private readonly Dictionary<string, decimal> actualizedBalance = new Dictionary<string, decimal>
        {["Coins"] = 1000000};

    /// <summary>
    ///     Initializes the new instance of <see cref="KinoaGameEventsProvider"/>.
    ///     Subscribes to the SDK game session events.
    /// </summary>
    public KinoaGameEventsProvider()
    {
        Kinoa.GameEvents.OnGameSessionStarted += OnGameSessionStarted;
        Kinoa.GameEvents.OnGameSessionStartFailed += OnGameSessionStartFailed;
    }

    /// <summary>
    ///     On game session start handler.
    /// </summary>
    public void OnGameSessionStart()
    {
        var e = new StartSessionEventData();
        PlayerState?.SessionData.SetDeepLink("SomeDeepLink");
        Kinoa.GameEvents.SendStartSessionEvent(e, PlayerState);
    }

    /// <summary>
    ///     On the new game session successfully started and registered on Kinoa backend.
    /// </summary>
    /// <param name="sessionID">Active session ID.</param>
    private void OnGameSessionStarted(string sessionID) =>
        Debug.Log($"The new game session is successfully started: {sessionID}.");

    /// <summary>
    ///     On failed to start the new game session.
    /// </summary>
    /// <param name="sessionID">Failed session ID.</param>
    /// <param name="responseState">The status of server response.</param>
    private void OnGameSessionStartFailed(string sessionID, ResponseState responseState) =>
        Debug.Log($"The new game session is open is failed: {sessionID}. Response status: {responseState}");

    /// <summary>
    ///     On trying to improve/increase the player's in-game progress handler.
    /// </summary>
    public void OnProgressionImprove()
    {
        PlayerState?.SetBalance(actualizedBalance);

        var e = new ProgressionEventData();
        e.SetPlace("Poker");
        e.SetSpent(new Dictionary<string, decimal> {["Coins"] = 9});
        e.SetReceived(new Dictionary<string, decimal> {["Coins"] = 999});
        e.SetDuration(60L);

        Kinoa.GameEvents.SendProgressionEvent(e, PlayerState);
    }

    /// <summary>
    ///     On player close InApp.
    /// </summary>
    public void OnCloseInApp()
    {
        PlayerState?.SetBalance(actualizedBalance);

        var e = new InAppCloseEventData("SomeInAppID");
        e.SetPlace("Poker");
        e.SetSpent(new Dictionary<string, decimal> {["Coins"] = 9});
        e.SetReceived(new Dictionary<string, decimal> {["Coins"] = 999});

        Kinoa.GameEvents.SendInAppCloseEvent(e, PlayerState);
    }

    /// <summary>
    ///     On player level up handler.
    /// </summary>
    public void OnLevelUp()
    {
        PlayerState?
            .SetProgress(actualizedProgress)
            .SetBalance(actualizedBalance);

        var e = new LevelUpEventData();
        e.SetLevel(10);
        e.SetPlace("Poker");
        e.SetSpent(new Dictionary<string, decimal> {["Coins"] = 9});
        e.SetReceived(new Dictionary<string, decimal> {["Coins"] = 999});
        e.SetDuration(60L);

        Kinoa.GameEvents.SendLevelUpEvent(e, PlayerState);
    }

    /// <summary>
    ///     On purchase for the real money handler.
    /// </summary>
    /// <param name="success">Is payment completed successfully.</param>
    /// <param name="productId">Shop product identifier in market.</param>
    /// <param name="spent">Spent money in USD.</param>
    /// <param name="received">Received in-game resources.</param>
    public void OnPayment(bool success = true,
        string productId = "play-market-product-id",
        decimal spent = 0.99m,
        Dictionary<string, decimal> received = null)
    {
        PlayerState?.SetBalance(actualizedBalance);

        var e = new PaymentEventData(success, productId, spent, received);
        e.SetLevel(3);
        e.SetPlace("Blackjack");

        Kinoa.GameEvents.SendPaymentEvent(e, PlayerState);
    }

    /// <summary>
    ///     On in-game purchase handler.
    /// </summary>
    /// <param name="spent">Spent in-game resources.</param>
    /// <param name="received">Received in-game resources.</param>
    public void OnInGamePurchase(Dictionary<string, decimal> spent = null, Dictionary<string, decimal> received = null)
    {
        PlayerState?.SetBalance(actualizedBalance);

        var e = new InGamePurchaseEventData(spent, received);
        e.SetLevel(10);
        e.SetPlace("Poker");

        Kinoa.GameEvents.SendInGamePurchaseEvent(e, PlayerState);
    }

    /// <summary>
    ///     On tutorial completed handler.
    /// </summary>
    /// <param name="action">Tutorial action.</param>
    public void OnTutorial(TutorialAction action = TutorialAction.Finish)
    {
        PlayerState?.SetBalance(actualizedBalance);

        var e = new TutorialEventData(action);
        e.SetStep(1);
        e.SetLevel(10);
        e.SetPlace("Poker");
        e.SetReceived(new Dictionary<string, decimal> {["Coins"] = 100});

        Kinoa.GameEvents.SendTutorialEvent(e, PlayerState);
    }

    /// <summary>
    ///     On player social network connect handler.
    ///     Sets player social networks with mocked IDs.
    /// </summary>
    public void OnSocialConnect()
    {
        PlayerState?.PlayerIdentifiers
            .SetFacebookID("mock-fb-id-x")
            .SetNativeID("mock-native-player-id-x");
        PlayerState?
            .SetBalance(actualizedBalance);

        var e = new SocialEventData();
        e.SetReceived(new Dictionary<string, decimal> {["Coins"] = 100});

        Kinoa.GameEvents.SendSocialConnectEvent(e, PlayerState);
    }

    /// <summary>
    ///     On player social network disconnect handler.
    ///     Clears information about the connected player social networks.
    /// </summary>
    public void OnSocialDisconnect()
    {
        PlayerState?.PlayerIdentifiers
            .SetFacebookID(null)
            .SetNativeID(null);

        var e = new SocialEventData();
        e.SetReceived(new Dictionary<string, decimal> {["Coins"] = 100});

        Kinoa.GameEvents.SendSocialDisconnectEvent(e, PlayerState);
    }

    /// <summary>
    ///     On post in social networks handler.
    /// </summary>
    public void OnSocialPost()
    {
        PlayerState?.SetBalance(actualizedBalance);

        var e = new SocialEventData();
        e.SetLevel(10);
        e.SetPlace("Poker");
        e.SetReceived(new Dictionary<string, decimal> {["Coins"] = 100});

        Kinoa.GameEvents.SendSocialPostEvent(e, PlayerState);
    }

    /// <summary>
    ///     On custom event handler.
    /// </summary>
    /// <param name="name">Custom event name.</param>
    public void OnCustomEvent(string name = "custom_event")
    {
        PlayerState?.PersonalInfo
            .SetCountry(Country.Ukraine)
            .SetCity("Kyiv");

        var e = new CustomEventData(name);
        e.SetLevel(10);
        e.SetPlace("profile_menu");
        e.SetCustomParams(new Dictionary<string, object>
        {
            ["custom_key_1"] = 1.0,
            ["custom_key_2"] = "custom_value_2",
            ["custom_key_3"] = true
        });

        Kinoa.GameEvents.SendCustomEvent(e, PlayerState);
    }

    /// <summary>
    ///     On client-side error handler.
    /// </summary>
    /// <param name="exception">Thrown exception.</param>
    public void OnError(Exception exception)
    {
        var e = new ErrorEventData(exception);
        e.SetCustomParams(new Dictionary<string, object>
            {["additional_message"] = "Something went wrong during e.g. SDK response deserialization."});

        Kinoa.GameEvents.SendErrorEvent(e);
    }
}