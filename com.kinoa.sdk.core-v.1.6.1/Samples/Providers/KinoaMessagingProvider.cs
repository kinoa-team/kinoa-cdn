﻿using System.Linq;
using Kinoa.Data.Messaging;
using Kinoa.Data.Messaging.InApps;
using UnityEngine;

/// <summary>
///     Kinoa messaging provider sample
/// </summary>
public class KinoaMessagingProvider
{
    /// <summary>
    ///     Initialize the new instance of <see cref="KinoaMessagingProvider"/>
    /// </summary>
    public KinoaMessagingProvider() => SetMessagingHandler();

    /// <summary>
    ///     Sets application handled messaging callbacks.
    /// </summary>
    public void SetMessagingHandler() => Kinoa.Messaging.SetMessageHandlers(OnCommandReceived, OnInnAppReceived);

    /// <summary>
    ///     On command message received.
    /// </summary>
    /// <param name="message">Command message.</param>
    private static void OnCommandReceived(CommandMessage message) =>
        Debug.Log($"Message received: {message.Type}: {message.Action}.");

    /// <summary>
    ///     On in-app messages received.
    /// </summary>
    /// <param name="messages">In-app messages.</param>
    private static void OnInnAppReceived(InAppMessages messages) =>
        Debug.Log($"Message received: {messages.Type}({messages.Data?.Count}): " +
                  $"{(messages.Data?.Count > 0 ? string.Join(", ", messages.Data.Select(o => o.Uuid).ToArray()) : string.Empty)}.");
}