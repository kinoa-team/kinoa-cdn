# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/), and this project adheres
to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.6.1.0] - 2022-09-14

### Added

- Game session implementation.
- Personal Offers support.
- In-app messages support.
- Get inbox messages.
- New inbox message web socket command.
- Delete inbox message by ID.
- Delete all inbox messages.
- In-app click event.
- In-app impression event.

### Changed

- Bug fixes, refactoring, and other improvements.

## [1.6.0.0] - 2022-09-14

### Added

- GameID support.
- Game-Auth security layer.
- Async UnityWebRequest client.
- Unstable Internet connection handling. Retries implementation.
- Get Feature Settings directly from cache.

### Changed

- Global SDK project structure updates.
- Bug fixes, refactoring, and other improvements.

## [1.5.5.0] - 2022-06-22

### Added

- Logging severity levels.
- Predefined state fields:
  - The "PlayerState.SessionData" field includes important session data.
  - The "PlayerState.Devices" field includes player devices data.
  - Other important state fields.
  
- Get/Set player country method (Countries enum).
- Get/Set player language method (Languages enum).

### Changed

- Built-in features settings mobile platforms fix.
- The "PlayerState.TimeZone" updates.
- The "GameEventData" fields updates.
- Reset Player State business logic updates.
- Bug fixes, refactoring, and other improvements.

## [1.5.0] - 2022-05-24

### Added

- In-app messages.
- In-app close event.
- Get Features Settings by key.

### Changed

- Bug fixes, refactoring, and other improvements.

## [1.4.5] - 2022-04-19

### Added

- Get player state request.
- Update player state request.
- Create the new player request.
- Compatibility with the player state derided types.
- An ActivePlayerID setter moved on the game side.
- Get all player-related accounts request.
- Approve player state changes request.
- Handling player state-changing by an operator.
- Country and language code support by the SetPlayerInfo player state extension method.
- P2P events:
  - Get P2P events request.
  - Send P2P events request.
  - Delete P2P events request.
- Messaging & WebSocket protocol support:
  - Command messages.

### Changed

- Bug fixes, refactoring, and other improvements.

## [1.4.0] - 2022-02-18

### Added

- An "Error" event:
    - SDK internal critical errors reporting.
    - Client-side errors reporting.

### Changed

- Transition to the UUID's identifiers.
- Transition to the Single-economy (features settings).
- Transition from "Economy" to "Features Settings" component:
    - Updated namespaces and signatures: "Kinoa.Economy" to "Kinoa.FeaturesSettings"
    - Updated built-in features settings file path

- Third-party dependencies:
    - Used Unity compatible newtonsoft.json package: "jillejr.newtonsoft.json-for-unity" to "
      com.unity.nuget.newtonsoft-json"

- Bug fixes and other improvements.

## [1.3.5] - 2022-01-14

### Added

- Compressed events data-transfer.
- The updated economies compression algorithm.

- The new events signatures.
- Billing "platform" field setter.
- "Install" event automatic raising on the first game launch.

- "KinoaBaseProvider" - SDK basic methods and properties provider sample.
- "KinoaEventsProvider" - SDK events provider sample.

### Changed

- SDK configuration is moved from "KinoaAnalyticsProvider" to the new "KinoBaseProvider" component.
- Events are moved from "KinoaAnalyticsProvider" to the new "KinoaEventsProvider" component.
- Updated SDK namespaces:
    - "Kinoa.Analytics" was renamed to "Kinoa.Events".
    - "Kinoa.SDK" - contains SDK basic methods and properties.

- Bug fixes and other minor improvements.

### Removed

- "Install" event manual invocation possibility.
- "KinoaAnalyticsProvider" was removed.

## [1.3.0] - 2021-11-08

### Added

- One-click integration – just import the Kinoa samples from Unity PM to your game and that's it.

- Kinoa Offline gaming:
    - Offline and built-in economies are available now. See the documentation for more
      details: "https://docs.google.com/document/d/10no1lT7n-3IG_IRFoeF8C-6-XYQ0sOtJ/edit#heading=h.9cy9xioexqo4"
    - Offline local events storage were implemented.

- Сommon SDK files storage mechanism developed.
- Multiple players events storage supporting. Sending, failed, and actual events storage for each player.
- Events processing behaviour was changed.

- "Kinoa.Analytics.SetPlayerChangedHandler" method added – returns an active player identifier when it was changed. Use
  cases:
    - Download Economy for the new Player
    - In-app business logic on Active Player changed

- "start_session" / "install" events were updated:
    - SetSocialNetworks possibility added
    - Device information was extended with the new extra fields:
      "time_zone", "device_model", "screen_resolution", "screen_dpi", "locale"

- "social_connect" / "social_disconnect" events were updated:
    - Player state – progress and player balance is required now

### Changed

- Kinoa CDN is moved to the Kinoa repositories. The new Kinoa SDK package URL:
  "https://bitbucket.org/kinoa-team/kinoa-cdn/src/master/kinoa-sdk-v.1.3.0"

- "Kinoa.Economy.Checksum" / "Kinoa.Economy.Download" signature updates

- Bug fixes and other minor improvements.

### Removed

- "start_session" event – application handled callback was removed

## [1.2.1] - 2021-09-30

### Added

- Social disconnect event. Social connect and disconnect states added.

### Changed

- Event data custom parameters for all events.
- Bug fixes and other minor improvements.

### Removed

- ThirdPartyServices field from PlayerState.

## [1.2.0] - 2021-08-30

### Added

- Multiple economies caching system.
- Economies filters: by name, by category.
- SDK testing solution.
- Bug fixes.

## [1.1.0] - 2021-08-09

### Added

- Economies checksum request.
- Economies downloading request.
- Economies caching system.
- Compressed economies files implementation (20kB instead of 7mB).
- Bug fixes.

## [1.0.0] - 2021-07-30

### Added

- The first version of SDK developed.
- All game events are sent and processed via the API and Kinoa SDK for Unity.
- Event data aggregated by ClickHouse.
- Kinoa API analyzes data and calculates the audiences dynamically.

### Changed

- SDK is moved to a separate repository to provide the possibility to instal the SDK through the Unity Package Manager.

### Removed

- SDK is removed from C&F repository.

### Fixed

- All the major and minor bugs were fixed.