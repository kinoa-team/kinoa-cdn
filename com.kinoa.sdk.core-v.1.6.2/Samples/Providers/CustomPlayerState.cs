﻿using System.Collections.Generic;
using Kinoa.Data.State;

/// <summary>
///     The custom player state object <see cref="PlayerState"/>.
/// </summary>
public class CustomPlayerState : PlayerState
{
    public string Foo { get; set; } = "Foo";
    public List<string> Bar { get; set; } = new List<string> {"Bar"};
}