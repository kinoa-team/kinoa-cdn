﻿using System.Linq;
using System.Text;
using DefaultNamespace;
using Kinoa.Core.Network;
using Kinoa.Data.Enum;
using Kinoa.Data.Messaging.Command;
using Kinoa.Data.Messaging.InApp;
using UnityEngine;

/// <summary>
///     Kinoa messaging provider sample.
/// </summary>
public class KinoaMessagingProvider
{
    /// <summary>
    ///     Initialize the new instance of <see cref="KinoaMessagingProvider"/>
    /// </summary>
    public KinoaMessagingProvider() => SetMessagingHandler();

    /// <summary>
    ///     Sets application handled messaging callbacks.
    /// </summary>
    public void SetMessagingHandler() => Kinoa.Messaging.SetMessageHandlers(OnCommandReceived, OnInnAppReceived);

    /// <summary>
    ///     On command message received.
    /// </summary>
    /// <param name="message">Command message.</param>
    private static void OnCommandReceived(CommandMessage message) =>
        Log($"Message type of {message.Type} is received: {message.Action}." +
            $"\nID: {message.Uuid}");

    /// <summary>
    ///     On in-app messages received.
    /// </summary>
    /// <param name="messages">In-app messages.</param>
    private static void OnInnAppReceived(InAppMessages messages) =>
        Log($"Messages type of {messages.Type} are received ({messages.Data?.Count}):\n" +
            $"{(messages.Data?.Count > 0 ? string.Join(",\n", messages.Data.Select(o => $"{o.Type.ToString()}: {o.Uuid}").ToArray()) : string.Empty)}.");

    /// <summary>
    ///     Gets the list of all inbox messages async.
    /// </summary>
    /// <returns>Result of processed request as logs.</returns>
    public async void GetInboxMessagesAsync()
    {
        var logBuilder = new StringBuilder();
        var response = await Kinoa.Messaging.GetInboxMessagesAsync();
        if (response.Status == ResponseState.Success && response.Data != null)
        {
            var inAppMessages =
                response.Data.Where(x => x.Type == SocketMessageType.InAppMessage).ToList();
            var personalOffers =
                response.Data.Where(x => x.Type == SocketMessageType.PersonalOffer).ToList();

            logBuilder.Append($"Inbox messages received successfully: ({response.Data.Count})." +
                              $"\nIn-app messages ({inAppMessages.Count})" +
                              //$": {string.Join(", ", inAppMessages.Select(o => o.Uuid).ToArray())}" +
                              $"\nPersonal offers ({personalOffers.Count})");
                              //$": {string.Join(", ", personalOffers.Select(o => o.Uuid).ToArray())}");
        }
        else logBuilder.Append($"Get inbox messages request status: {response.Status.ToString()}.");

        Log(logBuilder.ToString());
    }

    /// <summary>
    ///     Deletes all inbox messages.
    /// </summary>
    public async void DeleteAllInboxMessagesAsync()
    {
        var response = await Kinoa.Messaging.DeleteAllInboxMessagesAsync();
        Log($"{nameof(DeleteAllInboxMessagesAsync)} request status: {response.ToString()}.");
    }

    /// <summary>
    ///     Deletes inbox message by uuid.
    /// </summary>
    /// <param name="messageID">Message uuid.</param>
    public async void DeleteInboxMessageAsync(string messageID)
    {
        var response = await Kinoa.Messaging.DeleteInboxMessageAsync(messageID);
        Log($"{nameof(DeleteInboxMessageAsync)} request status: {response.ToString()}.");
    }

    /// <summary>
    ///     Shows inbox messages on UI (for demonstration purposes only).
    ///     Use your custom UI interface and client notification mechanism.
    /// </summary>
    private static void Log(string message)
    {
        Debug.Log(message);
        NotificationManager.Instance.ShowNotification(message);
    }
}