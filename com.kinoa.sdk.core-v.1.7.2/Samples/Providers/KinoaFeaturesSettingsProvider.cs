﻿using System.Collections.Generic;
using System.Threading;
using Kinoa.Data;
using Kinoa.Data.FeaturesSettings;
using UnityEngine;

/// <summary>
///     Kinoa features settings provider sample.
/// </summary>
public class KinoaFeaturesSettingsProvider
{
    /// <summary>
    ///     Gets the newest features settings checksum.
    /// </summary>
    public void Checksum() => Kinoa.FeaturesSettings.GetChecksum(OnChecksumReceived);

    /// <summary>
    ///     Gets last cached or built-in player features settings file.
    /// </summary>
    /// <param name="keys">Filters features settings by keys.</param>
    /// <returns>Cached or built-in features settings.</returns>
    public async void GetCached(IEnumerable<string> keys = null)
    {
        var featuresSettings = await Kinoa.FeaturesSettings.GetCached(keys);
        Debug.Log($"Last cached settings loaded successfully: {featuresSettings != null}." +
                  $"\nChecksum: {featuresSettings?.Checksum}");
    }

    /// <summary>
    ///     Gets the newest available features settings file.
    /// </summary>
    public void Download()
    {
        var cts = new CancellationTokenSource();
        cts.CancelAfter(10000);
        Kinoa.FeaturesSettings.Download(OnFeaturesSettingsDownloaded, OnProgressChanged, null, cts.Token);
    }

    /// <summary>
    ///     Gets the newest available features settings file by keys.
    /// </summary>
    public void Download(IEnumerable<string> keys)
    {
        var cts = new CancellationTokenSource();
        cts.CancelAfter(10000);
        Kinoa.FeaturesSettings.Download(OnFeaturesSettingsDownloaded, OnProgressChanged, keys, cts.Token);
    }

    /// <summary>
    ///     On the features settings checksum received handler.
    /// </summary>
    /// <param name="response">Processed API response with the newest features settings checksum.</param>
    private static void OnChecksumReceived(Response<FeaturesSettingsChecksum> response) =>
        Debug.Log($"Features settings checksum received successfully: {response.IsSuccessful()}." +
                  $"\nChecksum: {response.Data?.Checksum}");

    /// <summary>
    ///     On features settings downloaded handler.
    /// </summary>
    /// <param name="response">Processed get Features Settings response with the FeaturesSettingsFile.</param>
    private static void OnFeaturesSettingsDownloaded(Response<FeaturesSettingsFile> response) =>
        Debug.Log($"Features settings downloaded successfully: {response.IsSuccessful()}." +
                  $"\nChecksum: {response.Data?.Checksum}");

    /// <summary>
    ///     On features settings loading progress changed handler.
    /// </summary>
    /// <param name="progress">Content loading progress.</param>
    private static void OnProgressChanged(decimal progress) =>
        Debug.Log($"Features settings loading progress: {progress}");
}