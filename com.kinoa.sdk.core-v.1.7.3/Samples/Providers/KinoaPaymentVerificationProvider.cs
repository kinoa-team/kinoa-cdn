using Kinoa.Data.State;
using UnityEngine;

/// <summary>
///     Kinoa payment verification provider sample.
/// </summary>
public class KinoaPaymentVerificationProvider
{
    /// <summary>
    ///     Verifies purchase.
    /// </summary>
    public async void Verify(string orderId, string packageName, string purchaseToken, string productId)
    {
        var purchase = new Purchase(new PaymentReceipt(orderId, packageName, purchaseToken, productId));
        var response = await Kinoa.PaymentVerification.VerifyPurchase(purchase);
        Debug.Log($"{nameof(KinoaPaymentVerificationProvider)}.{nameof(Verify)} " +
                  $"request status: {response.Status.ToString()}" +
                  $"\nVerified successfully: {(response.Data != null ? response.Data.Result : "Payment is not verified")}.");
    }
}