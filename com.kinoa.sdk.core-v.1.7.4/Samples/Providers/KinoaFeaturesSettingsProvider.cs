﻿using System.Collections.Generic;
using System.Threading;
using Kinoa.Data;
using Kinoa.Data.FeaturesSettings;
using UnityEngine;

/// <summary>
///     Kinoa Features Settings provider sample.
/// </summary>
public class KinoaFeaturesSettingsProvider
{
    /// <summary>
    ///     Gets the newest Features Settings checksum.
    /// </summary>
    public void Checksum() => Kinoa.FeaturesSettings.GetChecksum(OnChecksumReceived);

    /// <summary>
    ///     Gets cached or built-in player Features Settings file by the newest timestamp.
    /// </summary>
    /// <param name="keys">Filters Features Settings by keys.</param>
    /// <returns>Cached or built-in Features Settings.</returns>
    public async void GetLastCachedOrBuiltIn(IEnumerable<string> keys = null)
    {
        var response = await Kinoa.FeaturesSettings.GetLastCachedOrBuiltIn(keys);
        Debug.Log($"Last cached or built-in Features Settings loaded successfully: {response.IsSuccessful()}." +
                  $"\nChecksum: {response.Data?.Checksum}");
    }

    /// <summary>
    ///     Gets built-in player Features Settings file.
    /// </summary>
    /// <param name="keys">Filters Features Settings by keys.</param>
    /// <returns>Built-in Feature Settings file.</returns>
    public async void GetBuiltIn(IEnumerable<string> keys = null)
    {
        var response = await Kinoa.FeaturesSettings.GetBuiltIn(keys);
        Debug.Log($"Built-in Features Settings loaded successfully: {response.IsSuccessful()}." +
                  $"\nChecksum: {response.Data?.Checksum}");
    }

    /// <summary>
    ///     Gets cached player Features Settings file.
    /// </summary>
    /// <param name="keys">Filters Features Settings by keys.</param>
    /// <returns>Cached Feature Settings file.</returns>
    public async void GetCached(IEnumerable<string> keys = null)
    {
        var response = await Kinoa.FeaturesSettings.GetCached(keys);
        Debug.Log($"Cached Features Settings loaded successfully: {response.IsSuccessful()}." +
                  $"\nChecksum: {response.Data?.Checksum}");
    }

    /// <summary>
    ///     Gets the newest available Features Settings file.
    /// </summary>
    public void Download()
    {
        var cts = new CancellationTokenSource();
        cts.CancelAfter(10000);
        Kinoa.FeaturesSettings.Download(OnFeaturesSettingsDownloaded, OnProgressChanged, null, cts.Token);
    }

    /// <summary>
    ///     Gets the newest available Features Settings file by keys.
    /// </summary>
    public void Download(IEnumerable<string> keys)
    {
        var cts = new CancellationTokenSource();
        cts.CancelAfter(10000);
        Kinoa.FeaturesSettings.Download(OnFeaturesSettingsDownloaded, OnProgressChanged, keys, cts.Token);
    }

    /// <summary>
    ///     On the Features Settings checksum received handler.
    /// </summary>
    /// <param name="response">Processed API response with the newest Features Settings checksum.</param>
    private static void OnChecksumReceived(Response<FeaturesSettingsChecksum> response) =>
        Debug.Log($"Features settings checksum received successfully: {response.IsSuccessful()}." +
                  $"\nChecksum: {response.Data?.Checksum}");

    /// <summary>
    ///     On Features Settings downloaded handler.
    /// </summary>
    /// <param name="response">Processed get Features Settings response with the FeaturesSettingsFile.</param>
    private static void OnFeaturesSettingsDownloaded(Response<FeaturesSettingsFile> response) =>
        Debug.Log($"Features settings downloaded successfully: {response.IsSuccessful()}." +
                  $"\nChecksum: {response.Data?.Checksum}");

    /// <summary>
    ///     On Features Settings loading progress changed handler.
    /// </summary>
    /// <param name="progress">Content loading progress.</param>
    private static void OnProgressChanged(decimal progress) =>
        Debug.Log($"Features settings loading progress: {progress}");
}