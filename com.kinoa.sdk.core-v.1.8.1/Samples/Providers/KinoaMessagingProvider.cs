﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using DefaultNamespace;
using Kinoa.Core.Network;
using Kinoa.Data.Enum;
using Kinoa.Data.Messaging.Command;
using Kinoa.Data.Messaging.InApp;
using UnityEngine;

/// <summary>
///     Kinoa messaging provider sample.
/// </summary>
public class KinoaMessagingProvider
{
    /// <summary>
    ///     Local inbox storage.
    /// </summary>
    public List<InAppMessage> LocalInboxStorage { get; set; } = new List<InAppMessage>();

    /// <summary>
    ///     Initialize the new instance of <see cref="KinoaMessagingProvider"/>
    /// </summary>
    public KinoaMessagingProvider() => SetMessagingHandler();

    /// <summary>
    ///     Sets application handled messaging callbacks.
    /// </summary>
    public void SetMessagingHandler() => Kinoa.Messaging.SetMessageHandlers(OnCommandReceived, OnInnAppReceived);

    /// <summary>
    ///     On command message received.
    /// </summary>
    /// <param name="message">Command message.</param>
    private static void OnCommandReceived(CommandMessage message)
    {
        switch (message)
        {
            case NewResourceCommand cmd:
                Log($"Command type of {nameof(NewResourceCommand)} is received:" +
                    $"\nInbox ID: '{cmd.Id}', Message UUID:'{cmd.Uuid}'");
                break;
            case NewInboxMessageCommand cmd:
                Log($"Command type of {nameof(NewInboxMessageCommand)} is received:" +
                    $"\nInbox ID: '{cmd.Id}', Message UUID:'{cmd.Uuid}'");
                break;
            case NewInboxMessagesCommand cmd:
                Log($"Command type of {nameof(NewInboxMessagesCommand)} is received:" +
                    $"\nMessage UUID:'{cmd.Uuid}" +
                    $"\n{string.Join(",\n", cmd.Ids.Select(o => $"Inbox ID: {o.ToString()}").ToArray())}'");
                break;
            case ReloadP2PCommand cmd:
                Log($"Command type of {nameof(ReloadP2PCommand)} is received:" +
                    $"\nMessage UUID:'{cmd.Uuid}'");
                break;
            case UpdateEconomyCommand cmd:
                Log($"Command type of {nameof(UpdateEconomyCommand)} is received:" +
                    $"\nMessage UUID:'{cmd.Uuid}'");
                break;
            case PlayerStateChangedByOperatorCommand cmd:
                Log($"Command type of {nameof(PlayerStateChangedByOperatorCommand)} is received:" +
                    $"\nMessage UUID:'{cmd.Uuid}'");
                break;
            default:
                Log($"Message type of {message.Type} is received: {message.Action}." +
                    $"\nID: {message.Uuid}");
                break;
        }
    }

    /// <summary>
    ///     On in-app messages received.
    /// </summary>
    /// <param name="messages">In-app messages.</param>
    private static void OnInnAppReceived(InAppMessages messages) =>
        Log($"Messages type of {messages.Type} are received ({messages.Data?.Count}):\n" +
            $"{(messages.Data?.Count > 0 ? string.Join(",\n", messages.Data.Select(o => $"{o.Type.ToString()}: {o.Uuid}").ToArray()) : string.Empty)}.");

    /// <summary>
    ///     Gets the list of all inbox messages async.
    /// </summary>
    public async void GetInboxMessagesAsync()
    {
        var logBuilder = new StringBuilder();
        var response = await Kinoa.Messaging.GetInboxMessagesAsync();
        if (response.Status == ResponseState.Success && response.Data != null)
        {
            var simpleInAppMessages =
                response.Data.Where(x => x.Data.Template == InAppDataTemplate.Simple).ToList();
            var customInAppMessages =
                response.Data.Where(x => x.Data.Template == InAppDataTemplate.Custom).ToList();

            logBuilder.Append($"Inbox messages received successfully: ({response.Data.Count})." +
                              $"\nSimple In-app messages ({simpleInAppMessages.Count})" +
                              $"\nCustom In-app messages ({customInAppMessages.Count})");

            LocalInboxStorage = response.Data;
        }
        else logBuilder.Append($"Get inbox messages request status: {response.Status.ToString()}.");

        Log(logBuilder.ToString());
    }

    /// <summary>
    ///     Updates inbox In-app message async.
    /// </summary>
    /// <param name="message">Inbox In-app message.</param>
    /// <returns>Processed API response.</returns>
    public async void UpdateInboxMessageAsync(InAppMessage message)
    {
        var customParams = new Dictionary<string, object>
        {
            ["custom_key_1"] = 1.0,
            ["custom_key_2"] = "custom_value_2",
            ["custom_key_3"] = true
        };
        message
            .SetCustomParameters(customParams)
            .SetViewsMetrics(99)
            .SetUsageMetrics(1)
            .SetEligibility(2);

        var response = await Kinoa.Messaging.UpdateInboxMessageAsync(message);
        Log($"{nameof(UpdateInboxMessageAsync)} request status: {response.Status.ToString()}.");
    }

    /// <summary>
    ///     Updates multiple inbox In-app messages async.
    /// </summary>
    /// <param name="messages">Multiple inbox In-app messages.</param>
    /// <returns>Processed API response.</returns>
    public async void UpdateInboxMessagesAsync(List<InAppMessage> messages)
    {
        var customParams = new Dictionary<string, object>
        {
            ["custom_key_1"] = 1.0,
            ["custom_key_2"] = "custom_value_2",
            ["custom_key_3"] = true
        };
        foreach (var message in messages)
        {
            message
                .SetCustomParameters(customParams)
                .SetViewsMetrics(99)
                .SetUsageMetrics(1)
                .SetEligibility(2);
        }

        var response = await Kinoa.Messaging.UpdateInboxMessagesAsync(messages);
        Log($"{nameof(UpdateInboxMessagesAsync)} request status: {response.Status.ToString()}.");
    }

    /// <summary>
    ///     Deletes all inbox messages.
    /// </summary>
    public async void DeleteAllInboxMessagesAsync()
    {
        var response = await Kinoa.Messaging.DeleteAllInboxMessagesAsync();
        Log($"{nameof(DeleteAllInboxMessagesAsync)} request status: {response.Status.ToString()}.");
    }

    /// <summary>
    ///     Deletes inbox message by uuid.
    /// </summary>
    /// <param name="message">Inbox message.</param>
    public async void DeleteInboxMessageAsync(InAppMessage message)
    {
        var response = await Kinoa.Messaging.DeleteInboxMessageAsync(message);
        Log($"{nameof(DeleteInboxMessageAsync)} request status: {response.Status.ToString()}.");
    }

    /// <summary>
    ///     Deletes multiple inbox In-app messages async.
    /// </summary>
    /// <param name="messages">The In-app messages.</param>
    /// <returns>Processed API response.</returns>
    public async void DeleteInboxMessagesAsync(IEnumerable<InAppMessage> messages)
    {
        var response = await Kinoa.Messaging.DeleteInboxMessagesAsync(messages);
        Log($"{nameof(DeleteInboxMessagesAsync)} request status: {response.Status.ToString()}.");
    }

    /// <summary>
    ///     Creates new in-app message by passed token for the current player.
    /// </summary>
    /// <param name="token">Unique in-app message token.</param>
    public async void CreateInAppMessageAsync(string token)
    {
        var response = await Kinoa.Messaging.CreateInAppMessageAsync(token);
        if (response.IsSuccessful())
            Log($"The in-app message was successfully generated by the passed token: {token}." +
                $"\nMessage ID: {response.Data.Uuid}");
        else
            Log($"{nameof(CreateInAppMessageAsync)} request status: {response.Status.ToString()}." +
                $"\nError code: {(response.Error != null ? response.Error.Code : "Error object is null.")}");
    }

    /// <summary>
    ///     Shows inbox messages on UI (for demonstration purposes only).
    ///     Use your custom UI interface and client notification mechanism.
    /// </summary>
    private static void Log(string message)
    {
        Debug.Log(message);
        NotificationManager.Instance.ShowNotification(message);
    }
}