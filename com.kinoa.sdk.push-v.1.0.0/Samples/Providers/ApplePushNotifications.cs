﻿#if UNITY_IOS
using System;
using System.Text;
using Kinoa.PushNotifications.iOS.Clients;
using Unity.Notifications.iOS;
using UnityEngine;

/// <summary>
///     Apple Push Notification service client sample.
///     By installing a Kinoa Push Notifications, the <see cref="Unity.Notifications"/> features
///     are automatically available and could be used directly to extend the client push notification features
///     e.g <see cref="iOSNotificationCenter.GetLastRespondedNotification"/> <seealso cref="GetLastOpenedNotification"/>.
/// </summary>
public class ApplePushNotifications : ApplePushNotificationClient
{
    /// <summary>
    ///     The authorization options your app is requesting.
    ///     You may specify multiple options to request authorization for.
    ///     Request only the authorization options that you plan to use.
    /// </summary>
    protected override AuthorizationOption AuthorizationOptions =>
        AuthorizationOption.Alert | AuthorizationOption.Badge | AuthorizationOption.Sound;

    /// <summary>
    ///     On push notification service initialization successfully completed.
    /// </summary>
    protected override void OnInitializationCompleted() =>
        Debug.Log("[GAME] APNs initialization is completed. Status: success.");

    /// <summary>
    ///     On push notification service initialization failed.
    /// </summary>
    protected override void OnInitializationFailed() =>
        Debug.Log("[GAME] APNs initialization is completed. Status: failed.");

    /// <summary>
    ///     On whenever a local or a remote notification is received, regardless if it's shown in the foreground.
    ///     <see cref="iOSNotificationCenter.OnNotificationReceived"/>.
    ///     <seealso href="https://docs.unity3d.com/Packages/com.unity.mobile.notifications@2.0/manual/iOS.html"/>.
    ///
    ///     Sample: By default, if your app triggers a local notification while it is in the foreground,
    ///     the device won’t display an alert for that notification.
    ///     If you want the notification to behave as though the device isn’t running the app,
    ///     set the ShowInForeground property to true when you schedule the notification, as shown below.
    /// </summary>
    /// <param name="notification">iOS notification message.</param>
    protected override void OnPushNotificationReceived(iOSNotification notification)
    {
        Debug.Log("[Game] The new push remote notification message successfully received.");

        notification.ShowInForeground = true;
        // In this case you need to specify its 'ForegroundPresentationOption'
        notification.ForegroundPresentationOption = PresentationOption.Sound | PresentationOption.Alert;
    }

    /// <summary>
    ///     On whenever a remote notification is received while the app is in foreground.
    ///     <see cref="iOSNotificationCenter.OnRemoteNotificationReceived"/>.
    ///     <seealso href="https://docs.unity3d.com/Packages/com.unity.mobile.notifications@2.0/manual/iOS.html"/>.
    ///
    ///     Sample: When a remote notification is received, modify its contents and show it after 1 second.
    /// </summary>
    /// <param name="remoteNotification">iOS notification message.</param>
    protected override void OnRemotePushNotificationReceived(iOSNotification remoteNotification)
    {
        Debug.Log("[Game] The remote notification message successfully received.");
        var timeTrigger = new iOSNotificationTimeIntervalTrigger
        {
            TimeInterval = new TimeSpan(0, 0, 1),
            Repeats = false
        };

        var notification = new iOSNotification
        {
            Title = remoteNotification.Title,
            Body = remoteNotification.Body,
            Subtitle = "This subtitle was modified by the client before the notification was shown.",
            ShowInForeground = true,
            ForegroundPresentationOption = PresentationOption.Sound | PresentationOption.Alert,
            CategoryIdentifier = remoteNotification.CategoryIdentifier,
            ThreadIdentifier = remoteNotification.ThreadIdentifier,
            Trigger = timeTrigger,
        };
        iOSNotificationCenter.ScheduleNotification(notification);
    }

    /// <summary>
    ///     On device registration token updated.
    /// </summary>
    /// <param name="token">Updated token for a current device.</param>
    protected override void OnRegistrationTokenUpdated(string token) =>
        Debug.Log($"[GAME] Received APNs device registration Token: {token}");

    /// <summary>
    ///     Sample shows how to get the last opened notification
    ///     using the <see cref="Unity.Notifications.iOS"/> features.
    /// </summary>
    public iOSNotification GetLastOpenedNotification()
    {
        var openedNotification = iOSNotificationCenter.GetLastRespondedNotification();
        if (openedNotification != null)
            Debug.Log("[GAME] The last local or remote notification used to open the app or clicked on by the user: " +
                      $"{openedNotification.Identifier}.");
        else Debug.Log("[GAME] There is no last opened notification available.");

        return openedNotification;
    }

    /// <summary>
    ///     Sample shows how to get delivered notifications that are currently shown in the Notification Center.
    ///     using the <see cref="Unity.Notifications.iOS"/> features.
    /// </summary>
    public iOSNotification[] GetDeliveredNotifications()
    {
        var deliveredNotifications = iOSNotificationCenter.GetDeliveredNotifications();
        LogNotifications(deliveredNotifications, nameof(GetDeliveredNotifications));
        return deliveredNotifications;
    }

    /// <summary>
    ///     Sample shows how to get notifications that are currently scheduled.
    ///     using the <see cref="Unity.Notifications.iOS"/> features.
    /// </summary>
    public iOSNotification[] GetScheduledNotifications()
    {
        var deliveredNotifications = iOSNotificationCenter.GetScheduledNotifications();
        LogNotifications(deliveredNotifications, nameof(GetScheduledNotifications));
        return deliveredNotifications;
    }

    /// <summary>
    ///     Logs the notifications.
    /// </summary>
    /// <param name="notifications">Notification array.</param>
    /// <param name="source">The source of notification array.</param>
    private void LogNotifications(iOSNotification[] notifications, string source)
    {
        var logBuilder = new StringBuilder();
        logBuilder.AppendFormat($"{source}({notifications.Length}):");

        foreach (var notification in notifications)
            logBuilder.AppendFormat($"ID: {notification.Identifier}:");

        Debug.Log(logBuilder.ToString());
    }
}
#endif