﻿using System.Text;
using Firebase.Messaging;
using Kinoa.PushNotifications.Clients;
using UnityEngine;

/// <summary>
///     Firebase Cloud Messaging client sample.
/// </summary>
public class FirebaseCloudMessaging : FirebasePushNotificationClient
{
    /// <summary>
    ///     On push notification service initialization successfully completed.
    /// </summary>
    protected override void OnInitializationCompleted() =>
        Debug.Log("[GAME] FCM initialization is completed. Status: success.");

    /// <summary>
    ///     On push notification service initialization failed.
    /// </summary>
    protected override void OnInitializationFailed() =>
        Debug.Log("[GAME] FCM initialization is completed. Status: failed.");

    /// <summary>
    ///     On push notification message received.
    /// </summary>
    /// <param name="message">Firebase cloud message.</param>
    protected override void OnPushNotificationReceived(FirebaseMessage message)
    {
        var logBuilder = new StringBuilder();
        logBuilder.AppendFormat("[GAME] Received a new push notification message:");

        var notification = message.Notification;
        if (notification != null)
        {
            logBuilder.AppendFormat("\nTitle: {0}", notification.Title);
            logBuilder.AppendFormat("\nBody: {0}", notification.Body);
            var android = notification.Android;
            if (android != null)
                logBuilder.AppendFormat("\nAndroid channel_id: {0}", android.ChannelId);
        }

        if (message.From.Length > 0)
            logBuilder.AppendFormat("\nFrom: {0}", message.From);
        if (message.Link != null)
            logBuilder.AppendFormat("\nLink: {0}", message.Link);

        if (message.Data.Count > 0)
        {
            logBuilder.AppendFormat("\nData:");
            foreach (var iter in message.Data)
                logBuilder.AppendFormat("  " + iter.Key + ": " + iter.Value);
        }

        Debug.Log(logBuilder.ToString());
    }

    /// <summary>
    ///     On device registration token updated.
    /// </summary>
    /// <param name="token">Updated token for a current device.</param>
    protected override void OnRegistrationTokenUpdated(string token)
    {
        Debug.Log($"[GAME] Received FCM device registration Token: {token}");
    }
}