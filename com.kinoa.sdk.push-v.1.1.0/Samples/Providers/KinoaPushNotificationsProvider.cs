﻿/// <summary>
///     Kinoa Push Notifications SDK basic methods and properties provider sample.
/// </summary>
public class KinoaPushNotificationsProvider
{
    /// <summary>
    ///     Kinoa game API base URL. Use game staging or production environment URL address.
    /// </summary>
    private const string KinoaPushNotificationsApiUrl = "https://pntest.kindev.net";
        
    /// <summary>
    ///     Initialize the new instance of <see cref="KinoaPushNotificationsProvider"/>
    /// </summary>
    public KinoaPushNotificationsProvider() => Init();
        
    /// <summary>
    ///     Initialize and configure the Push Notifications  SDK
    /// </summary>
    private void Init()
    {
        Kinoa.PushNotifications.SDK.Initialize(KinoaPushNotificationsApiUrl);
    }
}