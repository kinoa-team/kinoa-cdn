#if UNITY_ANDROID
using System.Collections.Generic;
using Kinoa.PushNotifications.Android.Clients;
using Kinoa.PushNotifications.Android.Data;
using UnityEngine;

/// <summary>
///     Kinoa Android local push notification sample.
/// </summary>
public class KinoaAndroidLocalPushNotificationProvider
{
    /// <summary>
    ///     KinoaAndroidLocalNotificationClient not providing storage for local notification.
    ///     Local storage is needed to be implemented on client side to save and handle scheduled local notification.
    /// </summary>
    public List<int> NotificationLocalStorage { get; }
    
    private readonly KinoaAndroidLocalNotificationClient _localNotificationClient;
    
    public KinoaAndroidLocalPushNotificationProvider()
    {
        _localNotificationClient = new KinoaAndroidLocalNotificationClient();
        NotificationLocalStorage = new List<int>();
        _localNotificationClient.OnLocalPushNotificationReceived += OnLocalPushNotificationReceived;
    }

    /// <summary>
    ///     Sample shows how to handle notification received.
    /// </summary>
    private void OnLocalPushNotificationReceived(KinoaAndroidNotificationWrapper notification)
    {
        Debug.Log($"[GAME] Local push notification received.");
    }

    /// <summary>
    ///     Sample shows how to schedule notification.
    /// </summary>
    public void ScheduleNotification(KinoaAndroidLocalNotificationData localNotificationData)
    {
        var scheduledNotification = _localNotificationClient.ScheduleLocalNotification(localNotificationData);
        if (scheduledNotification != null)
        {
            NotificationLocalStorage.Add(scheduledNotification.Identifier);
        }
    }

    /// <summary>
    ///     Sample shows how to cancel notification.
    /// </summary>
    public void CancelNotification(int notificationId)
    {
        _localNotificationClient.CancelLocalNotification(notificationId);
        NotificationLocalStorage.Remove(notificationId);
    }
}
#endif