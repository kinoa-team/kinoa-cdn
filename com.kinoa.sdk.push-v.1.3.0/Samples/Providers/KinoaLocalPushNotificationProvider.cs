using System.Collections.ObjectModel;
using Kinoa.PushNotifications.Clients;
using Kinoa.PushNotifications.Data;
using Kinoa.PushNotifications.Interfaces;
using Newtonsoft.Json;
using UnityEngine;

/// <summary>
///     Kinoa local push notification sample.
/// </summary>
public class KinoaLocalPushNotificationProvider<T>
{
    /// <summary>
    ///     Local storage. 
    /// </summary>
    public ObservableCollection<string> NotificationLocalStorage { get; }

    public KinoaLocalPushNotificationProvider(ILocalPushNotificationClient<T> localPushNotificationClient)
    {
        KinoaLocalPushNotificationClient<T>.Initialize(localPushNotificationClient);
        NotificationLocalStorage = new ObservableCollection<string>();
        KinoaLocalPushNotificationClient<T>.OnLocalPushNotificationReceived += OnLocalPushNotificationReceived;
    }

    private void OnLocalPushNotificationReceived(KinoaLocalNotification<T> notification)
    {
        var serializeObject = JsonConvert.SerializeObject(notification, Formatting.Indented, new JsonSerializerSettings
        {
            ReferenceLoopHandling = ReferenceLoopHandling.Ignore
        });
        Debug.Log($"[GAME] Local push notification received:  \n{serializeObject}");
    }

    public void ScheduleNotification(KinoaLocalNotificationData localNotificationData)
    {
        var scheduledNotification = KinoaLocalPushNotificationClient<T>.ScheduleNotification(localNotificationData);
        if (scheduledNotification != null)
        {
            NotificationLocalStorage.Add(scheduledNotification.Identifier);
        }
    }

    public void CancelNotification(int index)
    {
        var notificationId = NotificationLocalStorage[index];
        KinoaLocalPushNotificationClient<T>.CancelNotification(notificationId);
        NotificationLocalStorage.Remove(notificationId);
    }
}