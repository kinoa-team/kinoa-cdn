﻿using UnityEngine;

/// <summary>
///     Kinoa Push Notifications SDK basic methods and properties provider sample.
/// </summary>
public class KinoaPushNotificationsProvider
{
    /// <summary>
    ///     Kinoa game API base URL. Use game staging or production environment URL address.
    /// </summary>
    private const string KinoaPushNotificationsApiUrl = "https://pntest.kindev.net";
        
    /// <summary>
    ///     Initialize the new instance of <see cref="KinoaPushNotificationsProvider"/>
    /// </summary>
    public KinoaPushNotificationsProvider() => Init();
        
    /// <summary>
    ///     Initialize and configure the Push Notifications SDK
    /// </summary>
    private void Init()
    {
        Kinoa.PushNotifications.SDK.Initialize(KinoaPushNotificationsApiUrl);
    }

    /// <summary>
    ///     Block player pushes.
    /// </summary>
    public async void BlockPushes()
    {
        var response = await Kinoa.PushNotifications.SDK.BlockPushes();
        Debug.Log($"Kinoa pushes block request completed with status: {response.Status}");
    }
    
    /// <summary>
    ///     Unblock player pushes.
    /// </summary>
    public async void UnblockPushes()
    {
        var response =await Kinoa.PushNotifications.SDK.UnblockPushes();
        Debug.Log($"Kinoa pushes unblock request completed with status: {response.Status}");
    }
    
    /// <summary>
    ///     Check pushes status on Kinoa.
    /// </summary>
    public async void CheckPushStatus()
    {
        var response = await Kinoa.PushNotifications.SDK.GetPushStatus();
        if (response.IsSuccessful())
        {
            Debug.Log($"Kinoa pushes are blocked: {response.Data.Blocked}");
        }
        else
        {
            Debug.Log($"Get push status request status: {response.Status}.");
        }
    }
}