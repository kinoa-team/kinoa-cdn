# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/), and this project adheres
to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.4.0.0]

### Added
- Local storage for Android local push notifications
- Dismiss option for iOS pushes
- Integration with Settings service

## [1.3.0.0]

### Added
- Local push notification:
    - Schedule/cancel local notifications
    - Calendar and interval triggers
- Block/unblock pushes:
    - Block/Unblock Kinoa Push Notifications with REST call
    - Get current status of Kinoa Push Notifications
 

## [1.2.0.0] - 2023-01-19

### Changed

- Kinoa core major update

## [1.1.0.0] - 2022-12-06

### Added

- Apple Push Notification Extension service.

## [1.0.0.0] - 2022-07-11

### Added

- Firebase Cloud Messaging client.
- Apple Push Notification service client.