#if UNITY_ANDROID
using System.Collections.Generic;
using System.Threading.Tasks;
using Kinoa.PushNotifications.Android.Clients;
using Kinoa.PushNotifications.Android.Data;
using UnityEngine;

/// <summary>
///     Kinoa Android local push notification sample.
/// </summary>
public class KinoaAndroidLocalPushNotificationProvider
{
    private KinoaAndroidLocalNotificationClient _localNotificationClient;

    public async Task Initialize()
    {
        _localNotificationClient = await KinoaAndroidLocalNotificationClient.GetInstance();
        _localNotificationClient.OnLocalPushNotificationReceived += OnLocalPushNotificationReceived;
    }

    /// <summary>
    ///     Sample shows how to handle notification received.
    /// </summary>
    private void OnLocalPushNotificationReceived(KinoaAndroidNotificationWrapper notification)
    {
        Debug.Log($"[GAME] Local push notification received.");
    }

    /// <summary>
    ///     Sample shows how to schedule notification.
    /// </summary>
    public void ScheduleNotification(KinoaAndroidLocalNotificationData localNotificationData)
    {
        _localNotificationClient?.ScheduleLocalNotification(localNotificationData);
    }

    /// <summary>
    ///     Sample shows how to cancel notification.
    /// </summary>
    public void CancelNotification(int notificationId)
    {
        _localNotificationClient?.CancelLocalNotification(notificationId);
    }
    
    /// <summary>
    ///     Sample shows how to get notifications that are currently scheduled.
    /// </summary>
    public List<KinoaAndroidScheduledLocalNotificationData> GetScheduledNotifications()
    {
        var scheduledNotifications = _localNotificationClient?.GetScheduledNotifications();
        return scheduledNotifications ?? new List<KinoaAndroidScheduledLocalNotificationData>();
    }
}
#endif