# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.2.0] - 2021-08-30
### Added
- Multiple economies caching system.
- Economies filters: by name, by category.
- SDK testing solution.
- Bug fixes.

## [1.1.0] - 2021-08-09
### Added
- Economies checksum request.
- Economies downloading request.
- Economies caching system.
- Compressed economies files implementation (20kB instead of 7mB).
- Bug fixes.


## [1.0.0] - 2021-07-30
### Added
- The first version of SDK developed.
- All game events are sent and processed via the API and Kinoa SDK for Unity. 
- Event data aggregated by ClickHouse. 
- Kinoa API analyzes data and calculates the audiences dynamically. 


### Changed
- SDK is moved to a separate repository to provide the possibility to instal the SDK through the Unity Package Manager.


### Removed
- SDK is removed from C&F repository.

### Fixed
- All the major and minor bugs were fixed.