using System.Collections.Generic;
using Kinoa.Data.Enum;
using Kinoa.Data.Events;
using Kinoa.Data.State;
using UnityEngine;
using Event = Kinoa.Data.Events.Event;

/// <summary>
///     Kinoa analytics provider sample
/// </summary>
public class KinoaAnalyticsProvider
{
    private const bool IsLogEnabled = true;

    private const string KinoaGameToken = "TYPE_YOUR_GAME_TOKEN_HERE";

    private const string KinoaApiUrl = "https://TYPE_YOYR_COMPANY_DOMAIN_HERE/api/v3";

    /// <summary>
    ///     Bind the actualized player progress with the real one.
    /// </summary>
    private readonly Dictionary<string, int> playerProgress = new Dictionary<string, int>
        {["Solitaire"] = 9, ["Coloring"] = 3};

    /// <summary>
    ///     Bind the actualized player balance with the real one.
    /// </summary>
    private readonly Dictionary<string, decimal> playerBalance = new Dictionary<string, decimal>
        {["Coins"] = 1000000};

    /// <summary>
    ///     Initialize the new instance of <see cref="KinoaAnalyticsProvider"/>
    /// </summary>
    public KinoaAnalyticsProvider() => Init();

    /// <summary>
    ///     Initialize and configure SDK with secret game token.
    /// </summary>
    private static void Init()
    {
        Kinoa.Analytics.SetActiveLog(IsLogEnabled);
        Kinoa.Analytics.SetPlayerChangedHandler(OnActivePlayerChanged);
        Kinoa.Analytics.Initialize(KinoaGameToken, KinoaApiUrl);
    }

    /// <summary>
    ///     On active player identifier changed.
    ///     Use cases: - download economy for the new player <see cref="KinoaEconomiesProvider.EconomyDownload"/>;
    ///     - in-app business logic;
    /// </summary>
    /// <param name="activePlayerId">Active player identifier</param>
    private static void OnActivePlayerChanged(string activePlayerId)
    {
        Debug.Log($"Active player was changed successfully: {activePlayerId}.");
        //TODO: kinoaEconomiesProvider.EconomyDownload(); //Download economy for the new active player
    }

    /// <summary>
    ///     On game install handler
    /// </summary>
    public void OnGameInstall()
    {
        var e = new Event();
        var state = new InitPlayerState(playerProgress, playerBalance);

        state.SetMediaSource(
            new MediaSourceInfo {Channel = "Google", Campaign = "ads"});

        Kinoa.Analytics.Install(e, state);
    }

    /// <summary>
    ///     On game session start handler
    /// </summary>
    public void OnGameSessionStart()
    {
        var e = new Event();
        var state = new InitPlayerState(playerProgress, playerBalance);

        state.SetPlayerNativeID("native-player-id");
        state.SetSocialNetworks(new Dictionary<SocialMediaChanel, string>
        {
            [SocialMediaChanel.facebook] = "test-fb-id",
            [SocialMediaChanel.twitter] = "test-twitter-id"
        });

        Kinoa.Analytics.StartSession(e, state);
    }

    /// <summary>
    ///     On game progress changed handler
    /// </summary>
    /// <param name="success">Is level completed successfully</param>
    public void OnProgressChanged(bool success = true)
    {
        var e = new ProgressionEvent(success);

        e.SetLevel(10);
        e.SetPlace("Solitaire");
        e.SetSpent(new Dictionary<string, decimal> {["Coins"] = 9});
        e.SetReceived(new Dictionary<string, decimal> {["Coins"] = 999});
        e.SetDuration(60L);

        var state = new PlayerState(playerProgress, playerBalance);

        Kinoa.Analytics.Progression(e, state);
    }

    /// <summary>
    ///     On game level up handler
    /// </summary>
    public void OnLevelUp()
    {
        var e = new LevelUpEvent();

        e.SetLevel(10);
        e.SetPlace("Solitaire");
        e.SetSpent(new Dictionary<string, decimal> {["Coins"] = 9});
        e.SetReceived(new Dictionary<string, decimal> {["Coins"] = 999});
        e.SetDuration(60L);

        var state = new PlayerState(playerProgress, playerBalance);

        Kinoa.Analytics.LevelUp(e, state);
    }

    /// <summary>
    ///     On real payment handler
    /// </summary>
    /// <param name="success">Is payment completed successfully</param>
    /// <param name="productId">Shop product identifier in Google Play</param>
    /// <param name="spent">Spent money in USD</param>
    /// <param name="received">Received in-game resources</param>
    public void OnPayment(bool success = true,
        string productId = "play-market-product-id",
        decimal spent = 0.99m,
        Dictionary<string, decimal> received = null)
    {
        var e = new PaymentEvent(success, productId, spent, received);

        e.SetLevel(10);
        e.SetPlace("Solitaire");

        var state = new PlayerState(playerProgress, playerBalance);

        Kinoa.Analytics.Payment(e, state);
    }

    /// <summary>
    ///     On in-game purchase handler
    /// </summary>
    /// <param name="spent">Spent in-game resources</param>
    /// <param name="received">Received in-game resources</param>
    public void OnInGamePurchase(Dictionary<string, decimal> spent = null, Dictionary<string, decimal> received = null)
    {
        var e = new InGamePurchaseEvent(spent, received);

        e.SetLevel(10);
        e.SetPlace("Solitaire");

        var state = new PlayerState(playerProgress, playerBalance);

        Kinoa.Analytics.InGamePurchase(e, state);
    }

    /// <summary>
    ///     On tutorial completed handler
    /// </summary>
    /// <param name="action">Tutorial action</param>
    public void OnTutorial(TutorialAction action = TutorialAction.Finish)
    {
        var e = new TutorialEvent(action);

        e.SetStep(1);
        e.SetLevel(10);
        e.SetPlace("Solitaire");
        e.SetReceived(new Dictionary<string, decimal> {["Coins"] = 100});

        var state = new PlayerState(playerProgress, playerBalance);

        Kinoa.Analytics.Tutorial(e, state);
    }

    /// <summary>
    ///     On player social network connect handler
    /// </summary>
    /// <param name="channels">Social media channels to connect</param>
    public void OnSocialConnect(Dictionary<SocialMediaChanel, string> channels = null)
    {
        var e = new SocialNetworkEvent();

        e.SetLevel(10);
        e.SetPlace("Solitaire");
        e.SetReceived(new Dictionary<string, decimal> {["Coins"] = 100});

        var state = new SocialConnectState(playerProgress, playerBalance, new Dictionary<SocialMediaChanel, string>
        {
            [SocialMediaChanel.facebook] = "test-fb-id-x",
            [SocialMediaChanel.twitter] = "test-twitter-id-x"
        });
        state.SetPlayerNativeID("native-player-id-x");

        Kinoa.Analytics.SocialConnect(e, state);
    }

    /// <summary>
    ///     On player social network disconnect handler
    /// </summary>
    /// <param name="channels">Social media channels to disconnect</param>
    public void OnSocialDisconnect(Dictionary<SocialMediaChanel, string> channels = null)
    {
        var e = new SocialNetworkEvent();

        e.SetLevel(10);
        e.SetPlace("Solitaire");
        e.SetReceived(new Dictionary<string, decimal> {["Coins"] = 100});

        var state = new SocialDisconnectState(playerProgress, playerBalance,
            new[] {SocialMediaChanel.facebook, SocialMediaChanel.twitter});
        state.SetPlayerNativeID("native-player-id");

        Kinoa.Analytics.SocialDisconnect(e, state);
    }

    /// <summary>
    ///     On post in social networks handler
    /// </summary>
    public void OnSocialPost()
    {
        var e = new SocialNetworkEvent();

        e.SetLevel(10);
        e.SetPlace("Solitaire");
        e.SetReceived(new Dictionary<string, decimal> {["Coins"] = 100});

        var state = new PlayerState(playerProgress, playerBalance);

        Kinoa.Analytics.SocialPost(e, state);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="name">Custom event name</param>
    public void OnCustomEvent(string name = "custom_event_name")
    {
        var e = new CustomEvent(name);

        e.SetLevel(10);
        e.SetPlace("Solitaire");
        e.SetSpent(new Dictionary<string, decimal> {["Coins"] = 100});
        e.SetReceived(new Dictionary<string, decimal> {["Diamonds"] = 1});
        e.SetCustomParams(new Dictionary<string, object>
        {
            ["custom_key_1"] = 1.0,
            ["custom_key_2"] = "custom_value_2",
            ["custom_key_3"] = true
        });

        var state = new PlayerState(playerProgress, playerBalance);
        state.SetCustomParams(new Dictionary<string, object>
        {
            ["custom_key_1"] = 1.0,
            ["custom_key_2"] = "custom_value_2",
            ["custom_key_3"] = true
        });

        Kinoa.Analytics.CustomEvent(e, state);
    }
}