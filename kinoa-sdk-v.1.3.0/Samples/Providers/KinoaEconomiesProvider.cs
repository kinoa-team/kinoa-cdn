﻿using Kinoa.Data.Economy;
using UnityEngine;

/// <summary>
///     Kinoa economies provider sample
/// </summary>
public class KinoaEconomiesProvider
{
    /// <summary>
    ///     Get actual player economy checksum
    /// </summary>
    /// <param name="name">Economy name</param>
    public void EconomyChecksum(string name = "default") =>
        Kinoa.Economy.Checksum(name, OnChecksumCallback);

    /// <summary>
    ///     On player economy checksum received game handled callback
    /// </summary>
    /// <param name="success">Is checksum received successfully</param>
    /// <param name="data">Deserialized checksum</param>
    private static void OnChecksumCallback(bool success, EconomyChecksum data = null) =>
        Debug.Log($"Economy checksum received successfully: {success}");

    /// <summary>
    ///     Get actual player economy file
    /// </summary>
    /// <param name="name">Economy name</param>
    public void EconomyDownload(string name = "default") =>
        Kinoa.Economy.Download(name, OnEconomyDownloaded, OnProgressChanged);

    /// <summary>
    ///     On economy downloaded game handled callback
    /// </summary>
    /// <param name="success">Is economy downloaded successfully</param>
    /// <param name="data">Deserialized economy file</param>
    private static void OnEconomyDownloaded(bool success, EconomyFile data = null) =>
        Debug.Log($"Economy downloaded successfully: {success}");

    /// <summary>
    ///     On economy loading progress changed callback
    /// </summary>
    /// <param name="progress">Content loading progress</param>
    private static void OnProgressChanged(decimal progress) =>
        Debug.Log($"Economy loading progress: {progress}");
}