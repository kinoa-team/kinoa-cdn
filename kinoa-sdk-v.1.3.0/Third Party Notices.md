This package contains third-party software components governed by the license(s) indicated below:

Component Name: Newtonsoft.Json-for-Unity

License Type: "MIT"

[Newtonsoft.Json-for-Unity](https://github.com/jilleJr/Newtonsoft.Json-for-Unity/blob/master/LICENSE.md)