using System.Collections.Generic;
using Kinoa.Data.Enum;
using Kinoa.Data.Events;
using Kinoa.Data.State;
using Event = Kinoa.Data.Events.Event;

/// <summary>
///     Kinoa events provider sample
/// </summary>
public class KinoaEventsProvider
{
    /// <summary>
    ///     Bind the actualized player progress with the real one.
    /// </summary>
    private readonly Dictionary<string, int> actualizedProgress = new Dictionary<string, int>
        {["Solitaire"] = 9, ["Coloring"] = 3};

    /// <summary>
    ///     Bind the actualized player balance with the real one.
    /// </summary>
    private readonly Dictionary<string, decimal> actualizedBalance = new Dictionary<string, decimal>
        {["Coins"] = 1000000};

    /// <summary>
    ///     On game session start handler
    /// </summary>
    public void OnGameSessionStart()
    {
        var e = new Event();
        var state = new InitPlayerState();

        state.SetProgress(actualizedProgress);
        state.SetBalance(actualizedBalance);
        state.SetPlayerNativeID("native-player-id");
        state.SetSocialNetworks(new Dictionary<SocialMediaChanel, string>
        {
            [SocialMediaChanel.facebook] = "test-fb-id",
            [SocialMediaChanel.twitter] = "test-twitter-id"
        });
        state.SetBillingPlatform(BillingPlatform.Apple);
        
        Kinoa.Events.StartSession(e, state);
    }

    /// <summary>
    ///     On game progress changed handler
    /// </summary>
    public void OnProgressChanged()
    {
        var e = new ProgressionEvent();

        e.SetLevel(10);
        e.SetPlace("Solitaire");
        e.SetSpent(new Dictionary<string, decimal> {["Coins"] = 9});
        e.SetReceived(new Dictionary<string, decimal> {["Coins"] = 999});
        e.SetDuration(60L);

        var state = new PlayerState();
        state.SetBalance(actualizedBalance);

        Kinoa.Events.Progression(e, state);
    }

    /// <summary>
    ///     On game level up handler
    /// </summary>
    public void OnLevelUp()
    {
        var e = new LevelUpEvent();

        e.SetLevel(10);
        e.SetPlace("Solitaire");
        e.SetSpent(new Dictionary<string, decimal> {["Coins"] = 9});
        e.SetReceived(new Dictionary<string, decimal> {["Coins"] = 999});
        e.SetDuration(60L);

        var state = new PlayerState();
        state.SetProgress(actualizedProgress);
        state.SetBalance(actualizedBalance);

        Kinoa.Events.LevelUp(e, state);
    }

    /// <summary>
    ///     On real payment handler
    /// </summary>
    /// <param name="success">Is payment completed successfully</param>
    /// <param name="productId">Shop product identifier in market</param>
    /// <param name="spent">Spent money in USD</param>
    /// <param name="received">Received in-game resources</param>
    public void OnPayment(bool success = true,
        string productId = "play-market-product-id",
        decimal spent = 0.99m,
        Dictionary<string, decimal> received = null)
    {
        var e = new PaymentEvent(success, productId, spent, received);

        e.SetLevel(10);
        e.SetPlace("Solitaire");

        var state = new PlayerState();
        state.SetBalance(actualizedBalance);

        Kinoa.Events.Payment(e, state);
    }

    /// <summary>
    ///     On in-game purchase handler
    /// </summary>
    /// <param name="spent">Spent in-game resources</param>
    /// <param name="received">Received in-game resources</param>
    public void OnInGamePurchase(Dictionary<string, decimal> spent = null, Dictionary<string, decimal> received = null)
    {
        var e = new InGamePurchaseEvent(spent, received);

        e.SetLevel(10);
        e.SetPlace("Solitaire");

        var state = new PlayerState();
        state.SetBalance(actualizedBalance);

        Kinoa.Events.InGamePurchase(e, state);
    }

    /// <summary>
    ///     On tutorial completed handler
    /// </summary>
    /// <param name="action">Tutorial action</param>
    public void OnTutorial(TutorialAction action = TutorialAction.Finish)
    {
        var e = new TutorialEvent(action);

        e.SetStep(1);
        e.SetLevel(10);
        e.SetPlace("Solitaire");
        e.SetReceived(new Dictionary<string, decimal> {["Coins"] = 100});

        var state = new PlayerState();
        state.SetBalance(actualizedBalance);

        Kinoa.Events.Tutorial(e, state);
    }

    /// <summary>
    ///     On player social network connect handler
    /// </summary>
    /// <param name="channels">Social media channels to connect</param>
    public void OnSocialConnect(Dictionary<SocialMediaChanel, string> channels = null)
    {
        var e = new SocialNetworkEvent();

        e.SetLevel(10);
        e.SetPlace("Solitaire");
        e.SetReceived(new Dictionary<string, decimal> {["Coins"] = 100});

        var state = new SocialConnectState(new Dictionary<SocialMediaChanel, string>
        {
            [SocialMediaChanel.facebook] = "test-fb-id-x",
            [SocialMediaChanel.twitter] = "test-twitter-id-x"
        });
        state.SetPlayerNativeID("native-player-id-x");
        state.SetBalance(actualizedBalance);

        Kinoa.Events.SocialConnect(e, state);
    }

    /// <summary>
    ///     On player social network disconnect handler
    /// </summary>
    /// <param name="channels">Social media channels to disconnect</param>
    public void OnSocialDisconnect(Dictionary<SocialMediaChanel, string> channels = null)
    {
        var e = new SocialNetworkEvent();

        e.SetLevel(10);
        e.SetPlace("Solitaire");
        e.SetReceived(new Dictionary<string, decimal> {["Coins"] = 100});

        var state = new SocialDisconnectState(
            new[] {SocialMediaChanel.facebook, SocialMediaChanel.twitter});
        state.SetPlayerNativeID("native-player-id");
        state.SetBalance(actualizedBalance);

        Kinoa.Events.SocialDisconnect(e, state);
    }

    /// <summary>
    ///     On post in social networks handler
    /// </summary>
    public void OnSocialPost()
    {
        var e = new SocialNetworkEvent();

        e.SetLevel(10);
        e.SetPlace("Solitaire");
        e.SetReceived(new Dictionary<string, decimal> {["Coins"] = 100});

        var state = new PlayerState();
        state.SetBalance(actualizedBalance);

        Kinoa.Events.SocialPost(e, state);
    }

    /// <summary>
    ///     On custom event invoked handler
    /// </summary>
    /// <param name="name">Custom event name</param>
    public void OnCustomEvent(string name = "custom_event_name")
    {
        var e = new CustomEvent(name);

        e.SetLevel(10);
        e.SetPlace("Solitaire");
        e.SetSpent(new Dictionary<string, decimal> {["Coins"] = 100});
        e.SetReceived(new Dictionary<string, decimal> {["Diamonds"] = 1});
        e.SetCustomParams(new Dictionary<string, object>
        {
            ["custom_key_1"] = 1.0,
            ["custom_key_2"] = "custom_value_2",
            ["custom_key_3"] = true
        });

        var state = new PlayerState();
        state.SetCustomParams(new Dictionary<string, object>
        {
            ["custom_key_1"] = 1.0,
            ["custom_key_2"] = "custom_value_2",
            ["custom_key_3"] = true
        });

        Kinoa.Events.Custom(e, state);
    }
}