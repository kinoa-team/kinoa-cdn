﻿using UnityEngine;

/// <summary>
///     Kinoa SDK basic methods and properties provider sample
/// </summary>
public class KinoaBaseProvider
{
    /// <summary>
    ///     Is SDK console logs enabled
    /// </summary>
    private const bool IsLogEnabled = true;

    /// <summary>
    ///    Kinoa game secret token 
    /// </summary>
    private const string KinoaGameToken = "TYPE_YOUR_GAME_TOKEN_HERE";

    /// <summary>
    ///     Kinoa game API base URL. Use game staging or production environment URL address
    /// </summary>
    private const string KinoaApiUrl = "https://TYPE_YOYR_COMPANY_DOMAIN_HERE/api/v3";

    /// <summary>
    ///     Initialize the new instance of <see cref="KinoaBaseProvider"/>
    /// </summary>
    public KinoaBaseProvider() => Init();

    /// <summary>
    ///     Initialize and configure the SDK
    /// </summary>
    private static void Init()
    {
        Kinoa.SDK.SetActiveLog(IsLogEnabled);
        Kinoa.SDK.SetPlayerChangedHandler(OnActivePlayerChanged);
        Kinoa.SDK.Initialize(KinoaGameToken, KinoaApiUrl);
    }

    /// <summary>
    ///     On active player identifier changed.
    ///     Use cases: - download the newest features settings for the new player <see cref="KinoaFeaturesSettingsProvider.Download"/>;
    ///     - in-app business logic;
    /// </summary>
    /// <param name="activePlayerId">Active player identifier</param>
    private static void OnActivePlayerChanged(string activePlayerId)
    {
        Debug.Log($"Active player was changed successfully: {activePlayerId}.");
        //TODO: KinoaFeaturesSettingsProvider.Download(); //Download the newest features settings for the new active player
    }
}