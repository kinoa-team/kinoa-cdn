﻿using Kinoa.Data.FeaturesSettings;
using UnityEngine;

/// <summary>
///     Kinoa features settings provider sample
/// </summary>
public class KinoaFeaturesSettingsProvider
{
    /// <summary>
    ///     Get the newest features settings checksum
    /// </summary>
    public void Checksum() => Kinoa.FeaturesSettings.GetChecksum(OnChecksumReceived);

    /// <summary>
    ///     Get the newest available features settings file
    /// </summary>
    public void Download() => Kinoa.FeaturesSettings.Download(OnFeaturesSettingsDownloaded, OnProgressChanged);

    /// <summary>
    ///     On the features settings checksum received handler
    /// </summary>
    /// <param name="success">Is the request completed successfully</param>
    /// <param name="data">The newest features settings checksum</param>
    private static void OnChecksumReceived(bool success, FeaturesSettingsChecksum data = null) =>
        Debug.Log($"Features settings checksum received successfully: {success}");

    /// <summary>
    ///     On features settings downloaded handler
    /// </summary>
    /// <param name="success">Is the request completed successfully</param>
    /// <param name="data">The newest available features settings file</param>
    private static void OnFeaturesSettingsDownloaded(bool success, FeaturesSettingsFile data = null) =>
        Debug.Log($"Features settings downloaded successfully: {success}");

    /// <summary>
    ///     On features settings loading progress changed handler
    /// </summary>
    /// <param name="progress">Content loading progress</param>
    private static void OnProgressChanged(decimal progress) =>
        Debug.Log($"Features settings loading progress: {progress}");
}