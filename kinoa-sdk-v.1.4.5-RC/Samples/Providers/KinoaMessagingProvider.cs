﻿using Kinoa.Data.Messaging;
using UnityEngine;

/// <summary>
///     Kinoa messaging provider sample
/// </summary>
public class KinoaMessagingProvider
{
    /// <summary>
    ///     Initialize the new instance of <see cref="KinoaMessagingProvider"/>
    /// </summary>
    public KinoaMessagingProvider() => SetMessagingHandler();

    /// <summary>Sets application handled messaging callbacks.</summary>
    public void SetMessagingHandler() => Kinoa.Messaging.SetMessageHandlers(OnCommandReceived, OnInnAppReceived);

    /// <summary>
    ///     On command message received.
    /// </summary>
    /// <param name="message">Command message.</param>
    private static void OnCommandReceived(CommandMessage message) =>
        Debug.Log($"Command message received: {message.Type}: {message.Action}.");

    /// <summary>
    ///     On in-app message received.
    /// </summary>
    /// <param name="message">In-app message.</param>
    private static void OnInnAppReceived(InAppMessage message) =>
        Debug.Log($"In-app message received: {message.Type}.");
}