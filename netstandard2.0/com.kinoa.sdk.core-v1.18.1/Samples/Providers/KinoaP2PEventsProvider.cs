﻿using System.Collections.Generic;
using System.Linq;
using Kinoa.Data;
using Kinoa.Data.P2PEvents;
using UnityEngine;

/// <summary>
///     Kinoa P2P events provider sample.
/// </summary>
public class KinoaP2PEventsProvider
{
    /// <summary>
    ///     The incoming P2P events list.
    /// </summary>
    private List<IncomingP2PEvent> incomingP2PEvents = new List<IncomingP2PEvent>();

    /// <summary>
    ///     The mocked P2P event data.
    ///     Player "A" attacks Player "B" and decrements 11 health units.
    /// </summary>
    private class MockedEventData
    {
        public int Amount { get; set; } = -11;
        public string Resource { get; set; } = "health";
    }

    /// <summary>
    ///     Gets the current list of player's P2P events.
    /// </summary>
    public void Get() => Kinoa.P2PEvents.Get(OnP2PEventsReceived);

    /// <summary>
    ///     Sends P2P event.
    /// </summary>
    public void Send()
    {
        var targetPlayerID = "target_player_ID";
        var eventData = new MockedEventData();

        Kinoa.P2PEvents.Send(new OutgoingP2PEvent(targetPlayerID, "mocked_event", eventData));
    }

    /// <summary>
    ///     Deletes all P2P events from the P2P events list.
    /// </summary>
    public void Delete() => Kinoa.P2PEvents.Delete(
        incomingP2PEvents.Select(c => c.ID).ToList(), OnP2PEventDeleted);


    /// <summary>
    ///     Application handle callback with the updated P2P events list.
    /// </summary>
    /// <param name="response">Processed API response with the incoming P2P events collection.</param>
    private void OnP2PEventsReceived(Response<List<IncomingP2PEvent>> response)
    {
        incomingP2PEvents = response.Data;
        Debug.Log($"All incoming P2P events received successfully: {response.IsSuccessful()}." +
                  $"\nEvents count: {incomingP2PEvents?.Count}");
    }

    /// <summary>
    ///     Handled by application P2P event delete callback.
    /// </summary>
    /// <param name="response">Processed API response.</param>
    private void OnP2PEventDeleted(Response response) =>
        Debug.Log($"P2P event deleted successfully: {response.IsSuccessful()}.");
}