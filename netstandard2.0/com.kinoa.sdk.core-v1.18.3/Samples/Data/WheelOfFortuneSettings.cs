﻿using Kinoa.Data.FeaturesSettings;
using Newtonsoft.Json;

/// <summary>
///     Wheel of Fortune Feature Settings data model.
/// </summary>
public class WheelOfFortuneSettings : FeatureSettingsData
{
    /// <summary>
    ///     The prize reward.
    /// </summary>
    [JsonProperty]
    public string Prize { get; private set; }
}