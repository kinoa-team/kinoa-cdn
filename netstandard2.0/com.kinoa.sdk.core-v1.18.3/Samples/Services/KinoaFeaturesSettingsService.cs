﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Kinoa.Core.Callbacks;
using Kinoa.Core.Utils.Json.Converters;
using Kinoa.Data;
using Kinoa.Data.Enum;
using Kinoa.Data.FeaturesSettings;
using Newtonsoft.Json.Linq;
using UnityEngine;

/// <summary>
///     Kinoa Features Settings service.
/// </summary>
public class KinoaFeaturesSettingsService
{
    /// <summary>
    ///     Downloads Features Settings by the provided request parameters asynchronously.
    ///     Deserialization of Features Settings <see cref="FeatureSettingsResponse{TData}.Data"/>
    ///     and its performance are handled on the client side.
    /// </summary>
    /// <param name="settingsRequestParams">Features Settings request parameters.</param>
    /// <param name="cancellationToken">Cancellation token.</param>
    /// <param name="downloadProgressChangedCallback">Callback function that is invoked on download progress change.</param>
    /// <returns>
    ///     Requested Features Settings <see cref="FeatureSettingsResponse{TData}.Data"/>
    ///     type of JObject <see cref="Newtonsoft.Json.Linq.JObject"/>.
    /// </returns>
    public async Task<Response<FeaturesSettingsResponse<JObject>>> DownloadAsync(
        List<FeatureSettingsRequestParams> settingsRequestParams, CancellationToken cancellationToken = default,
        ProgressChangedCallback downloadProgressChangedCallback = null)
    {
        if (settingsRequestParams == null)
        {
            settingsRequestParams = new List<FeatureSettingsRequestParams>
            {
                new FeatureSettingsRequestParams(key: "DailyBonus", version: 1),
                new FeatureSettingsRequestParams(key: "WheelOfFortune", version: 1)
            };
        }

        if (cancellationToken == default)
        {
            var cancellationTokenSource = new CancellationTokenSource();
            cancellationTokenSource.CancelAfter(10 * 1000);
            cancellationToken = cancellationTokenSource.Token;
        }

        var response = await Kinoa.FeaturesSettings.DownloadAsync(
            settingsRequestParams, cancellationToken, downloadProgressChangedCallback ?? OnDownloadProgressChanged);

        if (response.IsSuccessful())
        {
            var log = new StringBuilder();
            var settingsLog = response.Data.Settings.Select(x =>
                $"Key: {x.Request.Key}. Version: {x.Request.Version}. Status: {x.Status.ToString()}.").ToArray();
            log.AppendFormat("The requested Features Settings were received successfully: " +
                             $"\n{string.Join("\n", settingsLog)}'");

            var dailyBonusSettings =
                response.Data.Settings.FirstOrDefault(x => x.Request.Key == "DailyBonus");
            if (dailyBonusSettings != null && dailyBonusSettings.Status == FeatureSettingsResponseStatus.Ok)
            {
                log.AppendFormat("\n\nDaily Bonus settings:");
                foreach (var settings in dailyBonusSettings.Data)
                {
                    //TODO: Convert Feature Settings JObject to a DailyBonusSettings client data model.
                    log.AppendFormat("\nJObject: {0}", settings);
                }
            }

            var wheelOfFortuneSettings =
                response.Data.Settings.FirstOrDefault(x => x.Request.Key == "WheelOfFortune");
            if (wheelOfFortuneSettings != null && wheelOfFortuneSettings.Status == FeatureSettingsResponseStatus.Ok)
            {
                log.AppendFormat("\n\nWheel of Fortune settings:");
                foreach (var settings in wheelOfFortuneSettings.Data)
                {
                    //TODO: Convert Feature Settings JObject to a WheelOfFortuneSettings client data model.
                    log.AppendFormat("\nJObject: {0}", settings);
                }
            }

            log.AppendFormat("\n");
            Debug.Log(log.ToString());
            log.Clear();
        }

        return response;
    }

    /// <summary>
    ///     Downloads Features Settings by the provided request parameters asynchronously.
    ///     Deserialization of Features Settings <see cref="FeatureSettingsResponse{TData}.Data"/>
    ///     and its performance are handled on the SDK side.
    /// </summary>
    /// <param name="settingsRequestParams">Features Settings request parameters.</param>
    /// <param name="converter">Feature Settings custom creation converter.</param>
    /// <param name="cancellationToken">Cancellation token.</param>
    /// <param name="downloadProgressChangedCallback">Callback function that is invoked on download progress change.</param>
    /// <returns>
    ///     Requested Features Settings <see cref="FeatureSettingsResponse{TData}.Data"/>
    ///     type of FeatureSettingsData <see cref="FeatureSettingsData"/>.
    /// </returns>
    public async Task<Response<FeaturesSettingsResponse<FeatureSettingsData>>> DownloadAsync(
        List<FeatureSettingsRequestParams> settingsRequestParams, FeatureSettingsCreationConverter converter,
        CancellationToken cancellationToken = default,
        ProgressChangedCallback downloadProgressChangedCallback = null)
    {
        if (settingsRequestParams == null)
        {
            settingsRequestParams = new List<FeatureSettingsRequestParams>
            {
                new FeatureSettingsRequestParams(key: "DailyBonus", version: 1),
                new FeatureSettingsRequestParams(key: "WheelOfFortune", version: 1)
            };
        }

        if (converter == null)
        {
            converter = new FeatureSettingsCustomCreationConverter();
        }

        if (cancellationToken == default)
        {
            var cancellationTokenSource = new CancellationTokenSource();
            cancellationTokenSource.CancelAfter(10 * 1000);
            cancellationToken = cancellationTokenSource.Token;
        }

        var response = await Kinoa.FeaturesSettings.DownloadAsync(
            settingsRequestParams, converter, cancellationToken,
            downloadProgressChangedCallback ?? OnDownloadProgressChanged);

        if (response.IsSuccessful())
        {
            var log = new StringBuilder();
            var settingsLog = response.Data.Settings.Select(x =>
                $"Key: {x.Request.Key}. Version: {x.Request.Version}. Status: {x.Status.ToString()}.").ToArray();
            log.AppendFormat("The requested Features Settings were received successfully: " +
                             $"\n{string.Join("\n", settingsLog)}'");

            var dailyBonusSettings =
                response.Data.Settings.FirstOrDefault(x => x.Request.Key == "DailyBonus");
            if (dailyBonusSettings != null && dailyBonusSettings.Status == FeatureSettingsResponseStatus.Ok)
            {
                log.AppendFormat("\n\nDaily Bonus settings:");
                foreach (var settings in dailyBonusSettings.Data)
                {
                    log.AppendFormat($"\nCoins: {((DailyBonusSettings) settings).Coins}");
                }
            }

            var wheelOfFortuneSettings =
                response.Data.Settings.FirstOrDefault(x => x.Request.Key == "WheelOfFortune");
            if (wheelOfFortuneSettings != null && wheelOfFortuneSettings.Status == FeatureSettingsResponseStatus.Ok)
            {
                log.AppendFormat("\n\nWheel of Fortune settings:");
                foreach (var settings in wheelOfFortuneSettings.Data)
                {
                    log.AppendFormat($"\nPrize: {((WheelOfFortuneSettings) settings).Prize}");
                }
            }

            log.AppendFormat("\n");
            Debug.Log(log.ToString());
            log.Clear();
        }

        return response;
    }

    /// <summary>
    ///     On Features Settings download progress changed.
    /// </summary>
    /// <param name="progress">Content download progress.</param>
    private static void OnDownloadProgressChanged(decimal progress)
    {
        Debug.Log($"Features Settings download progress changed: {progress}");
    }
}