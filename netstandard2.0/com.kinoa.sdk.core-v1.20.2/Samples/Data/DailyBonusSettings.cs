﻿using Kinoa.Data.FeaturesSettings;
using Newtonsoft.Json;

/// <summary>
///     Daily Bonus Feature Settings data model.
/// </summary>
public class DailyBonusSettings : FeatureSettingsData
{
    /// <summary>
    ///     The coins reward.
    /// </summary>
    [JsonProperty]
    public double Coins { get; private set; }
}