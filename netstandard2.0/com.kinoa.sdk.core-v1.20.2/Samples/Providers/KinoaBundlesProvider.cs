using System.Linq;
using System.Text;
using UnityEngine;

/// <summary>
///     Kinoa bundles provider sample.
/// </summary>
public class KinoaBundlesProvider
{
    public async void GetBundle(string bundleKey)
    {
        var response = await Kinoa.Bundles.GetBundleResources(bundleKey);

        if (response.IsSuccessful() && response.Data != null && response.Data.Count > 0)
        {
            var resources = response.Data
                .Select(y => $"{y.ResourceKey}: {y.Amount}")
                .ToArray();

            var logBuilder = new StringBuilder();
            logBuilder.Append($"Bundle resources received successfully: ({response.Data.Count})." +
                              $"    \n{string.Join("    \n", resources)}");
            Debug.Log(logBuilder.ToString());
        }
        else
        {
            Debug.Log($"Get bundle resources request status: {response.Status.ToString()}.");
        }
    }
}
