﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading;
using System.Threading.Tasks;
using Kinoa.Core.Callbacks;
using Kinoa.Data;
using Kinoa.Data.Enum;
using Kinoa.Data.FeaturesSettings;
using UnityEngine;

/// <summary>
///     Kinoa Features Settings service.
/// </summary>
public class KinoaFeaturesSettingsService
{
    /// <summary>
    ///     Downloads Features Settings by the provided request parameters asynchronously.
    /// </summary>
    /// <param name="settingsRequestParams">Features Settings request parameters.</param>
    /// <param name="cancellationToken">Cancellation token.</param>
    /// <param name="downloadProgressChangedCallback">Callback function that is invoked on download progress change.</param>
    /// <typeparam name="TData">
    ///     Represents the Feature Settings polymorphic data model type,
    ///     deserialized based on the "$type" TypeDiscriminator property,
    ///     where "$type" is the requested Feature Settings key <see cref="FeatureSettingsRequestParams.Key"/>.
    ///<remarks>
    ///     Apply the <see cref="JsonPolymorphicAttribute"/> for the provided <see cref="TData"/> type
    ///     to enable the polymorphic deserialization.
    /// </remarks>
    /// <remarks>
    ///     In this example, <see cref="FeatureSettingsData"/> is the base polymorphic type for all Feature Settings data models.
    ///     You can change the type, by providing the <see cref="TData"/> instead of hardcoded <see cref="FeatureSettingsData"/>.
    /// </remarks>
    /// </typeparam>
    /// <returns>The requested Features Settings type of <see cref="TData"/>.</returns>
    public async Task<Response<FeaturesSettingsResponse<FeatureSettingsData>>> DownloadAsync<TData>(
        List<FeatureSettingsRequestParams> settingsRequestParams, CancellationToken cancellationToken = default,
        ProgressChangedCallback downloadProgressChangedCallback = null)
    {
        if (settingsRequestParams == null)
        {
            settingsRequestParams = new List<FeatureSettingsRequestParams>
            {
                new FeatureSettingsRequestParams(key: "DailyBonus", version: 1, getDefault: false),
                new FeatureSettingsRequestParams(key: "WheelOfFortune", version: 1, getDefault: false)
            };
        }

        if (cancellationToken == default)
        {
            var cancellationTokenSource = new CancellationTokenSource();
            cancellationTokenSource.CancelAfter(10 * 1000);
            cancellationToken = cancellationTokenSource.Token;
        }

        var response = await Kinoa.FeaturesSettings.DownloadAsync<FeatureSettingsData>(
            settingsRequestParams, cancellationToken, downloadProgressChangedCallback ?? OnDownloadProgressChanged);
        if (response.IsSuccessful())
        {
            LogSettings(response.Data.Settings);
        }

        return response;
    }

    /// <summary>
    ///     On Features Settings download progress changed.
    /// </summary>
    /// <param name="progress">Content download progress.</param>
    private static void OnDownloadProgressChanged(decimal progress)
    {
        Debug.Log($"Features Settings download progress changed: {progress}");
    }

    /// <summary>
    ///     Gets Built-in Features Settings by the provided request parameters asynchronously.
    /// </summary>
    /// <param name="settingsRequestParams">Features Settings request parameters.</param>
    /// <typeparam name="TData">
    ///     Represents the Feature Settings polymorphic data model type,
    ///     deserialized based on the "$type" TypeDiscriminator property,
    ///     where "$type" is the requested Feature Settings key <see cref="FeatureSettingsRequestParams.Key"/>.
    ///<remarks>
    ///     Apply the <see cref="JsonPolymorphicAttribute"/> for the provided <see cref="TData"/> type
    ///     to enable the polymorphic deserialization.
    /// </remarks>
    /// </typeparam>
    /// <returns>The requested Features Settings type of <see cref="TData"/>.</returns>
    public async Task<Response<FeaturesSettingsResponse<FeatureSettingsData>>> GetBuiltInAsync<TData>(
        List<BuiltInFeatureSettingsRequestParams> settingsRequestParams)
    {
        if (settingsRequestParams == null)
        {
            settingsRequestParams = new List<BuiltInFeatureSettingsRequestParams>
            {
                new BuiltInFeatureSettingsRequestParams(key: "DailyBonus"),
                new BuiltInFeatureSettingsRequestParams(key: "WheelOfFortune")
            };
        }

        var response = await Kinoa.FeaturesSettings.GetBuiltInAsync<FeatureSettingsData>(settingsRequestParams);
        if (response.IsSuccessful())
        {
            LogSettings(response.Data.Settings);
        }

        return response;
    }

    /// <summary>
    ///     Logs the requested Features Settings.
    /// </summary>
    /// <param name="settings">The Feature Settings collection.</param>
    private void LogSettings(IEnumerable<FeatureSettingsResponse<FeatureSettingsData>> settings)
    {
        var settingsList = settings?.ToList();
        if (settingsList == null || !settingsList.Any())
        {
            return;
        }

        var briefLog = new StringBuilder();
        briefLog.AppendFormat("The requested Features Settings were received:");
        foreach (var setting in settingsList)
        {
            briefLog.AppendFormat($"\nKey: {setting.Request.Key}. " +
                                  $"Version: {setting.Request.Version}. " +
                                  $"Status: {setting.Status.ToString()}.");
        }

        Debug.Log(briefLog.ToString());
        briefLog.Clear();

        var dailyBonusSettings =
            settingsList.FirstOrDefault(x => x.Request.Key == "DailyBonus");
        if (dailyBonusSettings != null && dailyBonusSettings.Status == FeatureSettingsResponseStatus.Ok &&
            dailyBonusSettings.Data.Any())
        {
            var log = new StringBuilder();
            log.AppendFormat("\tDaily Bonus Settings:");
            foreach (var data in dailyBonusSettings.Data)
            {
                log.AppendFormat($"\nCoins: {((DailyBonusSettings) data).Coins}");
            }

            Debug.Log(log.ToString());
            log.Clear();
        }

        var wheelOfFortuneSettings =
            settingsList.FirstOrDefault(x => x.Request.Key == "WheelOfFortune");
        if (wheelOfFortuneSettings != null && wheelOfFortuneSettings.Status == FeatureSettingsResponseStatus.Ok &&
            wheelOfFortuneSettings.Data.Any())
        {
            var log = new StringBuilder();
            log.AppendFormat("\tWheel of Fortune Settings:");
            foreach (var data in wheelOfFortuneSettings.Data)
            {
                log.AppendFormat($"\nPrize: {((WheelOfFortuneSettings) data).Prize}");
                log.AppendFormat($"\nCoins: {((WheelOfFortuneSettings) data).Coins}");
            }

            Debug.Log(log.ToString());
            log.Clear();
        }
    }
}