﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Kinoa.Core.Network;
using Kinoa.Data;
using Kinoa.Data.State;
using UnityEngine;

/// <summary>
///     The sample Player Account service used to manage the Player accounts.
/// </summary>
public class KinoaPlayerAccountService : KinoaSingleton<KinoaPlayerAccountService>
{
    /// <summary>
    ///     Gets and sets an Active Player identifier <see cref="Kinoa.Player.ID"/>
    /// </summary>
    public string ActivePlayerID
    {
        get => Kinoa.Player.ID;
        set => Kinoa.Player.ID = value;
    }

    /// <summary>
    ///     Log in to the Player account.
    /// </summary>
    public void LogInPlayer()
    {
        //TODO: Identify the active Player ID using the internal login mechanism.
        var loggedInPlayerId = GetLoggedInPlayerId();
        if (!string.IsNullOrEmpty(loggedInPlayerId))
        {
            if (loggedInPlayerId == ActivePlayerID)
            {
                Debug.Log($"The Player from the last Game Session is already logged in: {loggedInPlayerId}");
                return;
            }

            Debug.Log($"The Active Player ID is updated: Old: {ActivePlayerID} -> New: {loggedInPlayerId}");
            ActivePlayerID = loggedInPlayerId;
            return;
        }

        //TODO: Implement the new Player ID generation logic.
        // Recommended to use your Player ID as Kinoa Player ID.
        Debug.Log("The Active Player ID is not identified: The new Player is logged in.");
        ActivePlayerID = Guid.NewGuid().ToString();
    }

    /// <summary>
    ///     Log in to the Player account.
    /// </summary>
    public async Task LogInPlayerWithRecovery()
    {
        // When identifying the active Player ID using the internal login mechanism is not possible.
        await LogInPlayerByRelatedAccountsAsync();
    }

    /// <summary>
    ///     Gets the logged in Player ID based on the game login data.
    /// </summary>
    /// <returns>The logged in Player ID.</returns>
    private string GetLoggedInPlayerId()
    {
        //TODO: Identify the active Player ID based on your internal game data e.g., using the PlayerPrefs or any other login mechanisms.
        var loggedInPlayerId = PlayerPrefs.GetString("ActivePlayerID", null);

        //TODO: You can use the Kinoa.Player.ID as a source of the last Active Player ID across the game launches.
        return !string.IsNullOrEmpty(loggedInPlayerId) ? loggedInPlayerId : Kinoa.Player.ID;
    }

    /// <summary>
    ///     Deletes the Kinoa Player.
    /// </summary>
    /// <returns>The response object.</returns>
    public Task<Response> DeletePlayer(string playerID)
    {
        return Kinoa.Player.DeletePlayerAsync(playerID);
    }

    /// <summary>
    ///     Log in to the Player account by the Kinoa related accounts asynchronously.
    ///     <remarks>
    ///     Use the related accounts to identify the logged in Player
    ///     only if the Active Player ID cannot be identified with the game login mechanism.
    ///     </remarks>
    /// </summary>
    public async Task LogInPlayerByRelatedAccountsAsync()
    {
        var response = await GetRelatedAccountsAsync();
        if (response.IsSuccessful())
        {
            var playerRelatedAccounts = response.Data;
            if (playerRelatedAccounts != null && playerRelatedAccounts.Count > 0)
            {
                //TODO: Implement the logic to choose the logged in Player among the related accounts.
                var loggedInPlayer = playerRelatedAccounts[0].PlayerId;
                Debug.Log($"The Active Player is successfully logged in: {loggedInPlayer}");
                ActivePlayerID = loggedInPlayer;
            }
            else
            {
                //TODO: Implement the new Player ID generation logic.
                // Recommended to use your Player ID as Kinoa Player ID.
                Debug.Log("The Active Player ID is not identified: No related accounts found.");
                ActivePlayerID = Guid.NewGuid().ToString();
            }
        }
        else if (response.IsConnectionError())
        {
            //TODO: Process the failed request e.g., show an connection error dialog to the client.
            Debug.Log("The Active Player ID is not identified: Connection error.");
        }
        else
        {
            //TODO: Process the failed request e.g., show an error message to the client.
            Debug.Log("The Active Player ID is not identified: Get related accounts request is failed.");
        }
    }

    /// <summary>
    ///     Gets all player-related accounts asynchronously.
    /// </summary>
    /// <param name="playerSearchIDs">The known Player IDs.</param>
    /// <returns>The Player related accounts.</returns>
    public async Task<Response<List<PlayerRelatedAccount>>> GetRelatedAccountsAsync(
        PlayerSearchIDs playerSearchIDs = null)
    {
        var response = await Kinoa.Player.GetRelatedAccountsAsync(
            playerSearchIDs ?? new PlayerSearchIDs(ActivePlayerID));
        Debug.Log($"All player-related accounts received successfully: {response.IsSuccessful()}.\n" +
                  $"Related accounts: {response.Data?.Count}");

        return response;
    }

    /// <summary>
    ///     Creates the new Kinoa player using passed identifiers.
    /// </summary>
    [Obsolete("Use the Kinoa.GameSession.OpenSessionAsync instead." +
              "The Player will be created automatically after the first session open request if it's not created yet.")]
    public async Task<Response<string>> CreateNewPlayerAsync(
        string playerId = null,
        string nativeId = null,
        string facebookId = null,
        string googleId = null,
        string appleId = null,
        string extraId1 = null,
        string extraId2 = null,
        string extraId3 = null,
        bool isTester = false)
    {
        var response = await Kinoa.Player.CreateAsync(new PlayerCreateIDs(
            playerId, nativeId, facebookId, googleId, appleId, extraId1, extraId2, extraId3, isTester));
        if (response.IsSuccessful())
        {
            var newPlayerID = response.Data;
            Debug.Log("The new Kinoa player was created successfully: True.\n" +
                      $"The new Kinoa player ID: {newPlayerID}");

            ActivePlayerID = newPlayerID;
        }
        else if (response.IsResponseFailed())
        {
            if (response.Error?.Code == ResponseErrorCode.PlayerAlreadyExists)
            {
                Debug.Log("The new Kinoa player was not created. Player with given PlayerID is already exists.");
            }
        }

        return response;
    }

    /// <summary>
    ///     Creates the new Kinoa Tester player using passed identifiers.
    /// </summary>
    [Obsolete("Use the Kinoa.GameSession.OpenSessionAsync instead." +
              "The Player will be created automatically after the first session open request if it's not created yet.")]
    public Task<Response<string>> CreateNewTesterPlayerAsync(
        string playerId = null,
        string nativeId = null,
        string facebookId = null,
        string googleId = null,
        string appleId = null,
        string extraId1 = null,
        string extraId2 = null,
        string extraId3 = null)
    {
        return CreateNewPlayerAsync(
            playerId, nativeId, facebookId, googleId, appleId, extraId1, extraId2, extraId3, true);
    }
}