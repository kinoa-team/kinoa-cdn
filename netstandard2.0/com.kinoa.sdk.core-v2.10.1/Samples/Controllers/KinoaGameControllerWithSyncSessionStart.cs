﻿using System.Threading.Tasks;
using Kinoa.Data;
using UnityEngine;

/// <summary>
///     The sample Game Session Controller used to manage the Game Session.
/// </summary>
public class KinoaGameControllerWithSyncSessionStart : MonoBehaviour
{
    // Loading overlay sample.
    public GameObject overlay;
    
    //// <summary>
    ///     On Start, initializes the Kinoa SDK, In-app Messaging, Push Notifications, and opens the Game Session.
    /// </summary>
    private async void Start()
    {
        if (KinoaSdkInitService.Instance.IsInitialized)
            return;

        using (new KinoaOverlay(overlay))
        {
            //Step 0 - Initialize the Kinoa SDK, and In-app Messaging services.
            await KinoaSdkInitService.Instance.InitializeAsync();
            await KinoaMessagingService.Instance.InitializeAsync();

            await LogInAndOpenSession();
        }
    }

    /// <summary>
    ///     Log in to the Player account and open Game Session.
    /// </summary>
    public async Task LogInAndOpenSession()
    {
        //Step 1 - Set Active Player ID (Log in).
        KinoaPlayerAccountService.Instance.LogInPlayer();

        //Step 2 - Get Player State of an Active Player.
        var playerState = await KinoaPlayerStateService<CustomPlayerState>.Instance.GetPlayerStateAsync();

        //Step 3 - Open the New Game Session with the Actual Player State.
        await OpenGameSessionAsync(playerState, new GameSessionData());
    }

    /// <summary>
    ///     Sample method to retry opening of the failed Game Session, e.g., on returning to the lobby.
    ///     <remarks>
    ///     The method uses the <see cref="Kinoa.GameSession.ActiveSession"/> to retry the opening of the active Game Session
    ///     and save the same Session ID <see cref="GameSessionData.ID"/> for old and new Game Events.
    ///     </remarks>
    /// </summary>
    public async void RetryGameSessionOpen()
    {
        var gameSession = Kinoa.GameSession.ActiveSession ?? new GameSessionData();
        if (gameSession.IsOpened)
        {
            Debug.Log("The active Game Session is already opened, no need to retry.");
            return;
        }

        Debug.Log("The active Game Session is not opened, need to retry.");
        await OpenGameSessionAsync(KinoaPlayerStateService<CustomPlayerState>.Instance.PlayerState, gameSession);
    }

    /// <summary>
    ///     Sends the Session Open SDK request.
    ///     Sends the Session Start Game Event.
    /// </summary>
    /// <param name="playerState">The Player State.</param>
    /// <param name="gameSessionData">The Game Session Data.</param>
    private async Task OpenGameSessionAsync(CustomPlayerState playerState, GameSessionData gameSessionData)
    {
        //Open the Game Session.
        var response = await KinoaGameSessionService.Instance.OpenSessionAsync(playerState, gameSessionData);
        if (response.IsSuccessful())
        {
            //Send the Session Start Game Event.
            await SendSessionStartEventAsync(playerState);
        }
        else
        {
            //TODO: Implement the retry logic on session opening failed e.g. on coming back to the lobby.
            //TODO: See RetryGameSessionOpenAsync sample method.
        }
    }

    /// <summary>
    ///     Sends the Session Start Game Event.
    ///     <remarks>
    ///     <see cref="Kinoa.SyncGameEvents.SendSessionStartEventAsync{TPlayerState}"/> (Sync API) - returns the In-app messages in the response.
    ///     <see cref="Kinoa.GameEvents.SendSessionStartEvent{TPlayerState}"/> (Async API) - returns the In-app messages asynchronously via Web Socket channel.
    ///     </remarks>
    /// </summary>
    /// <param name="playerState">The Player State.</param>
    private async Task SendSessionStartEventAsync(CustomPlayerState playerState)
    {
        //Send the Session Start Game Event using the Sync API.
        await KinoaSyncGameEventsService<CustomPlayerState>.Instance.SendSessionStartEventAsync(playerState);
    }
}