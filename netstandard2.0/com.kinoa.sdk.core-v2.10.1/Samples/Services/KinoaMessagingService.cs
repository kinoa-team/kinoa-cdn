﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Kinoa.Core.Network;
using Kinoa.Data;
using Kinoa.Data.Messaging.Command;
using Kinoa.Data.Messaging.InApp;
using Kinoa.Data.Messaging.InApp.Capping;
using Kinoa.Data.Messaging.InApp.CreationParams;
using Kinoa.Data.Messaging.InApp.Templates.Custom;
using Kinoa.Data.Messaging.InApp.Templates.Simple;
using Kinoa.Data.Network;
using Kinoa.Data.ResourceManagement;
using Kinoa.Data.Security;
using UnityEngine;
using Utils;

/// <summary>
///     The sample implementation of the Kinoa Messaging service.
/// </summary>
public class KinoaMessagingService : KinoaSingleton<KinoaMessagingService>
{
    /// <summary>
    ///     Delegate for showing inbox messages for demonstration purposes only.
    ///     Use your custom UI interface and client notification mechanism.
    /// </summary>
    public event Action<List<InAppMessage>, string> LogNotifications = delegate { };

    /// <summary>
    ///     Local inbox storage.
    /// </summary>
    public List<InAppMessage> LocalInboxStorage { get; set; } = new List<InAppMessage>();

    /// <summary>
    ///     Initialize and configure the SDK asynchronously.
    /// </summary>
    public async Task InitializeAsync()
    {
        //Add the In-app security configuration.
        var inAppSecurityConfiguration = new InAppSecurityConfiguration(true);
        await Kinoa.Messaging.Initialize(inAppSecurityConfiguration);

        //Set the game-handled WebSocket messaging callbacks.
        Kinoa.Messaging.SetMessageHandlers(OnCommandReceived, OnInAppReceived);
    }

    /// <summary>
    ///     Handles the received Command message.
    /// </summary>
    /// <param name="message">Command message.</param>
    private void OnCommandReceived(CommandMessage message)
    {
        var uuid = $"\n\nMessage UUID: {message.Uuid}";
        switch (message.Command)
        {
            case ReloadP2PCommand cmd:
                Log($"Command type of {nameof(ReloadP2PCommand)} is received:" +
                    $"\nMessage UUID:'{uuid}'");
                break;
            case RemovedInboxInAppsCommand cmd:
                Log($"Command type of {nameof(RemovedInboxInAppsCommand)} is received:" +
                    $"\n{string.Join(",\n", cmd.InApps.Select(o => $"Inbox UUID: {o.Uuid.ToString()}").ToArray())}'");
                break;
            default:
                Log($"Message type of {message.GetType()} is received: ID: {message.Uuid}");
                break;
        }
    }

    /// <summary>
    ///     Handles the received In-app messages.
    /// </summary>
    /// <param name="messages">In-app messages.</param>
    private void OnInAppReceived(InAppMessages messages)
    {
        LogReceivedInApp(messages.Data);
    }

    /// <summary>
    ///     Gets the list of all inbox messages async.
    /// </summary>
    public async void GetInboxMessagesAsync()
    {
        var logBuilder = new StringBuilder();
        var response = await Kinoa.Messaging.GetInboxMessagesAsync();
        if (response.Status == ResponseState.Success && response.Data != null)
        {
            var simpleInAppMessages =
                response.Data.Where(x => x.Data.GetType() == typeof(InAppSimpleTemplateData)).ToList();
            var customInAppMessages =
                response.Data.Where(x => x.Data.GetType() == typeof(InAppCustomTemplateData)).ToList();

            logBuilder.Append($"Inbox messages received successfully: ({response.Data.Count})." +
                              $"\nSimple In-app messages ({simpleInAppMessages.Count})" +
                              $"\nCustom In-app messages ({customInAppMessages.Count})");

            LocalInboxStorage = response.Data;
        }
        else logBuilder.Append($"Get inbox messages request status: {response.Status.ToString()}.");

        Log(logBuilder.ToString());
    }

    /// <summary>
    ///     Uses the eligibility of the inbox In-app message.
    /// </summary>
    /// <param name="message">The inbox message.</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>In-app eligibility update response.</returns>
    public async Task<Response<InAppEligibilityUpdateResult>> UseInboxMessageEligibilityAsync(InAppMessage message,
        CancellationToken cancellationToken = default)
    {
        if (message.Capping?.EligibilityLimit == null)
        {
            Log($"The In-app message '{message.Name}' '{message.Uuid}' has no eligibility limit, " +
                "and is limited by the countdown timer only.");
            return null;
        }

        var response = await Kinoa.Messaging.UseInboxMessageEligibilityAsync(message, cancellationToken);
        if (response.IsSuccessful() && response.Data != null)
        {
            message.SetLocalEligibility(response.Data.ActualEligibility);
            if (response.Data.Processed)
            {
                //TODO: Give the reward to the Player.
                Log($"The In-app message '{message.Name}' '{message.Uuid}' eligibility is successfully used." +
                    $"\nActual eligibility: {response.Data.ActualEligibility}" +
                    $"\nIs deleted: {response.Data.Deleted}");
            }
            else
            {
                Log($"The In-app message '{message.Uuid}' eligibility cannot used (limit is reached)." +
                    $"\nActual eligibility: {response.Data.ActualEligibility}" +
                    $"\nIs deleted: {response.Data.Deleted}");
            }

            if (response.Data.Deleted)
            {
                //TODO: Remove the In-app message from the local inbox storage.
                LocalInboxStorage.Remove(message);
            }
        }
        else
        {
            Log($"{nameof(UseInboxMessageEligibilityAsync)} request status: {response.Status.ToString()}.");
        }

        return response;
    }

    /// <summary>
    ///     Updates inbox In-app message async.
    /// </summary>
    /// <param name="message">Inbox In-app message.</param>
    /// <returns>Processed API response.</returns>
    public async void UpdateInboxMessageAsync(InAppMessage message)
    {
        var endTimestampUpdate = DateTimeOffset.UtcNow.AddSeconds(60).ToUnixTimeSeconds();
        var customParams = new Dictionary<string, object>
        {
            ["custom_key_1"] = 1.0,
            ["custom_key_2"] = "custom_value_2",
            ["custom_key_3"] = true
        };
        message
            .SetCustomParameters(customParams)
            .SetViewsMetrics(message.InboxStats.Views + 1)
            .SetUsageMetrics(message.InboxStats.Usage + 1)
            .SetCountdownTimerEndTimestamp(endTimestampUpdate)
            .ResetRemindersMetrics();

        var response = await Kinoa.Messaging.UpdateInboxMessageAsync(message);
        Log($"{nameof(UpdateInboxMessageAsync)} request status: {response.Status.ToString()}.");
    }

    /// <summary>
    ///     Updates multiple inbox In-app messages async.
    /// </summary>
    /// <param name="messages">Multiple inbox In-app messages.</param>
    /// <returns>Processed API response.</returns>
    public async void UpdateInboxMessagesAsync(List<InAppMessage> messages)
    {
        var endTimestampUpdate = DateTimeOffset.UtcNow.AddSeconds(60).ToUnixTimeSeconds();
        var customParams = new Dictionary<string, object>
        {
            ["ID"] = 1.0,
            ["custom_key_2"] = "custom_value_2",
            ["camelCase"] = true
        };
        foreach (var message in messages)
        {
            message
                .SetCustomParameters(customParams)
                .SetViewsMetrics(message.InboxStats.Views + 1)
                .SetUsageMetrics(message.InboxStats.Usage + 1)
                .SetCountdownTimerEndTimestamp(endTimestampUpdate)
                .ResetRemindersMetrics();
        }

        var response = await Kinoa.Messaging.UpdateInboxMessagesAsync(messages);
        Log($"{nameof(UpdateInboxMessagesAsync)} request status: {response.Status.ToString()}.");
    }

    /// <summary>
    ///     Deletes all inbox messages.
    /// </summary>
    public async void DeleteAllInboxMessagesAsync()
    {
        var response = await Kinoa.Messaging.DeleteAllInboxMessagesAsync();
        Log($"{nameof(DeleteAllInboxMessagesAsync)} request status: {response.Status.ToString()}.");
    }

    /// <summary>
    ///     Deletes inbox message by uuid.
    /// </summary>
    /// <param name="message">Inbox message.</param>
    public async void DeleteInboxMessageAsync(InAppMessage message)
    {
        var response = await Kinoa.Messaging.DeleteInboxMessageAsync(message);
        Log($"{nameof(DeleteInboxMessageAsync)} request status: {response.Status.ToString()}.");
    }

    /// <summary>
    ///     Deletes multiple inbox In-app messages async.
    /// </summary>
    /// <param name="messages">The In-app messages.</param>
    /// <returns>Processed API response.</returns>
    public async void DeleteInboxMessagesAsync(IEnumerable<InAppMessage> messages)
    {
        var response = await Kinoa.Messaging.DeleteInboxMessagesAsync(messages);
        Log($"{nameof(DeleteInboxMessagesAsync)} request status: {response.Status.ToString()}.");
    }

    /// <summary>
    ///     Creates new In-app message by External Link.
    /// </summary>
    /// <param name="externalLink">Unique In-app message external link.</param>
    public async void CreateInAppMessageAsync(string externalLink)
    {
        var response = await Kinoa.Messaging.CreateInAppMessageAsync(externalLink);
        if (response.IsSuccessful())
            Log($"The In-app message was successfully generated by External Link: {externalLink}." +
                $"\nMessage ID: {response.Data.Uuid}");
        else
            Log($"{nameof(CreateInAppMessageAsync)} request status: {response.Status.ToString()}." +
                $"\nError code: {(response.Error != null ? response.Error.Code.ToString() : "Error object is null.")}");
    }

    /// <summary>
    ///     Creates new In-app message by Push Notification.
    /// </summary>
    /// <param name="inAppCreationParams">The In-app message creation parameters.</param>
    public async Task CreateInAppMessageAsync(InAppByPushCreationParams inAppCreationParams)
    {
        var response = await Kinoa.Messaging.CreateInAppMessageAsync(inAppCreationParams);
        if (response.IsSuccessful())
            Log($"The In-app message '{inAppCreationParams.ID}' " +
                "was successfully generated by Push Notification." +
                $"\nMessage ID: {response.Data.Uuid}");
        else
            Log($"{nameof(CreateInAppMessageAsync)} request status: {response.Status.ToString()}." +
                $"\nError code: {(response.Error != null ? response.Error.Code.ToString() : "Error object is null.")}");
    }

    /// <summary>
    ///     Shows inbox messages for demonstration purposes only.
    ///     Use your custom UI interface and client notification mechanism.
    /// </summary>
    private void Log(string message, List<InAppMessage> inApps = null)
    {
        Debug.Log(message);
        LogNotifications(inApps, message);
    }

    /// <summary>
    ///     Logs the received In-app messages.
    /// </summary>
    /// <param name="messages">In-app messages collection.</param>
    /// <param name="logMessage">Log message.</param>
    public void LogReceivedInApp(List<InAppMessage> messages, string logMessage = null)
    {
        if (messages == null)
        {
            return;
        }

        Log($"{logMessage}" + $"Messages type of {nameof(InAppMessages)} are received ({messages.Count}):\n" +
            $"{(messages.Count > 0 ? string.Join(",\n", messages.Select(o => $"{o.Uuid}").ToArray()) : string.Empty)}.",
            messages);

        //The In-app message properties access on the example of logging.
        foreach (var inApp in messages)
        {
            Log(inApp);
        }
    }

    /// <summary>
    ///     Debug logs string builder.
    /// </summary>
    private readonly StringBuilder log = new StringBuilder();

    /// <summary>
    ///     Logs In-app message properties.
    /// </summary>
    /// <param name="inApp">The In-app message.</param>
    private void Log(InAppMessage inApp)
    {
        log.AppendFormat("In-app message properties access:");
        log.AppendFormat($"\n\nUuid: {inApp.Uuid}");
        log.AppendFormat($"\nMessageId: {inApp.MessageId}");
        log.AppendFormat($"\nFlowId: {inApp.FlowId}");
        log.AppendFormat("\nName: {0}", inApp.Name);
        log.AppendFormat($"\nOrder: {inApp.Order}");
        log.AppendFormat($"\nSentTime: {inApp.SentTime}");
        log.AppendFormat($"\nIsInboxMessage: {inApp.IsInboxMessage}");
        log.AppendFormat($"\nIsTriggeredOffline: {inApp.IsTriggeredOffline}");

        Log(inApp.Command, inApp.Uuid);
        Log(inApp.Data);
        Log(inApp.LobbyIcon);
        Log(inApp.Placement);
        Log(inApp.CountdownTimer);
        Log(inApp.Capping);
        Log(inApp.Scheduling);
        Log(inApp.InboxStats);
        Log(inApp.ProgressionScore);
        Log(inApp.Extra);
        Log(inApp.CustomParams);
        Log(inApp.Audiences, inApp.UserLists, inApp.ConfigurationFilters);
        Log(inApp.SecurityData);
        //log.AppendFormat($"\nOriginalJson: {inApp.OriginalJson}");

        log.AppendFormat("\n");
        Debug.Log(log.ToString());
        log.Clear();
    }

    /// <summary>
    ///     Logs the In-app message segmentation: audiences, user lists, and configuration filters.
    /// </summary>
    /// <param name="inAppAudiences">The In-app message audiences.</param>
    /// <param name="inAppUserLists">The In-app message user lists.</param>
    /// <param name="inAppConfigurationFilters">The In-app message configuration filters.</param>
    private void Log(Dictionary<string, bool> inAppAudiences,
        Dictionary<string, bool> inAppUserLists,
        Dictionary<string, object> inAppConfigurationFilters)
    {
        var audiences = string.Join(", ", inAppAudiences
            .Where(x => x.Value == true)
            .Select(x => x.Key));
        var userLists = string.Join(", ", inAppUserLists
            .Where(x => x.Value == true)
            .Select(x => x.Key));
        var configurationFilters = string.Join(", ", inAppConfigurationFilters
            .Select(x => string.Format("{0}: {1}", x.Key, x.Value)));

        log.AppendFormat($"\n\tAudiences: {audiences}. " +
                         $"\n\tUser Lists: {userLists}. " +
                         $"\n\tConfiguration Filters: {configurationFilters}. ");
    }

    /// <summary>
    ///     Logs the In-app message custom parameters.
    /// </summary>
    /// <param name="inAppCustomParams">The In-app message custom parameters.</param>
    private void Log(Dictionary<string, object> inAppCustomParams)
    {
        if (inAppCustomParams == null) return;

        log.AppendFormat("\n\nCustomParams: ");
        log.AppendFormat(
            string.Join(",\n\t", inAppCustomParams.Select(o => $"{o.Key}: {o.Value}").ToArray()));
    }

    /// <summary>
    ///     Logs the In-app message extra fields.
    /// </summary>
    /// <param name="inAppExtraFields">The In-app message extra fields.</param>
    private void Log(List<InAppMessageExtraField> inAppExtraFields)
    {
        if (inAppExtraFields == null) return;

        log.AppendFormat("\n\nExtra: ");
        log.AppendFormat(
            string.Join(",\n\t", inAppExtraFields.Select(o => $"{o.Name}: {o.Value}").ToArray()));
    }

    /// <summary>
    ///     Logs the In-app message progression score.
    /// </summary>
    /// <param name="inAppProgressionScore">The In-app message progression score.</param>
    private void Log(InAppProgressionScore inAppProgressionScore)
    {
        if (inAppProgressionScore == null) return;

        log.AppendFormat("\n\nProgression Score: ");
        log.AppendFormat($"\n\tCurrent: {inAppProgressionScore.Current}");
        log.AppendFormat($"\n\tTotal: {inAppProgressionScore.Total}");
    }

    /// <summary>
    ///     Logs the In-app message statistics.
    /// </summary>
    /// <param name="inAppMessageStatistics">The In-app message statistics.</param>
    private void Log(InAppMessageStatistics inAppMessageStatistics)
    {
        if (inAppMessageStatistics == null) return;

        log.AppendFormat($"\n\nInboxStats: ");
        log.AppendFormat($"\n\tViews: {inAppMessageStatistics.Views}");
        log.AppendFormat($"\n\tUsage: {inAppMessageStatistics.Usage}");
        log.AppendFormat($"\n\tReminders: {inAppMessageStatistics.Reminders}");
    }

    /// <summary>
    ///     Logs the In-app message scheduling.
    /// </summary>
    /// <param name="inAppScheduling">The In-app message scheduling.</param>
    private void Log(InAppMessageScheduling inAppScheduling)
    {
        if (inAppScheduling == null) return;

        log.AppendFormat($"\n\nScheduling.StartTimeMs: {inAppScheduling.StartTimeMs}");
        if (inAppScheduling.EndTimeMs != null)
            log.AppendFormat($"\n\nScheduling.EndTimeMS: {inAppScheduling.EndTimeMs}");
    }

    /// <summary>
    ///     Logs the In-app message capping.
    /// </summary>
    /// <param name="inAppCapping">The In-app message capping.</param>
    private void Log(InAppMessageCapping inAppCapping)
    {
        if (inAppCapping == null) return;

        log.AppendFormat($"\n\nCapping.TotalLimit: {inAppCapping.TotalLimit}");
        if (inAppCapping.EligibilityLimit != null)
        {
            log.AppendFormat("\nCapping.EligibilityLimit: ");
            log.AppendFormat(
                $"\n\tOriginal: {inAppCapping.EligibilityLimit.Original}" +
                $"\n\tActual: {inAppCapping.EligibilityLimit.Actual}" +
                $"\n\tIsEligibilityUsed: {inAppCapping.EligibilityLimit.IsEligibilityUsed}");
        }

        if (inAppCapping.RecurrentLimit != null)
        {
            log.AppendFormat("\nCapping.RecurrentLimit: ");
            log.AppendFormat(
                $"\n\tAmount: {inAppCapping.RecurrentLimit.Amount}" +
                $"\n\tPeriod: {inAppCapping.RecurrentLimit.Period}");
        }

        if (inAppCapping.Cooldown != null)
        {
            log.AppendFormat("\nCapping.CoolDown: ");
            log.AppendFormat(
                $"\n\tPeriod: {inAppCapping.Cooldown.Period}");
        }

        if (inAppCapping.SessionCooldown != null)
        {
            log.AppendFormat("\nCapping.SessionCooldown: ");
            log.AppendFormat(
                $"\n\tPeriod: {inAppCapping.SessionCooldown.Count}");
        }
    }

    /// <summary>
    ///     Logs the In-app message countdown timer.
    /// </summary>
    /// <param name="inAppCountdownTimer">The In-app message countdown timer.</param>
    private void Log(InAppCountdownTimer inAppCountdownTimer)
    {
        if (inAppCountdownTimer == null) return;

        log.AppendFormat("\n\nCountdownTimer: ");
        log.AppendFormat($"\n\tEndTimestamp: {inAppCountdownTimer.EndTimestamp}");
        log.AppendFormat($"\n\tIsVisible: {inAppCountdownTimer.IsVisible.ToString()}");
    }

    /// <summary>
    ///     Logs the In-app message placement.
    /// </summary>
    /// <param name="inAppPlacement">The In-app message placement.</param>
    private void Log(InAppPlacement inAppPlacement)
    {
        if (inAppPlacement == null) return;

        log.AppendFormat($"\n\tPlacement ID: {Convert.ToString(inAppPlacement.Id)}");
    }

    /// <summary>
    ///     Logs the In-app JSON security data.
    /// </summary>
    /// <param name="inAppSecurityData">The In-app JSON security data.</param>
    private void Log(JsonSecurityData inAppSecurityData)
    {
        log.AppendFormat($"\n\tChecksum: {inAppSecurityData.Checksum}");
        log.AppendFormat($"\n\tSequenceId: {inAppSecurityData.SequenceId}");
        log.AppendFormat($"\n\tStateChangeSequenceId: {inAppSecurityData.StateChangeSequenceId}");
    }

    /// <summary>
    ///     Logs the command for the game to execute after the current In-app is received.
    /// </summary>
    /// <param name="inAppCommand">The In-app command.</param>
    /// <param name="uuid">The In-app inbox uuid.</param>
    private void Log(InAppCommand inAppCommand, string uuid)
    {
        if (inAppCommand == null) return;

        log.AppendFormat("\n\nCommand: ");
        switch (inAppCommand)
        {
            case InAppReplacedCommand cmd:
                log.AppendFormat($"\n\tAction: {nameof(InAppReplacedCommand)}" +
                                 $"\n\tReplaced UUID: '{cmd.ReplacedUuid}', Message UUID:'{uuid}'");
                break;
            case InAppReminderCommand cmd:
                log.AppendFormat($"\n\tAction: {nameof(InAppReminderCommand)}" +
                                 $"\n\tThe reminder that an In-app message with UUID: '{uuid}' is waiting for you in the inbox.");
                break;
            case InAppScoreChangedCommand cmd:
                log.AppendFormat($"\n\tAction: {nameof(InAppScoreChangedCommand)}" +
                                 $"\n\tThe In-app message with UUID: '{uuid}' progression score is changed.");
                break;
            default:
                log.AppendFormat("\n\tIn-app command cannot be processed: Unknown command.");
                break;
        }
    }

    /// <summary>
    ///     Logs the In-app message template data.
    /// </summary>
    /// <param name="inAppData">The In-app message template data.</param>
    private void Log(InAppMessageData inAppData)
    {
        if (inAppData == null) return;

        log.AppendFormat("\n\nData: ");
        switch (inAppData)
        {
            case InAppSimpleTemplateData data:
                Log(data);
                break;
            case InAppCustomTemplateData data:
                Log(data);
                break;
            default:
                log.AppendFormat("\n\tIn-app data cannot be processed: Unknown template type.");
                break;
        }
    }

    /// <summary>
    ///     Logs the In-app message data type of Simple template <see cref="InAppSimpleTemplateData"/>.
    /// </summary>
    /// <param name="inAppData">The In-app message template data.</param>
    private void Log(InAppSimpleTemplateData inAppData)
    {
        log.AppendFormat($"\n\tTemplate: {nameof(InAppSimpleTemplateData)}");
        if (inAppData.MainPortraitImage != null)
        {
            log.AppendFormat("\n\n\tMainPortraitImage: ");
            Log(inAppData.MainPortraitImage);
        }

        if (inAppData.MainLandscapeImage != null)
        {
            log.AppendFormat("\n\n\tMainLandscapeImage: ");
            Log(inAppData.MainLandscapeImage);
        }

        if (inAppData.ClickConfig != null)
        {
            log.AppendFormat("\n\n\tClickConfig: ");
            Log(inAppData.ClickConfig);
        }

        if (inAppData.Resources != null)
        {
            log.AppendFormat("\n\n\tResources: ");
            Log(inAppData.Resources);
        }

        if (inAppData.Packages != null)
        {
            log.AppendFormat("\n\n\tPackages: ");
            Log(inAppData.Packages);
        }
    }

    /// <summary>
    ///     Logs the In-app message data type of Custom template <see cref="InAppCustomTemplateData"/>.
    /// </summary>
    /// <param name="inAppData">The In-app message template data.</param>
    private void Log(InAppCustomTemplateData inAppData)
    {
        log.AppendFormat($"\n\tTemplate: {nameof(InAppCustomTemplateData)}");
        log.AppendFormat("\n\tTemplateKey: {0}", inAppData.TemplateKey);
        if (inAppData.Buttons != null)
        {
            log.AppendFormat("\n\n\tButtons: ");
            LogCustomButtons(inAppData.Buttons);
        }

        if (inAppData.Images != null)
        {
            log.AppendFormat("\n\n\tImages: ");
            LogCustomImages(inAppData.Images);
        }

        if (inAppData.Texts != null)
        {
            log.AppendFormat("\n\n\tTexts: ");
            LogCustomTexts(inAppData.Texts);
        }

        if (inAppData.Customs != null)
        {
            log.AppendFormat("\n\n\tCustom Elements: ");
            LogCustomElements(inAppData.Customs);
        }
    }

    /// <summary>
    ///     Logs the collection of In-app attached resources.
    /// </summary>
    /// <param name="resources">The resources collection.</param>
    private void Log(List<Resource> resources)
    {
        foreach (var resource in resources)
        {
            log.AppendFormat("\n\tResourceKey: {0}", resource.ResourceKey);
            log.AppendFormat($"\n\tAmount: {resource.Amount}");
            log.AppendFormat("\n\tBody: {0}", resource.Body);
            log.AppendFormat("\n");
        }
    }

    /// <summary>
    ///     Logs the collection In-app message buttons.
    ///     Key - button key, Value - button data.
    /// </summary>
    /// <param name="inAppButtons">The buttons collection.</param>
    private void LogCustomButtons(Dictionary<string, InAppCustomButton> inAppButtons)
    {
        foreach (var inAppButton in inAppButtons)
        {
            var value = inAppButton.Value;
            log.AppendFormat("\n\tKey: {0}", inAppButton.Key);
            log.AppendFormat("\n\tLabel: {0}", value.Label);

            if (value.BackgroundImage != null)
            {
                log.AppendFormat("\n\tBackgroundImage: ");
                Log(value.BackgroundImage);
            }

            if (value.ClickConfig != null)
            {
                log.AppendFormat("\n\tClickConfig: ");
                Log(value.ClickConfig);
            }

            if (value.Resources != null)
            {
                log.AppendFormat("\n\tResources: ");
                Log(value.Resources);
            }

            if (value.Packages != null)
            {
                log.AppendFormat("\n\tPackages: ");
                Log(value.Packages);
            }

            if (value.CustomFields != null)
            {
                log.AppendFormat("\n\tCustomFields: ");
                LogCustomFields(value.CustomFields);
            }

            log.AppendFormat("\n");
        }
    }

    /// <summary>
    ///     Logs the collection of In-app message custom images.
    ///     Key - image key, Value - image data.
    /// </summary>
    /// <param name="inAppImages">The images collection.</param>
    private void LogCustomImages(Dictionary<string, InAppCustomImage> inAppImages)
    {
        foreach (var image in inAppImages)
        {
            log.AppendFormat("\n\tKey: {0}", image.Key);
            Log(image.Value);

            if (image.Value.CustomFields != null)
            {
                log.AppendFormat("\n\tCustomFields: ");
                LogCustomFields(image.Value.CustomFields);
            }

            log.AppendFormat("\n");
        }
    }

    /// <summary>
    ///     Logs the collection of In-app message custom texts.
    /// </summary>
    /// <param name="inAppTexts">The texts collection.</param>
    private void LogCustomTexts(Dictionary<string, InAppCustomText> inAppTexts)
    {
        foreach (var inAppText in inAppTexts)
        {
            log.AppendFormat("\n\tKey: {0}", inAppText.Key);
            log.AppendFormat("\n\tContent: {0}", inAppText.Value.Content);

            if (inAppText.Value.CustomFields != null)
            {
                log.AppendFormat("\n\tCustomFields: ");
                LogCustomFields(inAppText.Value.CustomFields);
            }

            log.AppendFormat("\n");
        }
    }

    /// <summary>
    ///     Logs the collection of In-app message custom elements.
    /// </summary>
    /// <param name="inAppCustomElements">The custom elements collection.</param>
    private void LogCustomElements(Dictionary<string, InAppCustomElement> inAppCustomElements)
    {
        foreach (var inAppCustomElement in inAppCustomElements)
        {
            log.AppendFormat("\n\tKey: {0}", inAppCustomElement.Key);
            log.AppendFormat("\n\tValue: {0}", inAppCustomElement.Value.Value);

            if (inAppCustomElement.Value.CustomFields != null)
            {
                log.AppendFormat("\n\tCustomFields: ");
                LogCustomFields(inAppCustomElement.Value.CustomFields);
            }

            log.AppendFormat("\n");
        }
    }

    /// <summary>
    ///     Logs the collection of In-app message custom fields.
    ///     Key - custom field name, Value - custom field value.
    /// </summary>
    /// <param name="customFields">The custom fields collection.</param>
    private void LogCustomFields(Dictionary<string, object> customFields)
    {
        foreach (var customField in customFields)
        {
            log.AppendFormat("\n\tKey: {0}", customField.Key);
            log.AppendFormat("\n\tValue: {0}", customField.Value);
            log.AppendFormat("\n");
        }
    }

    /// <summary>
    ///     Logs the In-app message lobby icon.
    /// </summary>
    /// <param name="lobbyIcon">The In-app message lobby icon.</param>
    private void Log(InAppLobbyIcon lobbyIcon)
    {
        if (lobbyIcon == null) return;

        log.AppendFormat("\n\nLobbyIcon: ");
        Log((InAppImage) lobbyIcon);
        log.AppendFormat($"\n\tIsInAppTrigger: {lobbyIcon.IsInAppTrigger.ToString()}");
        log.AppendFormat("\n\tText: {0}", lobbyIcon.Text);
        log.AppendFormat($"\n\tScore: {Convert.ToString(lobbyIcon.Score)}");
    }

    /// <summary>
    ///     Logs the In-app message image.
    /// </summary>
    /// <param name="inAppImage">The In-app message image.</param>
    private void Log(InAppImage inAppImage)
    {
        if (inAppImage == null) return;
        log.AppendFormat("\n\tContent: {0}", inAppImage.Content);
        log.AppendFormat($"\n\tContentType: {inAppImage.ContentType}");
    }

    /// <summary>
    ///     Logs the collection of In-app attached store packages.
    /// </summary>
    /// <param name="inAppStorePackages">The store packages.</param>
    private void Log(InAppStorePackages inAppStorePackages)
    {
        log.AppendFormat("\n\tAndroidPackageID: {0}", inAppStorePackages.AndroidPackageID);
        log.AppendFormat("\n\tAndroidDiscountPackageID: {0}", inAppStorePackages.AndroidDiscountPackageID);
        log.AppendFormat("\n\tIosDiscountPackageID: {0}", inAppStorePackages.IosDiscountPackageID);
        log.AppendFormat("\n\tIosPackageID: {0}", inAppStorePackages.IosPackageID);
    }

    /// <summary>
    ///     Logs the In-app click action.
    /// </summary>
    /// <param name="inAppClickConfig">The In-app click configuration.</param>
    private void Log(InAppClickConfiguration inAppClickConfig)
    {
        switch (inAppClickConfig)
        {
            case InAppWebLinkClickConfiguration linkClickConfig:
                log.AppendFormat($"\n\tAction: {nameof(InAppWebLinkClickConfiguration)}");
                log.AppendFormat("\n\tLink: {0}", linkClickConfig.Link);
                break;
            case InAppDeepLinkClickConfiguration linkClickConfig:
                log.AppendFormat($"\n\tAction: {nameof(InAppDeepLinkClickConfiguration)}");
                log.AppendFormat("\n\tLink: {0}", linkClickConfig.Link);
                break;
            case InAppCloseClickConfiguration clickConfig:
                log.AppendFormat($"\n\tAction: {nameof(InAppCloseClickConfiguration)}");
                break;
            case InAppCollectResourceClickConfiguration clickConfig:
                log.AppendFormat($"\n\tAction: {nameof(InAppCollectResourceClickConfiguration)}");
                break;
            case InAppSoftBillingClickConfiguration softBillingClickConfig:
                var priceResources = softBillingClickConfig.PriceResources
                    .Select(o => $"Key: {o.ResourceKey.ToString()}, Amount: {o.Amount}").ToArray();
                log.AppendFormat($"\n\tAction: {nameof(InAppSoftBillingClickConfiguration)}" +
                                 $"\n\tPrice: {string.Join(",\n", priceResources)}");
                break;
            case InAppBillingClickConfiguration clickConfig:
                log.AppendFormat($"\n\tAction: {nameof(InAppBillingClickConfiguration)}");
                break;
            case InAppShowAdClickConfiguration clickConfig:
                log.AppendFormat($"\n\tAction: {nameof(InAppShowAdClickConfiguration)}");
                break;
            case InAppPromiseRewardsClickConfiguration clickConfig:
                log.AppendFormat($"\n\tAction: {nameof(InAppPromiseRewardsClickConfiguration)}");
                break;
            case InAppUpdateAppVersionClickConfiguration clickConfig:
                log.AppendFormat($"\n\tAction: {nameof(InAppUpdateAppVersionClickConfiguration)}");
                break;
            default:
                Debug.Log("The In-app click cannot be processed: Unknown click type.");
                break;
        }
    }
}