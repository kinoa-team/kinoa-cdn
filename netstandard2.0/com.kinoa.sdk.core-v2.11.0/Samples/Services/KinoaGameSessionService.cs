﻿using System.Threading;
using System.Threading.Tasks;
using Kinoa.Core.Network.Retry;
using Kinoa.Data;
using Kinoa.Data.Network;
using Kinoa.Data.State;
using Kinoa.Data.WebModels;
using UnityEngine;

/// <summary>
///     The sample Game Session Service used to manage the Game Session.
/// </summary>
public class KinoaGameSessionService : KinoaSingleton<KinoaGameSessionService>
{
    /// <summary>
    ///     The custom network configuration of the Session Open request.
    /// </summary>
    private readonly NetworkConfiguration openSessionNetworkConfig = new NetworkConfiguration(networkTimeout: 30,
        new RetryConfiguration(retryReason: RetryReason.AlwaysRetry,
            retryStrategy: RetryStrategy.Exponential,
            maxRetryAttempts: 5,
            maxRetryDelay: 15));

    /// <summary>
    ///     Opens a new Game Session asynchronously.
    ///     The provided Player State will merge with the server's Player State and return in the response.
    ///     If the provided Player State is null, the server's Player State will be applied to the active Player.
    /// <remarks>
    ///     The method doesn't sends the Session Start Game Event.
    ///     <see cref="Kinoa.GameEvents.SendSessionStartEvent{TPlayerState}"/> (Async API) ~or~
    ///     <see cref="Kinoa.SyncGameEvents.SendSessionStartEventAsync{TPlayerState}"/> (Sync API)
    ///     to trigger the Session Start Game Event.
    /// </remarks>
    /// </summary>
    /// <param name="gameSessionData">The Game Session Data.</param>
    /// <param name="playerState">The Player State.</param>
    /// <param name="networkConfiguration">The network configuration of the Session Start request.</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <typeparam name="TPlayerState">Type of the Player State object.</typeparam>
    /// <returns>The response of the Game Session Start request with the merged Player State.</returns>
    public async Task<Response<WebPlayerState<TPlayerState>>> OpenSessionAsync<TPlayerState>(
        GameSessionData gameSessionData, TPlayerState playerState = null,
        NetworkConfiguration networkConfiguration = null,
        CancellationToken cancellationToken = default)
        where TPlayerState : class, IPlayerState, new()
    {
        var gameSession = gameSessionData ?? new GameSessionData();
        var networkConfig = networkConfiguration ?? openSessionNetworkConfig;

        var response = await Kinoa.GameSession.OpenSessionAsync<TPlayerState>(
            gameSession, playerState, networkConfig, cancellationToken);
        if (response.IsSuccessful())
        {
            Log("The new Game Session is successfully opened.");
            if (response.Data != null)
            {
                Debug.Log("The Server Player State is successfully received.");
                KinoaPlayerStateService<TPlayerState>.Instance.PlayerState = response.Data.PlayerState;
            }
        }
        else
        {
            Log($"The new Game Session opening failed: {response.Error?.Code}. " +
                $"\nResponse status: {response.Status.ToString()}");
        }

        return response;
    }

    /// <summary>
    ///     The sample log method shows the messages in the console and the dialog.
    /// </summary>
    /// <param name="message">The message.</param>
    private void Log(string message)
    {
        Debug.Log(message);
    }
}