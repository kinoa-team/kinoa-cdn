﻿using System.Text.Json.Serialization;

/// <summary>
///     Daily Bonus Feature Settings data model.
/// </summary>
public class DailyBonusSettings : FeatureSettingsData
{
    /// <summary>
    ///     The coins reward.
    /// </summary>
    [JsonInclude]
    [JsonPropertyName("Coins")]
    public double Coins { get; private set; }
}