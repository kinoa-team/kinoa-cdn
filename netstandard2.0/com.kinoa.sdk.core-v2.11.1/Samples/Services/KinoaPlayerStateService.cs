﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Kinoa.Data;
using Kinoa.Data.Enum;
using Kinoa.Data.State;
using Kinoa.Data.SyncGameEvents;
using Kinoa.Data.WebModels;
using UnityEngine;

/// <summary>
///     The sample Player Service used to manage the Player data.
/// </summary>
public class KinoaPlayerStateService<TPlayerState> : KinoaSingleton<KinoaPlayerStateService<TPlayerState>>
    where TPlayerState : class, IPlayerState, new()
{
    /// <summary>
    ///     The Local Player State <see cref="PlayerState"/> sample set event.
    /// </summary>
    public event Action<bool> OnLocalPlayerStateSet;

    /// <summary>
    ///     The actual Player State.
    /// </summary>
    public TPlayerState PlayerState { get; set; }

    /// <summary>
    ///     Initialize the new instance of <see cref="KinoaPlayerStateService{T}"/>.
    ///     Sets player state changed by operator handler.
    /// </summary>
    public KinoaPlayerStateService()
    {
        Kinoa.Player.SetStateChangedByOperatorHandler(OnPlayerStateChangedByOperator);
    }

    /// <summary>
    ///     The Local Player State <see cref="PlayerState"/> sample set.
    /// </summary>
    public void LocalPlayerStateSet(bool isSet)
    {
        OnLocalPlayerStateSet?.Invoke(isSet);
    }

    /// <summary>
    ///     Gets the actual Player State of an Active Player <see cref="Kinoa.Player.ID"/> asynchronously.
    ///     <remarks>
    ///     Gets the local (source of truth) ~or~ Kinoa server Player State if the local State is empty or unavailable.
    ///     </remarks>
    /// </summary>
    /// <returns>The actual Player State object.</returns>
    public async Task<TPlayerState> GetPlayerStateAsync()
    {
        Debug.Log("The local Player State will be used to open session." +
                  "If the local Player State is null, the server's Player State will be applied " +
                  "to the active Player automatically on the first session start request.");
        
        PlayerState = await GetLocalPlayerStateAsync();
        LocalPlayerStateSet(true);

        return PlayerState;
    }

    /// <summary>
    ///     Gets the Kinoa server Player State by the Player ID asynchronously.
    /// </summary>
    /// <param name="playerId">The Player ID.</param>
    /// <returns>The server Player State object.</returns>
    public async Task<Response<TPlayerState>> GetServerPlayerStateAsync(string playerId)
    {
        var response = await Kinoa.Player.GetStateAsync<TPlayerState>(playerId);
        LogPlayerState(response);
        return response;
    }

    /// <summary>
    ///     Logs the Player State object in console.
    /// </summary>
    /// <param name="playerStateResponse">The Get Player State response.</param>
    private void LogPlayerState(Response<TPlayerState> playerStateResponse)
    {
        if (playerStateResponse.Data is PlayerState playerState)
        {
            Debug.Log($"The actual Player State received successfully: {playerStateResponse.IsSuccessful()}.\n" +
                      $"Player ID: \"{playerState.PlayerIdentifiers.PlayerId}\". " +
                      $"Native ID: \"{playerState.PlayerIdentifiers.NativeId}\"." +
                      $"Facebook ID: \"{playerState.PlayerIdentifiers.FacebookId}\"." +
                      $"Google ID: \"{playerState.PlayerIdentifiers.GoogleId}\"." +
                      $"Apple ID: \"{playerState.PlayerIdentifiers.AppleId}\"." +
                      $"Extra ID 1: \"{playerState.PlayerIdentifiers.ExtraId1}\"." +
                      $"Extra ID 2: \"{playerState.PlayerIdentifiers.ExtraId2}\"." +
                      $"Extra ID 3: \"{playerState.PlayerIdentifiers.ExtraId3}\".");

            if (!playerState.IsRegistered)
            {
                Debug.Log("Player is not registered in Kinoa and has empty Player State. " +
                          "Player will be created in Kinoa after first session start request with provided Player State.");
            }
        }

        if (playerStateResponse.Data is PlayerStateDictionary playerStateDictionary)
        {
            if (playerStateDictionary.TryGetValue("player_identifiers", out var value))
            {
                if (value is Dictionary<string, object> identifiers)
                {
                    Debug.Log($"The actual Player State received successfully: " +
                              $"{playerStateResponse.IsSuccessful()}.\n" +
                              $"Player ID: \"{identifiers["player_id"]}\".");
                }
            }

            if (playerStateDictionary.Count == 0)
            {
                Debug.Log("Player is not registered in Kinoa and has empty Player State. " +
                          "Player will be created in Kinoa after first session start request with provided Player State.");
            }
        }
    }

    /// <summary>
    ///     Gets the local Player State asynchronously.
    /// </summary>
    /// <returns>The local Player State object.</returns>
    public async Task<TPlayerState> GetLocalPlayerStateAsync()
    {
        //TODO: Get the Player State from your local storage or your server because it's always actual instead of null.
        //TODO: If the local Player State is null, the server's Player State will be applied to the active Player automatically on the first session start request.
        return await Task.FromResult(PlayerState);
    }

    /// <summary>
    ///     Sends the Async API Reset Player State Game Event
    ///     to reset the Player State to the provided one.
    /// </summary>
    public void ResetPlayerState(TPlayerState playerState)
    {
        Kinoa.GameEvents.SendResetPlayerStateEvent(playerState);
    }

    /// <summary>
    ///     Sends the Sync API Reset Player State Game Event
    ///     to reset the Player State to the provided one.
    /// </summary>
    /// <param name="playerState"></param>
    /// <returns></returns>
    public Task<Response<SyncGameEventResponse>> ResetPlayerStateAsync(TPlayerState playerState)
    {
        return Kinoa.SyncGameEvents.SendResetPlayerStateEventAsync(playerState);
    }

    /// <summary>
    ///     Approves the Player State operator changes asynchronously. Unblocks the future Player State changes.
    /// </summary>
    public async Task<Response> ApprovePlayerStateChangesAsync()
    {
        var response = await Kinoa.Player.ApproveStateChangesAsync();
        Debug.Log($"The Player State operator changes are approved: {response.IsSuccessful()}.");

        return response;
    }

    /// <summary>
    ///     Triggered on changing the Player State by an operator.
    ///     You should approve the state changes <see cref="Kinoa.Player.ApproveStateChanges"/> before sending new Game Events. 
    /// </summary>
    /// <param name="actualServerPlayerState">Kinoa player state.</param>
    private async void OnPlayerStateChangedByOperator(WebPlayerStateJsonNode actualServerPlayerState)
    {
        var message = "The Player State changed by an Operator.";
        Debug.Log(message);

        // TODO: Implement merging strategy of local and remote player states.
        // In current sample we take remote player state as actual one, ignoring local player state changes.
        PlayerState = await actualServerPlayerState.GetPlayerState<TPlayerState>();

        // Approve Player State changes after merging.
        await ApprovePlayerStateChangesAsync();

        // Reset remote Player State with actual Player State.
        ResetPlayerState(PlayerState);
    }

    /// <summary>
    ///     Sets preferred language for localization.
    /// </summary>
    public void SetLocalizationLanguage(Language language)
    {
        PlayerState?.SetLanguageCode(language);
    }

    /// <summary>
    ///     Sets install time.
    /// </summary>
    public void SetInstallTime(long timeMs)
    {
        PlayerState?.SetInstallTime(timeMs);
    }
}