﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kinoa.Core.Network;
using Kinoa.Data.ResourceManagement;
using UnityEngine;

namespace DefaultNamespace.Core
{
    /// <summary>
    ///     Kinoa resource management provider.
    /// </summary>
    public class KinoaResourceManagementProvider
    {
        /// <summary>
        ///     Local inbox storage.
        /// </summary>
        public List<InboxResource> LocalInboxStorage { get; set; } = new List<InboxResource>();

        /// <summary>
        ///     Gets the list of all inbox messages async.
        /// </summary>
        public async void GetInboxResourcesAsync()
        {
            var logBuilder = new StringBuilder();
            var response = await Kinoa.ResourceManagement.GetInboxResourcesAsync();
            if (response.Status == ResponseState.Success && response.Data != null)
            {
                var resources = response.Data.GroupBy(x => x.ResourceKey)
                    .Select(y => $"{y.Key}: {y.Count()}")
                    .ToArray();

                logBuilder.Append($"Inbox resources received successfully: ({response.Data.Count})." +
                                  $"\nInbox: {string.Join(", ", resources)}");

                LocalInboxStorage = response.Data;
            }
            else logBuilder.Append($"Get inbox messages request status: {response.Status.ToString()}.");

            Log(logBuilder.ToString());
        }

        /// <summary>
        ///     Deletes all inbox resources.
        /// </summary>
        public async void DeleteAllInboxResourcesAsync()
        {
            var response = await Kinoa.ResourceManagement.DeleteAllInboxResourcesAsync();
            Log($"{nameof(DeleteAllInboxResourcesAsync)} request status: {response.Status.ToString()}.");
        }

        /// <summary>
        ///     Deletes inbox resource by message uuid.
        /// </summary>
        /// <param name="message">Inbox message.</param>
        public async void DeleteInboxResourceAsync(InboxResource message)
        {
            var response = await Kinoa.ResourceManagement.DeleteInboxResourceAsync(message);
            Log($"{nameof(DeleteInboxResourceAsync)} request status: {response.Status.ToString()}.");
        }

        /// <summary>
        ///     Shows inbox resources on UI (for demonstration purposes only).
        ///     Use your custom UI interface and client notification mechanism.
        /// </summary>
        private static void Log(string message)
        {
            Debug.Log(message);
            NotificationManager.Instance.ShowNotification(message);
        }
    }
}