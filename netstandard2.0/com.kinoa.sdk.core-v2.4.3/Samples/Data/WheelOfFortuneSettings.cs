﻿using System.Text.Json.Serialization;

/// <summary>
///     Wheel of Fortune Feature Settings data model.
/// </summary>
public class WheelOfFortuneSettings : FeatureSettingsData
{
    /// <summary>
    ///     The prize reward.
    /// </summary>
    [JsonInclude]
    [JsonPropertyName("Prize")]
    public string Prize { get; private set; }

    /// <summary>
    ///     The coins reward.
    /// </summary>
    [JsonInclude]
    [JsonPropertyName("Coins")]
    public double Coins { get; private set; }
}