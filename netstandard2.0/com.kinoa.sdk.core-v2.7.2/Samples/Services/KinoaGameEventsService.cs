﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Kinoa.Data;
using Kinoa.Data.Enum;
using Kinoa.Data.Events;
using Kinoa.Data.Messaging.InApp;
using Kinoa.Data.Network;
using Kinoa.Data.WebModels;
using UnityEngine;

/// <summary>
///     Kinoa Asynchronous Game Events service.
/// </summary>
public class KinoaGameEventsService : KinoaGameEventBuildingService<KinoaGameEventsService>
{
    /// <summary>
    ///     Initializes the new instance of <see cref="KinoaGameEventsService"/>.
    ///     Subscribes to the SDK game session events.
    /// </summary>
    [Obsolete("Use the Kinoa.GameSession.OpenSessionAsync{TPlayerState} response to handle the Game Session open request.")]
    public KinoaGameEventsService()
    {
        Kinoa.GameEvents.OnGameSessionStarted += OnGameSessionStarted;
        Kinoa.GameEvents.OnGameSessionStartFailed += OnGameSessionStartFailed;
    }

    /// <summary>
    ///     Starts the new Game Session and sends the Start Session Game Event asynchronously.
    /// </summary>
    /// <param name="customParams">Custom event custom parameters.</param>
    /// <returns>The started Game Session response object.</returns>
    [Obsolete("Use the Kinoa.GameSession.OpenSessionAsync{TPlayerState} to open the new Game Session on Kinoa side " +
              "~AND~ Kinoa.GameEvents.SendSessionStartEvent{TPlayerState} (Async API) " +
              "~OR~ Kinoa.SyncGameEvents.SendSessionStartEventAsync{TPlayerState} (Sync API) to trigger the Session Start Game Event.")]
    public async Task<Response<WebPlayerState<CustomPlayerState>>> StartGameSessionAsync(
        Dictionary<string, object> customParams = null)
    {
        var e = new StartSessionEventData();
        e.AddCustomParameters(customParams);
        PlayerState?.SessionData.SetDeepLink("SomeDeepLink");

        const int timeout = 10;
        var networkConfig = new NetworkConfiguration(timeout);

        var response = await Kinoa.GameEvents.SendStartSessionEvent(e, PlayerState, networkConfig);
        if (response.IsSuccessful() && response.Data != null)
        {
            Debug.Log("The new game session is successfully started." +
                      "\nThe remote Player State received successfully: " +
                      $"Player ID: \"{response.Data?.PlayerState.PlayerIdentifiers.PlayerId}\".");
        }

        return response;
    }

    /// <summary>
    ///     On the new Game Session successfully started and registered on Kinoa backend.
    ///     Returns an active Session ID and the status of server response.
    /// <remarks>
    ///     Event will be triggered on each successful session start including the Automatic Retry Mechanism with a 5-second delay
    ///     <see cref="https://kinoa.atlassian.net/wiki/spaces/KW/pages/119275783/03+-+Game+Events+latest+version#Automatic-Retry-Mechanism"/>.
    ///
    ///     Use the response object of the <see cref="Kinoa.GameEvents.SendStartSessionEvent{TPlayerState}"/> call
    ///     to retrieve the Game Session start response object triggered by the client.
    /// </remarks>
    /// </summary>
    [Obsolete("Use the Kinoa.GameSession.OpenSessionAsync{TPlayerState} response to handle the successful Game Session opening.")]
    private void OnGameSessionStarted(Response<string> response) =>
        Debug.Log($"The new Game Session is successfully started: {response.IsSuccessful()}.");

    /// <summary>
    ///     On failed to start the new Game Session.
    ///     Returns a failed Session ID and the status of server response.
    /// <remarks>
    ///     Event will be triggered on each failed session start including the Automatic Retry Mechanism with a 5-second delay
    ///     <see cref="https://kinoa.atlassian.net/wiki/spaces/KW/pages/119275783/03+-+Game+Events+latest+version#Automatic-Retry-Mechanism"/>.
    ///
    ///     Use the response object of the <see cref="Kinoa.GameEvents.SendStartSessionEvent{TPlayerState}"/> call
    ///     to retrieve the Game Session start response object triggered by the client.
    /// </remarks>
    /// </summary>
    [Obsolete("Use the Kinoa.GameSession.OpenSessionAsync{TPlayerState} response to handle the failed Game Session opening.")]
    private void OnGameSessionStartFailed(Response<string> response)
    {
        var message = $"The new Game Session opening failed: {response.Error?.Code}. " +
                      $"\nResponse status: {response.Status.ToString()}";
        if (PlayerState == null)
        {
            message += "\nReason: PlayerState is null or empty.";
        }

        Debug.Log(message);
    }

    /// <summary>
    ///     Sends Session Start Game Event.
    /// </summary>
    /// <param name="playerState">Actualized Player State.</param>
    public void SendSessionStartEvent(CustomPlayerState playerState)
    {
        var e = OnGameSessionStart();
        Kinoa.GameEvents.SendSessionStartEvent(e, playerState);
    }

    /// <summary>
    ///     Sends the Progression Game Event.
    /// </summary>
    public void SendProgressionEvent()
    {
        var e = OnProgression();
        Kinoa.GameEvents.SendProgressionEvent(e, PlayerState);
    }

    /// <summary>
    ///     Sends the Level Up Game Event.
    /// </summary>
    /// <param name="customParams">Custom event custom parameters.</param>
    public void SendLevelUpEvent(Dictionary<string, object> customParams = null)
    {
        var e = OnLevelUp(customParams);
        Kinoa.GameEvents.SendLevelUpEvent(e, PlayerState);
    }

    /// <summary>
    ///     Sends the Payment Game Event.
    /// </summary>
    /// <param name="productId">Shop product identifier in market.</param>
    /// <param name="spent">Spent real money.</param>
    /// <param name="isoCurrencyCode">Real money currency in ISO 4217 format.</param>
    /// <param name="received">Received in-game resources.</param>
    public void SendPaymentEvent(string productId = "play-market-product-id",
        decimal spent = 0.99m,
        string isoCurrencyCode = "USD",
        Dictionary<string, decimal> received = null)
    {
        var e = OnPayment(productId, spent, isoCurrencyCode, received);
        Kinoa.GameEvents.SendPaymentEvent(e, PlayerState);
    }

    /// <summary>
    ///     Sends the Watch Ad Game Event.
    /// </summary>
    /// <param name="incomeFromAd">Real money income from ad.</param>
    /// <param name="isoCurrencyCode">Real money currency in ISO 4217 format.</param>
    /// <param name="adType">Ad type.</param>
    /// <param name="received">Received in-game resources.</param>
    public void SendWatchAdEvent(
        decimal incomeFromAd = 40m,
        string isoCurrencyCode = "UAH",
        string adType = "simple_ad",
        Dictionary<string, decimal> received = null)
    {
        var e = OnWatchAd(incomeFromAd, isoCurrencyCode, adType, received);
        Kinoa.GameEvents.SendWatchAdEvent(e, PlayerState);
    }

    /// <summary>
    ///     Sends the In-game purchase Game Event.
    /// </summary>
    /// <param name="spent">Spent in-game resources.</param>
    /// <param name="received">Received in-game resources.</param>
    public void SendInGamePurchaseEvent(Dictionary<string, decimal> spent = null,
        Dictionary<string, decimal> received = null)
    {
        var e = OnInGamePurchase(spent, received);
        Kinoa.GameEvents.SendInGamePurchaseEvent(e, PlayerState);
    }

    /// <summary>
    ///     Sends the Tutorial Game Event.
    /// </summary>
    /// <param name="action">Tutorial action.</param>
    public void SendTutorialEvent(TutorialAction action = TutorialAction.Finish)
    {
        var e = OnTutorial(action);
        Kinoa.GameEvents.SendTutorialEvent(e, PlayerState);
    }
    
    /// <summary>
    ///     Sends the Collected resource Game Event.
    /// </summary>
    public void SendCollectedResourceEvent()
    {
        var e = OnCollectedResource();
        Kinoa.GameEvents.SendCollectedResourceEvent(e, PlayerState);
    }

    /// <summary>
    ///     Sends the Social connect Game Event.
    /// </summary>
    public void SendSocialConnectEvent()
    {
        var e = OnSocialConnect();
        Kinoa.GameEvents.SendSocialConnectEvent(e, PlayerState);
    }

    /// <summary>
    ///     Sends the Social disconnect Game Event.
    /// </summary>
    public void SendsSocialDisconnectEvent()
    {
        var e = OnSocialDisconnect();
        Kinoa.GameEvents.SendSocialDisconnectEvent(e, PlayerState);
    }

    /// <summary>
    ///     Sends the Social post Game Event.
    /// </summary>
    public void SendSocialPostEvent()
    {
        var e = OnSocialPost();
        Kinoa.GameEvents.SendSocialPostEvent(e, PlayerState);
    }

    /// <summary>
    ///     Sends the Custom Game Event.
    /// </summary>
    /// <param name="name">Custom event name.</param>
    /// <param name="customParams">Custom event custom parameters.</param>
    public void SendCustomEvent(string name = "custom_event", Dictionary<string, object> customParams = null)
    {
        var e = OnCustomEvent(name, customParams);
        Kinoa.GameEvents.SendCustomEvent(e, PlayerState);
    }

    /// <summary>
    ///     Sends the Cheating Game Event.
    /// </summary>
    public void SendCheatingEvent()
    {
        var e = OnCheatingEvent();
        Kinoa.GameEvents.SendCustomEvent(e, PlayerState);
    }

    /// <summary>
    ///     Sends the Tester Game Event.
    /// </summary>
    public void SendTesterEvent()
    {
        var e = OnTesterEvent();
        Kinoa.GameEvents.SendCustomEvent(e, PlayerState);
    }

    /// <summary>
    ///     Sends the Error Game Event.
    /// </summary>
    /// <param name="exception">Thrown exception.</param>
    public void SendErrorEvent(Exception exception)
    {
        var e = OnError(exception);
        Kinoa.GameEvents.SendErrorEvent(e);
    }

    /// <summary>
    ///     Sends the In-app close Game Event.
    /// </summary>
    /// <param name="inAppMessage">The closed In-app message.</param>
    /// <param name="inAppMessageID">The closed In-app message identifier.</param>
    public void SendInAppCloseEvent(InAppMessage inAppMessage, string inAppMessageID = null)
    {
        var e = OnCloseInApp(inAppMessage, inAppMessageID);
        Kinoa.GameEvents.SendInAppCloseEvent(e, PlayerState);
    }

    /// <summary>
    ///     Sends the In-app click Game Event.
    /// </summary>
    /// <param name="inAppMessage">The clicked In-app message.</param>
    /// <param name="inAppMessageID">The clicked In-app message identifier.</param>
    /// <param name="received">Received in-game resources.</param>
    public void SendInAppClickEvent(InAppMessage inAppMessage, string inAppMessageID = null,
        Dictionary<string, decimal> received = null)
    {
        var e = OnClickInApp(inAppMessage, inAppMessageID, received);
        Kinoa.GameEvents.SendInAppClickEvent(e, PlayerState);
    }

    /// <summary>
    ///     Sends the In-app impression Game Event with the <see cref="InAppMessage.MessageId"/>.
    /// </summary>
    /// <param name="inAppMessage">The impressed In-app message.</param>
    /// <param name="inAppMessageID">The impressed In-app message identifier.</param>
    public void SendInAppImpressionEvent(InAppMessage inAppMessage, string inAppMessageID = null)
    {
        var e = OnInAppImpression(inAppMessage, inAppMessageID);
        Kinoa.GameEvents.SendInAppImpressionEvent(e);
    }

    /// <summary>
    ///     Sends multiple tutorial Game Events in a single request.
    /// </summary>
    public void SendEvents()
    {
        var startedTutorialEventData = OnTutorial(TutorialAction.Start);
        startedTutorialEventData.AddCustomParameters(new Dictionary<string, object> {{"tutorial_id", "tutorial_1"}});
        var finishedTutorialEventData = OnTutorial(TutorialAction.Finish);
        finishedTutorialEventData.AddCustomParameters(new Dictionary<string, object> {{"tutorial_id", "tutorial_2"}});
        var tutorialEventsCollection = new List<GameEventData> {startedTutorialEventData, finishedTutorialEventData};

        Kinoa.GameEvents.SendEvents(tutorialEventsCollection, PlayerState);
    }
}