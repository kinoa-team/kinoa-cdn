﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Kinoa.Data;
using Kinoa.Data.FeaturesSettings;
using Kinoa.Data.FeaturesSettings.Enum;
using UnityEngine;

/// <summary>
///     The sample Game Session Controller used to manage the Game Session.
/// </summary>
public class KinoaGameControllerWithFeatureSettings : MonoBehaviour
{
    // Loading overlay sample.
    public GameObject overlay;
    
    /// <summary>
    ///     On Start, initializes the Kinoa SDK, In-app Messaging, Push Notifications, and opens the Game Session.
    /// </summary>
    private async void Start()
    {
        if (KinoaSdkInitService.Instance.IsInitialized)
            return;

        using (new KinoaOverlay(overlay))
        {
            //Step 0 - Initialize the Kinoa SDK, and In-app Messaging services.
            await KinoaSdkInitService.Instance.InitializeAsync();
            await KinoaMessagingService.Instance.InitializeAsync();

            await LogInAndOpenSession();
        }
    }

    /// <summary>
    ///     Log in to the Player account and open Game Session.
    /// </summary>
    public async Task LogInAndOpenSession()
    {
        //Step 1 - Set Active Player ID (Log in).
        KinoaPlayerAccountService.Instance.LogInPlayer();

        //Step 2 - Get Player State of an Active Player.
        var playerState = await KinoaPlayerStateService.Instance.GetPlayerStateAsync();

        //Step 3 - Open the New Game Session with the Actual Player State.
        var sessionOpened = await OpenGameSessionAsync(playerState, new GameSessionData());

        //Step 4 - Load feature settings after session is opened.
        if (sessionOpened)
        {
            var settingsRequestParams = new List<FeatureSettingsSmartDownloadRequestParams>
            {
                new FeatureSettingsSmartDownloadRequestParams(key: "DailyBonus", version: 1, getDefault: false,
                    FeatureSettingsFailedDownloadStrategy.GetCachedOrBuiltIn, compressData: true),
                new FeatureSettingsSmartDownloadRequestParams(key: "WheelOfFortune", version: 1, getDefault: false,
                    FeatureSettingsFailedDownloadStrategy.GetCachedOrBuiltIn, compressData: true)
            };

            await KinoaFeaturesSettingsService.Instance.SmartDownloadAsync<FeatureSettingsData>(settingsRequestParams,
                configureChecksumLongPolling: true);

            var dailyBonusSettings = KinoaFeaturesSettingsService.Instance.LocalFeatureSettings
                .FirstOrDefault(x => x.Request.Key == "DailyBonus" && x.Request.Version == "1");
            var wheelOfFortuneSettings = KinoaFeaturesSettingsService.Instance.LocalFeatureSettings
                .FirstOrDefault(x => x.Request.Key == "WheelOfFortune" && x.Request.Version == "1");

            Debug.Log($"Daily Bonus Settings access: {dailyBonusSettings?.Data?.Count}" +
                      $"\nWheel Of Fortune Settings access: {wheelOfFortuneSettings?.Data?.Count}");
        }
    }

    /// <summary>
    ///     Sample method to retry opening of the failed Game Session, e.g., on returning to the lobby.
    ///     <remarks>
    ///     The method uses the <see cref="Kinoa.GameSession.ActiveSession"/> to retry the opening of the active Game Session
    ///     and save the same Session ID <see cref="GameSessionData.ID"/> for old and new Game Events.
    ///     </remarks>
    /// </summary>
    public async void RetryGameSessionOpen()
    {
        var gameSession = Kinoa.GameSession.ActiveSession ?? new GameSessionData();
        if (gameSession.IsOpened)
        {
            Debug.Log("The active Game Session is already opened, no need to retry.");
            return;
        }

        Debug.Log("The active Game Session is not opened, need to retry.");
        await OpenGameSessionAsync(KinoaPlayerStateService.Instance.PlayerState, gameSession);
    }

    /// <summary>
    ///     Sends the Session Open SDK request.
    ///     Sends the Session Start Game Event.
    /// </summary>
    /// <param name="playerState">The Player State.</param>
    /// <param name="gameSessionData">The Game Session Data.</param>
    private async Task<bool> OpenGameSessionAsync(CustomPlayerState playerState, GameSessionData gameSessionData)
    {
        //Open the Game Session.
        var response = await KinoaGameSessionService.Instance.OpenSessionAsync(playerState, gameSessionData);
        if (response.IsSuccessful())
        {
            //Send the Session Start Game Event.
            SendSessionStartEvent(playerState);
        }
        else
        {
            //TODO: Implement the retry logic on session opening failed e.g. on coming back to the lobby.
            //TODO: See RetryGameSessionOpenAsync sample method.
        }

        return response.IsSuccessful();
    }

    /// <summary>
    ///     Sends the Session Start Game Event.
    ///     <remarks>
    ///     <see cref="Kinoa.SyncGameEvents.SendSessionStartEventAsync{TPlayerState}"/> (Sync API) - returns the In-app messages in the response.
    ///     <see cref="Kinoa.GameEvents.SendSessionStartEvent{TPlayerState}"/> (Async API) - returns the In-app messages asynchronously via Web Socket channel.
    ///     </remarks>
    /// </summary>
    /// <param name="playerState">The Player State.</param>
    private void SendSessionStartEvent(CustomPlayerState playerState)
    {
        //Send the Session Start Game Event using the Async API.
        KinoaGameEventsService.Instance.SendSessionStartEvent(playerState);
    }
}