﻿using System;
using System.Threading.Tasks;
using Kinoa.Core.Network.Retry;
using Kinoa.Data.Enum;
using Kinoa.Data.Network;
using UnityEngine;

/// <summary>
///     The sample of the Kinoa SDK initialization service.
/// </summary>
public class KinoaSdkInitService : KinoaSingleton<KinoaSdkInitService>
{
    /// <summary>
    ///     Gets or sets a value indicating whether the SDK is initialized.
    /// </summary>
    public bool IsInitialized { get; private set; }
    
    /// <summary>
    ///    Kinoa Game unique identifier.
    /// </summary>
    private const string GameID = "TYPE_YOUR_GAME_ID_HERE";

    /// <summary>
    ///    Kinoa game secret token .
    /// </summary>
    private const string GameToken = "TYPE_YOUR_GAME_TOKEN_HERE";

    /// <summary>
    ///     Custom network requests timeout in seconds.
    /// </summary>
    public const int NetworkRequestsTimeout = 30;

    /// <summary>
    ///     Gets the SDK version number.
    /// </summary>
    public static string SDKVersion => Kinoa.SDK.Version;
    
    /// <summary>
    ///     Initialize and configure the SDK.
    /// </summary>
    /// <returns>True if the SDK is initialized, otherwise false.</returns>
    public async Task<bool> InitializeAsync()
    {
        if (IsInitialized)
            return true;

        if (string.IsNullOrEmpty(GameToken) || string.IsNullOrEmpty(GameID))
        {
            Debug.LogError("GameID or GameToken is not set.");
            return false;
        }

        IsInitialized = true;

        //Add the game secrets.
        var gameSecrets = new GameSecrets(GameID, GameToken, true);
        
        //Add the retry configuration for all network requests.
        var exponentialRetryConfig = new RetryConfiguration(
            retryReason: RetryReason.AlwaysRetry, 
            retryStrategy: RetryStrategy.Exponential,
            maxRetryAttempts: 5,
            maxRetryDelay: 15);
        
        /*var linearRetryConfig = new RetryConfiguration(
            retryReason: RetryReason.AlwaysRetry,
            retryStrategy: RetryStrategy.Linear,
            maxRetryAttempts: 5,
            retryDelay: 5);*/

        //Add the network configuration of all network sdk requests.
        var networkConfig = new NetworkConfiguration(NetworkRequestsTimeout, exponentialRetryConfig);
        
        //Add the configuration of the Tick Game Event (SDK heartbeat event).
        var tickEventsConfiguration = TickEventsConfiguration.GetCustom(60 * 1000);
        
        //Add the configuration of the Game Events security.
        var gameEventsSecurityConfiguration = new GameEventsSecurityConfiguration(true);
        
        //Add the configuration of the server time usage.
        var timeConfiguration = new TimeConfiguration(true);
        
        //Add the configuration of the language resolving.
        var languageConfiguration = new LanguageConfiguration(false);

        //Set SDK logging severity level.
        Kinoa.SDK.SetLogLevel(LogLevel.Trace);
        
        //Initialize the SDK.
        await Kinoa.SDK.Initialize(gameSecrets, networkConfig, tickEventsConfiguration, gameEventsSecurityConfiguration, timeConfiguration, languageConfiguration);

        return true;
    }
}