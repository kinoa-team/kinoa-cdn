This package contains third-party software components governed by the license(s) indicated below:

Component Name: Newtonsoft Json Unity Package

License Type: [Unity Companion License](https://unity3d.com/legal/licenses/Unity_Companion_License?_ga=2.196481259.631463143.1646053223-599372429.1616427546)

[Newtonsoft Json Unity Package](https://docs.unity3d.com/Packages/com.unity.nuget.newtonsoft-json@3.0/manual/index.html)