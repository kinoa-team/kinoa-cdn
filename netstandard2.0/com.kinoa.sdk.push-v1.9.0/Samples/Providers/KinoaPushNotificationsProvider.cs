﻿#if UNITY_ANDROID || UNITY_IOS
using Kinoa.PushNotifications.Data;
using UnityEngine;

/// <summary>
///     Kinoa Push Notifications SDK basic methods and properties provider sample.
/// </summary>
public static class KinoaPushNotificationsProvider
{
    /// <summary>
    ///     Block player pushes.
    /// </summary>
    public static async void BlockPushes()
    {
        var response = await Kinoa.PushNotifications.SDK.BlockPushes();
        Debug.Log($"Kinoa pushes block request completed with status: {response.Status}");
    }
    
    /// <summary>
    ///     Unblock player pushes.
    /// </summary>
    public static async void UnblockPushes()
    {
        var response = await Kinoa.PushNotifications.SDK.UnblockPushes();
        Debug.Log($"Kinoa pushes unblock request completed with status: {response.Status}");
    }
    
    /// <summary>
    ///     Delete device push token.
    /// </summary>
    public static async void DeleteToken()
    {
        var token = "FCM or APN device token";
        var response = await Kinoa.PushNotifications.SDK.DeleteToken(token);
        Debug.Log($"Kinoa delete token completed with status: {response.Status}");
    }
    
    /// <summary>
    ///     Check pushes status on Kinoa.
    /// </summary>
    public static async void CheckPushStatus()
    {
        var response = await Kinoa.PushNotifications.SDK.GetPushStatus();
        if (response.IsSuccessful())
        {
            Debug.Log($"Kinoa pushes are blocked: {response.Data.Blocked}");
        }
        else
        {
            Debug.Log($"Get push status request status: {response.Status}.");
        }
    }
    
    /// <summary>
    ///     Set personalization information.
    /// </summary>
    public static async void SetPersonalizationInfo()
    {
        var personalizationInfo = new KinoaPushPersonalizationInfo()
        {
            Name = "KinoaPlayer",
            Country = "KinoaCountry",
            City = "KinoaCity",
            Extra1 = "KinoaExtra1",
            Extra2 = "KinoaExtra2",
            Extra3 = "KinoaExtra3",
        };
        var response = await Kinoa.PushNotifications.SDK.SetPushPersonalizationInfo(personalizationInfo);
        Debug.Log($"Kinoa set personalization info completed with status: {response.Status}");
    }
    
    /// <summary>
    ///     Get personalization information.
    /// </summary>
    public static async void GetPersonalizationInfo()
    {
        var response = await Kinoa.PushNotifications.SDK.GetPushPersonalizationInfo();
        if (response.IsSuccessful())
        {
            Debug.Log($"Kinoa get personalization info completed successfully:.\n" +
                      $"Name: \"{response.Data?.Name}\". " +
                      $"City: \"{response.Data?.City}\"." +
                      $"Country: \"{response.Data?.Country}\"." +
                      $"Extra 1: \"{response.Data?.Extra1}\"." +
                      $"Extra 2: \"{response.Data?.Extra2}\"." +
                      $"Extra 3: \"{response.Data?.Extra3}\"");
        }
        else
        {
            Debug.Log($"Kinoa get personalization info completed with status: {response.Status}");
        }
    }
    
    /// <summary>
    ///     Delete personalization information.
    /// </summary>
    public static async void DeletePersonalizationInfo()
    {
        var response = await Kinoa.PushNotifications.SDK.DeletePushPersonalizationInfo();
        Debug.Log($"Kinoa delete personalization info completed with status: {response.Status}");
    }
}
#endif