This package contains third-party software components governed by the license(s) indicated below:


Component Name: Newtonsoft Json Unity Package

License Type: [Unity Companion License](https://unity3d.com/legal/licenses/Unity_Companion_License?_ga=2.196481259.631463143.1646053223-599372429.1616427546)

[Newtonsoft Json Unity Package](https://docs.unity3d.com/Packages/com.unity.nuget.newtonsoft-json@3.0/manual/index.html)



Component Name: Mobile Notifications copyright © 2020 Unity Technologies ApS

License Type: [Unity Companion License](https://unity.com/legal/licenses/unity-companion-license)

[Mobile Notifications](https://docs.unity3d.com/Packages/com.unity.mobile.notifications@2.0/manual/index.html)



Component Name: Firebase Cloud Messaging

License Type: Apache License Version 2.0, January 2004(http://www.apache.org/licenses/)

[Firebase Cloud Messaging](https://developers.google.com/unity/archive#firebase_cloud_messaging)