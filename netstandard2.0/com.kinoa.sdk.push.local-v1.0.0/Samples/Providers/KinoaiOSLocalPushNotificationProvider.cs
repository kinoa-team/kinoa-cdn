#if UNITY_IOS
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Kinoa.PushNotifications.iOS.Clients;
using Kinoa.PushNotifications.iOS.Local.Clients;
using Kinoa.PushNotifications.iOS.Local.Data;
using Unity.Notifications.iOS;
using UnityEngine;

/// <summary>
///     Kinoa local push notification sample.
/// </summary>
public class KinoaiOSLocalPushNotificationProvider
{
    private readonly KinoaiOSLocalNotificationClient _localNotificationClient;

    public KinoaiOSLocalPushNotificationProvider()
    {
        _localNotificationClient = new KinoaiOSLocalNotificationClient();
        _localNotificationClient.OnLocalPushNotificationReceived += OnLocalPushNotificationReceived;
    }

    /// <summary>
    ///     Sample shows how to handle notification received.
    /// </summary>
    private void OnLocalPushNotificationReceived(iOSNotification notification)
    {
        Debug.Log($"[GAME] Local push notification received.");
    }

    /// <summary>
    ///     Sample shows how to schedule notification.
    /// </summary>
    public iOSNotification ScheduleNotification(KinoaiOSLocalNotificationData localNotificationData)
    {
        var scheduledNotification = _localNotificationClient.ScheduleLocalNotification(localNotificationData);
        return scheduledNotification;
    }
    
    /// <summary>
    ///     Sample shows how to reschedule notification.
    /// </summary>
    public iOSNotification RescheduleNotification(string notificationId, KinoaiOSLocalNotificationData localNotificationData)
    {
        var scheduledNotification = _localNotificationClient.RescheduleLocalNotification(notificationId, localNotificationData);
        return scheduledNotification;
    }

    /// <summary>
    ///     Sample shows how to cancel notification.
    /// </summary>
    public void CancelNotification(string notificationId)
    {
        _localNotificationClient.CancelLocalNotification(notificationId);
    }

    /// <summary>
    ///     Sample shows how to get notifications that are currently scheduled.
    /// </summary>
    public List<iOSNotification> GetScheduledNotifications()
    {
        var scheduledNotifications = _localNotificationClient.GetScheduledNotifications();
        LogNotifications(scheduledNotifications, nameof(GetScheduledNotifications));
        return scheduledNotifications;
    }

    /// <summary>
    ///     Logs the notifications.
    /// </summary>
    /// <param name="notifications">Notification array.</param>
    /// <param name="source">The source of notification array.</param>
    private void LogNotifications(List<iOSNotification> notifications, string source)
    {
        var logBuilder = new StringBuilder();
        logBuilder.AppendFormat("[GAME] {0}:{1}", source, notifications.Count);

        foreach (var notification in notifications)
            logBuilder.AppendFormat("\nID: {0}", notification.Identifier);

        Debug.Log(logBuilder.ToString());
    }

    /// <summary>
    ///     Sample shows how to create interval notification.
    /// </summary>
    public async Task<KinoaiOSLocalNotificationData> GetIntervalLocalNotification(
        TimeSpan timeSpan,
        bool repeatable,
        bool dismiss,
        string imageUrl,
        bool rescheduled = false)
    {
        return await new KinoaiOSLocalNotificationData()
            .SetTitle((rescheduled ? "Rescheduled " : "") + "Local push notification title")
            .SetSubtitle("Local push notification subtitle")
            .SetBody("Local push notification text")
            .SetDismiss(dismiss)
            .SetTimeIntervalTrigger(timeSpan, repeatable)
            .SetImageAsync(imageUrl);
    }

    /// <summary>
    ///     Sample shows how to create calendar notification.
    /// </summary>
    public async Task<KinoaiOSLocalNotificationData> GetCalendarLocalNotification(
        DateTime dateTime,
        bool dismiss,
        string imageUrl)
    {
        return await new KinoaiOSLocalNotificationData()
            .SetTitle("Local push notification title")
            .SetSubtitle("Local push notification subtitle")
            .SetBody("Local push notification text")
            .SetDismiss(dismiss)
            .SetTimeCalendarTrigger(dateTime)
            .SetImageAsync(imageUrl);
    }
}
#endif