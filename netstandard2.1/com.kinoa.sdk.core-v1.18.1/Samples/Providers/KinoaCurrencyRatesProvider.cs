using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kinoa.Data;
using Kinoa.Data.Enum;
using UnityEngine;

/// <summary>
///     Kinoa currency rates provider sample.
/// </summary>
public class KinoaCurrencyRatesProvider
{
    public async Task<Response<Dictionary<string, double>>> GetStringCurrencyRates()
    {
        var response = await Kinoa.CurrencyRates.GetStringCurrencyRatesAsync();

        if (response.IsSuccessful() && response.Data != null)
        {
            var currencyRates = response.Data
                .Select(y => $"{y.Key}: {y.Value}")
                .ToArray();

            var logBuilder = new StringBuilder();
            logBuilder.Append($"Currency rates received successfully:" +
                              $"    \n{string.Join("    \n", currencyRates)}");
            Debug.Log(logBuilder.ToString());
        }
        else
        {
            Debug.Log($"Get currency rates request status: {response.Status.ToString()}.");
        }

        return response;
    }
    
    public async Task<Response<Dictionary<Currency, double>>> GetCurrencyRates()
    {
        var response = await Kinoa.CurrencyRates.GetCurrencyRatesAsync();

        if (response.IsSuccessful() && response.Data != null)
        {
            var currencyRates = response.Data
                .Select(y => $"{y.Key.ToString()}: {y.Value}")
                .ToArray();

            var logBuilder = new StringBuilder();
            logBuilder.Append($"Currency rates received successfully:" +
                              $"    \n{string.Join("    \n", currencyRates)}");
            Debug.Log(logBuilder.ToString());
        }
        else
        {
            Debug.Log($"Get currency rates request status: {response.Status.ToString()}.");
        }

        return response;
    }

}
