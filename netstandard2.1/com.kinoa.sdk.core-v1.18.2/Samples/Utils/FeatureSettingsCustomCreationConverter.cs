﻿using Kinoa.Core.Utils.Json.Converters;
using Kinoa.Data.FeaturesSettings;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using UnityEngine;

/// <summary>
///     Feature Settings custom creation converter.
/// </summary>
public class FeatureSettingsCustomCreationConverter : FeatureSettingsCreationConverter
{
    /// <summary>
    ///     Feature Settings data <see cref="Kinoa.Data.FeaturesSettings.FeatureSettingsResponse{TData}.Data"/>
    ///     custom serializer settings. Is optional and can be null.
    /// </summary>
    protected override JsonSerializerSettings JsonSerializerSettings => new JsonSerializerSettings()
    {
        ContractResolver = new DefaultContractResolver {NamingStrategy = new DefaultNamingStrategy()},
        MissingMemberHandling = MissingMemberHandling.Ignore,
        DateParseHandling = DateParseHandling.None
    };

    /// <summary>
    ///     Creates an settings data object <see cref="Kinoa.Data.FeaturesSettings.FeatureSettingsResponse{TData}.Data"/>
    ///     based on Feature Settings key <see cref="Kinoa.Data.FeaturesSettings.FeatureSettingsRequestParams.Key"/>
    ///     which will then be populated by the serializer using the provided JsonSerializerSettings
    ///     <see cref="JsonSerializerSettings"/>.
    /// </summary>
    /// <param name="featureSettingsKey">A Feature Settings key that points to the data model.</param>
    /// <returns>Concrete Feature Settings data model based on featureSettingsKey.</returns>
    protected override FeatureSettingsData Create(string featureSettingsKey)
    {
        switch (featureSettingsKey)
        {
            case "DailyBonus":
                return new DailyBonusSettings();
            case "WheelOfFortune":
                return new WheelOfFortuneSettings();
            default:
                Debug.LogWarning($"{nameof(FeatureSettingsData)} cast failed: " +
                                 $"Unknown Feature Settings key '{featureSettingsKey}'.");
                return null;
        }
    }
}