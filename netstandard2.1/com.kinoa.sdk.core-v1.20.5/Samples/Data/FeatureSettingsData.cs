﻿using System.Text.Json.Serialization;

/// <summary>
///     The polymorphic Feature Settings data model.
///     Concrete Feature Settings data model client classes should inherit from this class.
/// </summary>
[JsonPolymorphic(TypeDiscriminatorPropertyName = "$type", IgnoreUnrecognizedTypeDiscriminators = true)]
[JsonDerivedType(typeof(DailyBonusSettings), "DailyBonus")]
[JsonDerivedType(typeof(WheelOfFortuneSettings), "WheelOfFortune")]
public class FeatureSettingsData
{
}