﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Kinoa.Data;
using Kinoa.Data.Enum;
using Kinoa.Data.Events;
using Kinoa.Data.Messaging.InApp;
using Kinoa.Data.Network;
using Kinoa.Data.State;
using UnityEngine;

/// <summary>
///     Kinoa Asynchronous Game Events service.
/// </summary>
public class KinoaGameEventsService<TPlayerState> :
    KinoaGameEventBuildingService<KinoaGameEventsService<TPlayerState>, TPlayerState>
    where TPlayerState : PlayerState
{
    /// <summary>
    ///     Initializes the new instance of <see cref="KinoaGameEventsService{T}"/>.
    ///     Subscribes to the SDK game session events.
    /// </summary>
    public KinoaGameEventsService()
    {
        Kinoa.GameEvents.OnGameSessionStarted += OnGameSessionStarted;
        Kinoa.GameEvents.OnGameSessionStartFailed += OnGameSessionStartFailed;
    }

    /// <summary>
    ///     Starts the new game session and send Start Session Game Event.
    /// </summary>
    /// <param name="customParams">Custom event custom parameters.</param>
    public async Task StartGameSessionAsync(Dictionary<string, object> customParams = null)
    {
        var e = new StartSessionEventData();
        e.SetCustomParams(customParams);
        PlayerState?.SessionData.SetDeepLink("SomeDeepLink");

        const int timeout = 10;
        var networkConfig = new NetworkConfiguration(timeout);

        await Kinoa.GameEvents.SendStartSessionEvent(e, PlayerState, networkConfig);
    }

    /// <summary>
    ///     On the new game session successfully started and registered on Kinoa backend.
    /// </summary>
    /// <param name="response">Response of successful session start request.</param>
    private void OnGameSessionStarted(Response<string> response) =>
        Debug.Log($"The new game session is successfully started: {response.IsSuccessful()}.");

    /// <summary>
    ///     On failed to start the new game session.
    /// </summary>
    /// <param name="response">Response of unsuccessful session start request.</param>
    private void OnGameSessionStartFailed(Response<string> response)
    {
        var message = $"The new game session opening failed: {response.Error?.Code}. " +
                      $"\nResponse status: {response.Status.ToString()}";
        if (PlayerState == null)
        {
            message += "\nReason: PlayerState is null or empty.";
        }

        Debug.Log(message);
        DialogController.Instance.ShowToast(message);
    }

    /// <summary>
    ///     Sends the Progression Game Event.
    /// </summary>
    public void SendProgressionEvent()
    {
        var e = OnProgression();
        Kinoa.GameEvents.SendProgressionEvent(e, PlayerState);
    }

    /// <summary>
    ///     Sends the Level Up Game Event.
    /// </summary>
    /// <param name="customParams">Custom event custom parameters.</param>
    public void SendLevelUpEvent(Dictionary<string, object> customParams = null)
    {
        var e = OnLevelUp(customParams);
        Kinoa.GameEvents.SendLevelUpEvent(e, PlayerState);
    }

    /// <summary>
    ///     Sends the Payment Game Event.
    /// </summary>
    /// <param name="productId">Shop product identifier in market.</param>
    /// <param name="spent">Spent real money.</param>
    /// <param name="isoCurrencyCode">Real money currency in ISO 4217 format.</param>
    /// <param name="received">Received in-game resources.</param>
    public void SendPaymentEvent(string productId = "play-market-product-id",
        decimal spent = 0.99m,
        string isoCurrencyCode = "USD",
        Dictionary<string, decimal> received = null)
    {
        var e = OnPayment(productId, spent, isoCurrencyCode, received);
        Kinoa.GameEvents.SendPaymentEvent(e, PlayerState);
    }

    /// <summary>
    ///     Sends the Watch Ad Game Event.
    /// </summary>
    /// <param name="incomeFromAd">Real money income from ad.</param>
    /// <param name="isoCurrencyCode">Real money currency in ISO 4217 format.</param>
    /// <param name="adType">Ad type.</param>
    /// <param name="received">Received in-game resources.</param>
    public void SendWatchAdEvent(
        decimal incomeFromAd = 40m,
        string isoCurrencyCode = "UAH",
        string adType = "simple_ad",
        Dictionary<string, decimal> received = null)
    {
        var e = OnWatchAd(incomeFromAd, isoCurrencyCode, adType, received);
        Kinoa.GameEvents.SendWatchAdEvent(e, PlayerState);
    }

    /// <summary>
    ///     Sends the In-game purchase Game Event.
    /// </summary>
    /// <param name="spent">Spent in-game resources.</param>
    /// <param name="received">Received in-game resources.</param>
    public void SendInGamePurchaseEvent(Dictionary<string, decimal> spent = null,
        Dictionary<string, decimal> received = null)
    {
        var e = OnInGamePurchase(spent, received);
        Kinoa.GameEvents.SendInGamePurchaseEvent(e, PlayerState);
    }

    /// <summary>
    ///     Sends the Tutorial Game Event.
    /// </summary>
    /// <param name="action">Tutorial action.</param>
    public void SendTutorialEvent(TutorialAction action = TutorialAction.Finish)
    {
        var e = OnTutorial(action);
        Kinoa.GameEvents.SendTutorialEvent(e, PlayerState);
    }

    /// <summary>
    ///     Sends the Social connect Game Event.
    /// </summary>
    public void SendSocialConnectEvent()
    {
        var e = OnSocialConnect();
        Kinoa.GameEvents.SendSocialConnectEvent(e, PlayerState);
    }

    /// <summary>
    ///     Sends the Social disconnect Game Event.
    /// </summary>
    public void SendsSocialDisconnectEvent()
    {
        var e = OnSocialDisconnect();
        Kinoa.GameEvents.SendSocialDisconnectEvent(e, PlayerState);
    }

    /// <summary>
    ///     Sends the Social post Game Event.
    /// </summary>
    public void SendSocialPostEvent()
    {
        var e = OnSocialPost();
        Kinoa.GameEvents.SendSocialPostEvent(e, PlayerState);
    }

    /// <summary>
    ///     Sends the Custom Game Event.
    /// </summary>
    /// <param name="name">Custom event name.</param>
    /// <param name="customParams">Custom event custom parameters.</param>
    public void SendCustomEvent(string name = "custom_event", Dictionary<string, object> customParams = null)
    {
        var e = OnCustomEvent(name, customParams);
        Kinoa.GameEvents.SendCustomEvent(e, PlayerState);
    }

    /// <summary>
    ///     Sends the Cheating Game Event.
    /// </summary>
    public void SendCheatingEvent()
    {
        var e = OnCheatingEvent();
        Kinoa.GameEvents.SendCustomEvent(e, PlayerState);
    }

    /// <summary>
    ///     Sends the Tester Game Event.
    /// </summary>
    public void SendTesterEvent()
    {
        var e = OnTesterEvent();
        Kinoa.GameEvents.SendCustomEvent(e, PlayerState);
    }

    /// <summary>
    ///     Sends the Error Game Event.
    /// </summary>
    /// <param name="exception">Thrown exception.</param>
    public void SendErrorEvent(Exception exception)
    {
        var e = OnError(exception);
        Kinoa.GameEvents.SendErrorEvent(e);
    }

    /// <summary>
    ///     Sends the In-app close Game Event.
    /// </summary>
    /// <param name="inAppMessage">The closed In-app message.</param>
    /// <param name="inAppMessageID">The closed In-app message identifier.</param>
    public void SendInAppCloseEvent(InAppMessage inAppMessage, string inAppMessageID = null)
    {
        var e = OnCloseInApp(inAppMessage, inAppMessageID);
        Kinoa.GameEvents.SendInAppCloseEvent(e, PlayerState);
    }

    /// <summary>
    ///     Sends the In-app click Game Event.
    /// </summary>
    /// <param name="inAppMessage">The clicked In-app message.</param>
    /// <param name="inAppMessageID">The clicked In-app message identifier.</param>
    /// <param name="received">Received in-game resources.</param>
    public void SendInAppClickEvent(InAppMessage inAppMessage, string inAppMessageID = null,
        Dictionary<string, decimal> received = null)
    {
        var e = OnClickInApp(inAppMessage, inAppMessageID, received);
        Kinoa.GameEvents.SendInAppClickEvent(e, PlayerState);
    }

    /// <summary>
    ///     Sends the In-app impression Game Event with the <see cref="InAppMessage.MessageId"/>.
    /// </summary>
    /// <param name="inAppMessage">The impressed In-app message.</param>
    /// <param name="inAppMessageID">The impressed In-app message identifier.</param>
    public void SendInAppImpressionEvent(InAppMessage inAppMessage, string inAppMessageID = null)
    {
        var e = OnInAppImpression(inAppMessage, inAppMessageID);
        Kinoa.GameEvents.SendInAppImpressionEvent(e);
    }
}