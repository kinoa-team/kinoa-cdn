﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using Kinoa.Core.Utils.Helpers;
using Kinoa.Data.Enum;
using Kinoa.Data.Events;
using Kinoa.Data.Messaging.InApp;
using Kinoa.Data.State;
using UnityEngine;
using Utils;
using Utils.Helpers;

/// <summary>
///     Kinoa Game Event building service.
/// </summary>
/// <typeparam name="TService">Type of the custom Player State model.</typeparam>
/// <typeparam name="T">Type of the custom Player State model.</typeparam>
public abstract class KinoaGameEventBuildingService<T, TService> : KinoaSingleton<TService>
    where TService : new()
    where T : class, IPlayerState
{
    /// <summary>
    ///     The lobby place.
    /// </summary>
    private const string LobbyPlace = "Lobby";

    /// <summary>
    ///     The game arena place.
    /// </summary>
    private const string GameArenaPLace = "GameArena";

    /// <summary>
    ///     The shop place.
    /// </summary>
    private const string ShopPlace = "Shop";

    /// <summary>
    ///     The current player state.
    ///     To set current player state on game start <see cref="Kinoa.Player.GetState{T}"/>
    /// </summary>
    protected T PlayerState => typeof(T) == typeof(PlayerStateDictionary) 
        ? KinoaPlayerStateService<PlayerStateDictionary>.Instance.PlayerState as T 
        : KinoaPlayerStateService<CustomPlayerState>.Instance.PlayerState as T;
    
    /// <summary>
    ///     Implements logic of Player balance update.
    /// </summary>
    private void UpdateBalance(Dictionary<string, decimal> spent, Dictionary<string, decimal> received)
    {
        // Implements logic of Player balance update
    }

    /// <summary>
    ///     Implements logic of Player progress update.
    /// </summary>
    private void UpdateProgress()
    {
        // Implements logic of Player progress update
    }

    /// <summary>
    ///     On player closes the In-app message.
    ///     Sends the In-app close event with the <see cref="InAppMessage.MessageId"/>.
    /// </summary>
    /// <param name="inAppMessage">The closed In-app message.</param>
    /// <param name="inAppMessageID">The closed In-app message identifier.</param>
    /// <returns>In-app message close Event Data.</returns>
    protected InAppCloseEventData OnCloseInApp(InAppMessage inAppMessage, string inAppMessageID = null)
    {
        var e = new InAppCloseEventData(inAppMessage?.MessageId ??
                                        (!string.IsNullOrEmpty(inAppMessageID) ? inAppMessageID : "SomeInAppID"));
        e.SetPlace(LobbyPlace);

        return e;
    }

    /// <summary>
    ///     On player clicks the In-app message.
    ///     Sends the In-app click event with the <see cref="InAppMessage.MessageId"/>.
    /// </summary>
    /// <param name="inAppMessage">The clicked In-app message.</param>
    /// <param name="inAppMessageID">The clicked In-app message identifier.</param>
    /// <param name="received">Received in-game resources.</param>
    /// <returns>In-app message click Event Data.</returns>
    protected InAppClickEventData OnClickInApp(InAppMessage inAppMessage, string inAppMessageID = null,
        Dictionary<string, decimal> received = null)
    {
        UpdateBalance(null, received);

        var e = new InAppClickEventData(inAppMessage?.MessageId ??
                                        (!string.IsNullOrEmpty(inAppMessageID) ? inAppMessageID : "SomeInAppID"));
        e.SetPlace(LobbyPlace);
        e.SetReceived(received);

        return e;
    }

    /// <summary>
    ///     On player impresses the In-app message.
    ///     Sends the In-app impression event with the <see cref="InAppMessage.MessageId"/>.
    /// </summary>
    /// <param name="inAppMessage">The impressed In-app message.</param>
    /// <param name="inAppMessageID">The impressed In-app message identifier.</param>
    /// <returns>In-app message impression Event Data.</returns>
    protected InAppImpressionEventData OnInAppImpression(InAppMessage inAppMessage, string inAppMessageID = null)
    {
        var e = new InAppImpressionEventData(inAppMessage?.MessageId ??
                                             (!string.IsNullOrEmpty(inAppMessageID) ? inAppMessageID : "SomeInAppID"));
        e.SetPlace(LobbyPlace);

        return e;
    }
    
    /// <summary>
    ///     On player starts the game session.
    /// </summary>
    /// <returns>Start Session Event Data.</returns>
    protected StartSessionEventData OnGameSessionStart()
    {
        var e = new StartSessionEventData();
        e.AddCustomParameters(new Dictionary<string, object> {{"TestCustomParameterKey", "TestCustomParameterValue"}});
        
        return e;
    }

    /// <summary>
    ///     On trying to improve/increase player's in-game progress.
    /// </summary>
    /// <returns>Progression Event Data.</returns>
    protected ProgressionEventData OnProgression()
    {
        UpdateProgress();

        var e = new ProgressionEventData();
        e.SetPlace(GameArenaPLace);
        e.SetDuration(60L);

        return e;
    }

    /// <summary>
    ///     On player level up handler.
    /// </summary>
    /// <param name="customParams">Custom event custom parameters.</param>
    /// <returns>Level Up Event Data.</returns>
    protected LevelUpEventData OnLevelUp(Dictionary<string, object> customParams = null)
    {
        var newLevel = PlayerState?.Level == null ? 5 : (int) PlayerState.Level + 1;
        PlayerState?.SetLevel(newLevel);
        UpdateProgress();

        var e = new LevelUpEventData();
        e.SetLevel(newLevel);
        e.SetPlace(GameArenaPLace);
        e.SetDuration(60L);
        e.AddCustomParameters(customParams);

        return e;
    }

    /// <summary>
    ///     On purchase for the real money handler.
    /// </summary>
    /// <param name="productId">Shop product identifier in market.</param>
    /// <param name="spent">Spent real money.</param>
    /// <param name="isoCurrencyCode">Real money currency in ISO 4217 format.</param>
    /// <param name="received">Received in-game resources.</param>
    /// <returns>Payment Event Data.</returns>
    protected PaymentEventData OnPayment(string productId = "play-market-product-id",
        decimal spent = 0.99m,
        string isoCurrencyCode = "USD",
        Dictionary<string, decimal> received = null)
    {
        if (!CurrencyHelper.TryConvert(isoCurrencyCode, out var currency))
        {
            Debug.Log($"Currency \"{isoCurrencyCode}\" is not supported.");
            return null;
        }

        if (PlayerState is CustomPlayerState cps)
        {
            cps.CustomDateProperty = DateTime.Now;
        }
        
        if (PlayerState is PlayerStateDictionary psd)
        {
            psd["custom_date_property"] = DateTime.Now;
        }

        UpdateBalance(null, received);

        var inApp = KinoaMessagingService.Instance.LocalInboxStorage.FirstOrDefault();

        var e = new PaymentEventData(productId, spent, currency, received, inApp);
        e.SetPlace(ShopPlace);
        if (PlayerState?.Level != null)
            e.SetLevel((int) PlayerState.Level);

        return e;
    }

    /// <summary>
    ///     On watch ad for the real money handler.
    /// </summary>
    /// <param name="incomeFromAd">Real money income from ad.</param>
    /// <param name="isoCurrencyCode">Real money currency in ISO 4217 format.</param>
    /// <param name="adType">Ad type.</param>
    /// <param name="received">Received in-game resources.</param>
    /// <returns>Watch Ad Event Data.</returns>
    protected WatchAdEventData OnWatchAd(
        decimal incomeFromAd = 40m,
        string isoCurrencyCode = "UAH",
        string adType = "simple_ad",
        Dictionary<string, decimal> received = null)
    {
        if (!CurrencyHelper.TryConvert(isoCurrencyCode, out var currency))
        {
            Debug.Log($"Currency \"{isoCurrencyCode}\" is not supported.");
            return null;
        }

        UpdateBalance(null, received);
        
        var inApp = KinoaMessagingService.Instance.LocalInboxStorage.FirstOrDefault();

        var e = new WatchAdEventData(incomeFromAd, currency, adType, received, inApp);
        e.SetPlace(ShopPlace);
        if (PlayerState?.Level != null)
            e.SetLevel((int) PlayerState.Level);

        return e;
    }

    /// <summary>
    ///     On in-game purchase handler.
    /// </summary>
    /// <param name="spent">Spent in-game resources.</param>
    /// <param name="received">Received in-game resources.</param>
    /// <returns>In-game Purchase Event Data.</returns>
    protected InGamePurchaseEventData OnInGamePurchase(Dictionary<string, decimal> spent = null,
        Dictionary<string, decimal> received = null)
    {
        UpdateBalance(spent, received);
        
        var inApp = KinoaMessagingService.Instance.LocalInboxStorage.FirstOrDefault();

        var e = new InGamePurchaseEventData(spent, received, inApp);
        e.SetPlace(ShopPlace);
        if (PlayerState?.Level != null)
            e.SetLevel((int) PlayerState.Level);

        return e;
    }

    /// <summary>
    ///     On tutorial completed handler.
    /// </summary>
    /// <param name="action">Tutorial action.</param>
    /// <returns>Tutorial Event Data.</returns>
    protected TutorialEventData OnTutorial(TutorialAction action = TutorialAction.Finish)
    {
        var e = new TutorialEventData(action);
        e.SetStep(1);
        e.SetPlace(GameArenaPLace);
        if (PlayerState?.Level != null)
            e.SetLevel((int) PlayerState.Level);

        return e;
    }
    
    /// <summary>
    ///     On collected resource completed handler.
    /// </summary>
    /// <returns>Collected resource Event Data.</returns>
    protected CollectedResourceEventData OnCollectedResource()
    {
        var inApp = KinoaMessagingService.Instance.LocalInboxStorage.FirstOrDefault();
        var e = new CollectedResourceEventData(inApp);
        e.SetPlace(GameArenaPLace);
        if (PlayerState?.Level != null)
            e.SetLevel((int) PlayerState.Level);

        return e;
    }

    /// <summary>
    ///     On player social network connect handler.
    ///     Sets player social networks with mocked IDs.
    /// </summary>
    /// <returns>Social Connect Event Data.</returns>
    protected SocialConnectEventData OnSocialConnect()
    {
        if (PlayerState is CustomPlayerState cps)
        {
            //TODO: Use the real social network IDs instead of mocked GUIDs.
            cps?.PlayerIdentifiers
                .SetFacebookId(Guid.NewGuid().ToString())
                .SetGoogleId(Guid.NewGuid().ToString())
                .SetAppleId(Guid.NewGuid().ToString());
        }
        
        if (PlayerState is PlayerStateDictionary psd)
        {
            //TODO: Use the real social network IDs instead of mocked GUIDs.
            if (psd["player_identifiers"] is ConcurrentDictionary<string, object> playerIdentifiers)
            {
                playerIdentifiers["facebook_id"] = Guid.NewGuid().ToString();
                playerIdentifiers["google_id"] = Guid.NewGuid().ToString();
                playerIdentifiers["apple_id"] = Guid.NewGuid().ToString();
            }
        }

        return new SocialConnectEventData();
    }
    
    /// <summary>
    ///     On player social network disconnect handler.
    ///     Clears information about the connected player social networks.
    /// </summary>
    /// <returns>Social Disconnect Event Data.</returns>
    protected SocialDisconnectEventData OnSocialDisconnect()
    {
        if (PlayerState is CustomPlayerState cps)
        {
            cps?.PlayerIdentifiers
                .SetFacebookId(null)
                .SetGoogleId(null)
                .SetAppleId(null);
        }
        
        if (PlayerState is PlayerStateDictionary psd)
        {
            if (psd["player_identifiers"] is ConcurrentDictionary<string, object> playerIdentifiers)
            {
                playerIdentifiers["facebook_id"] = null;
                playerIdentifiers["google_id"] = null;
                playerIdentifiers["apple_id"] = null;
            }
        }

        return new SocialDisconnectEventData();
    }

    /// <summary>
    ///     On post in social networks handler.
    /// </summary>
    /// <returns>Social Post Event Data.</returns>
    protected SocialPostEventData OnSocialPost()
    {
        var e = new SocialPostEventData();
        e.SetPlace(LobbyPlace);
        if (PlayerState?.Level != null)
            e.SetLevel((int) PlayerState.Level);

        return e;
    }

    /// <summary>
    ///     On custom event handler.
    /// </summary>
    /// <param name="name">Custom event name.</param>
    /// <param name="customParams">Custom event custom parameters.</param>
    /// <returns>Custom Event Data.</returns>
    protected CustomEventData OnCustomEvent(string name = "custom_event",
        Dictionary<string, object> customParams = null)
    {
        if (PlayerState is CustomPlayerState cps)
        {
            cps?.PersonalInfo
                .SetCountry(Country.Ukraine)
                .SetCity("Kyiv");
        }

        if (PlayerState is PlayerStateDictionary psd)
        {
            if (!psd.ContainsKey("personal_info"))
            {
                psd["personal_info"] = new ConcurrentDictionary<string, object>();
            }

            if (psd["personal_info"] is ConcurrentDictionary<string, object> personalInfo)
            {
                personalInfo["city"] = "Kyiv";
                personalInfo["country_code"] = PlayerStateHelper.GetCountryCode(Country.Ukraine);
            }
        }

        var e = new CustomEventData(name);
        /*e.AddCustomParameters(new Dictionary<string, object>
        {
            ["ID"] = 1.0,
            ["custom_key_2"] = "custom_value_2",
            ["camelCase"] = true
        });*/
        e.AddCustomParameters(customParams);

        return e;
    }

    /// <summary>
    ///     On the Player cheating.
    ///     Marks the Player as a cheater and blocks the Player.
    /// </summary>
    /// <returns>Cheating Event Data.</returns>
    protected CustomEventData OnCheatingEvent()
    {
        if (PlayerState == null)
            return null;

        var isCheater = !PlayerState.IsCheater;
        PlayerState?
            .SetIsCheater(isCheater)
            .SetIsBlocked(isCheater);

        var e = new CustomEventData("cheating_event");
        return e;
    }

    /// <summary>
    ///     On the Player is tester.
    ///     Marks the Player as a tester.
    /// </summary>
    /// <returns>Tester Event Data.</returns>
    protected CustomEventData OnTesterEvent()
    {
        if (PlayerState == null)
            return null;

        var isTester = !PlayerState.IsTester;
        PlayerState?.SetIsTester(isTester);

        var e = new CustomEventData("tester_event");
        return e;
    }

    /// <summary>
    ///     On client-side error handler.
    /// </summary>
    /// <param name="exception">Thrown exception.</param>
    /// <returns>Error Event Data.</returns>
    protected ErrorEventData OnError(Exception exception)
    {
        var e = new ErrorEventData(exception);
        e.AddCustomParameters(new Dictionary<string, object>
            {["additional_message"] = "Something went wrong during e.g. SDK response deserialization."});

        return e;
    }
}