﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Kinoa.Data;
using Kinoa.Data.ResourceManagement;
using UnityEngine;

/// <summary>
///     Kinoa Bundles service.
/// </summary>
public class KinoaBundlesService : KinoaSingleton<KinoaBundlesService>
{
    /// <summary>
    ///     Gets the multiple bundle resources by keys.
    /// </summary>
    /// <param name="bundleKeys">Collection of bundle keys.</param>
    /// <param name="cancellationToken">Cancellation token.</param>
    /// <returns>
    ///     The processed API response with a dictionary of bundle resources.
    ///     The key is the bundle key, the value is the collection of bundle resources.
    /// </returns>
    public async Task<Response<BundleResources>> GetBundleResourcesAsync(
        List<string> bundleKeys, CancellationToken cancellationToken = default)
    {
        if (bundleKeys == null || bundleKeys.Count == 0)
        {
            bundleKeys = new List<string> {"demoBundle"};
        }

        if (cancellationToken == default)
        {
            var cancellationTokenSource = new CancellationTokenSource();
            cancellationTokenSource.CancelAfter(5 * 1000);
            cancellationToken = cancellationTokenSource.Token;
        }

        var response = await Kinoa.Bundles.GetBundleResourcesAsync(bundleKeys, cancellationToken);
        if (response.IsSuccessful())
        {
            if (response.Data?.BundleResourceBodiesDto == null || response.Data.BundleResourceBodiesDto.Count == 0)
            {
                Debug.Log("The bundles are empty.");
                return response;
            }

            var resourcesByBundleKey = response.Data.BundleResourceBodiesDto
                .Select(y => $"{y.Key}: " +
                             $"{string.Join(", ", y.Value.Select(v => $"{v.ResourceKey}: {v.Amount}"))}")
                .ToArray();

            Log($"The bundles are received successfully ({response.Data.BundleResourceBodiesDto.Count}):" +
                $"\n{string.Join("\n\t", resourcesByBundleKey)}");
        }
        else
        {
            Log($"Get bundle resources request status: {response.Status.ToString()}.");
        }

        return response;
    }

    /// <summary>
    ///     Logs the provided message.
    /// </summary>
    /// <param name="message">Message.</param>
    private void Log(string message)
    {
        Debug.Log(message);
    }
}