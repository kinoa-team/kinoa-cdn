﻿using System.Text.Json.Serialization;

/// <summary>
///     Wheel of Fortune Feature Settings data model.
/// </summary>
public class WheelOfFortuneSettings : FeatureSettingsData
{
    /// <summary>
    ///     The prize reward.
    /// </summary>
    [JsonInclude]
    [JsonPropertyName("Prize")]
    public string Prize { get; private set; }

    /// <summary>
    ///     The coins reward.
    /// </summary>
    [JsonInclude]
    [JsonPropertyName("Coins")]
    public double Coins { get; private set; }
    
    /// <summary>
    ///     The bundle key.
    ///     The name of the property that holds the bundle key should match
    ///     the name defined by the Feature Schema of type "Bundle Key".
    /// </summary>
    [JsonInclude]
    [JsonPropertyName("FooBundleKey")]
    public string FooBundleKey { get; private set; }
}