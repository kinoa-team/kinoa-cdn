﻿using System.Collections.Generic;
using System.Linq;
using Core.Data;
using Kinoa.Data.Messaging.InApp;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Core.Services
{
    /// <summary>
    ///     The sample of the UI service.
    /// </summary>
    public class KinoaUiService : KinoaSingleton<KinoaUiService>
    {
        /// <summary>
        ///     The collection of all the Game In-app messages - the UI representation of the In-app messages.
        /// </summary>
        public List<GameInAppMessage> GameInAppMessages { get; private set; } = new List<GameInAppMessage>();

        /// <summary>
        ///     The collection of the Game In-app messages <see cref="GameInAppMessages"/> awaiting to be displayed.
        /// </summary>
        private List<GameInAppMessage> GameInAppDisplayQueue { get; set; } = new List<GameInAppMessage>();

        /// <summary>
        ///     Tries to display the In-app messages.
        ///     Use case example: postponed displaying of the In-app messages on the return to the "Lobby" scene.
        ///     TODO: Trigger on the scene change to "Lobby", WebSocket In-apps, Sync Game Event In-apps, or other events.
        /// </summary>
        public void TryDisplayGameInApps()
        {
            if (!GameInAppDisplayQueue.Any())
                return;

            var scene = SceneManager.GetActiveScene().name;
            if (scene != "Lobby")
                return;

            var orderedGameInAppMessages = GameInAppDisplayQueue
                .OrderByDescending(msg => msg.InAppMessage.Order)
                .ToList();

            // Displays the In-apps in the correct order.
            foreach (var gameInAppMessage in orderedGameInAppMessages)
            {
                gameInAppMessage.Display();
            }

            // Removes the displayed In-apps from the display queue.
            GameInAppDisplayQueue.Clear();
        }

        /// <summary>
        ///     Creates the In-app message Game object.
        /// </summary>
        /// <param name="inAppMessage">In-app message data.</param>
        /// <param name="source">The source of the In-app message.</param>
        /// <param name="reason">The reason of the creation.</param>
        /// <param name="addToDisplayQueue">Flag to add to the display queue.</param>
        public void CreateGameInApp(InAppMessage inAppMessage, string source, string reason,
            bool addToDisplayQueue)
        {
            CreateGameInApps(new[] {inAppMessage}, source, reason, addToDisplayQueue);
        }

        /// <summary>
        ///     Creates the In-app messages Game objects.
        /// </summary>
        /// <param name="inAppMessages">In-app messages data.</param>
        /// <param name="source">The source of the In-app messages.</param>
        /// <param name="reason">The reason of the creation.</param>
        /// <param name="addToDisplayQueue">Flag to add to the display queue.</param>
        public void CreateGameInApps(IEnumerable<InAppMessage> inAppMessages, string source, string reason,
            bool addToDisplayQueue)
        {
            var inApps = inAppMessages?.ToArray();
            if (inApps == null || !inApps.Any()) return;

            // TODO: Create the In-app UI objects.
            var gameInAppMessages = inApps
                .Select(msg => new GameInAppMessage(msg))
                .ToList();

            GameInAppMessages.AddRange(gameInAppMessages);
            if (addToDisplayQueue)
            {
                // Adds the In-apps to the display queue based In-app type, command, lobby icon, etc. 
                GameInAppDisplayQueue.AddRange(gameInAppMessages.Where(x =>
                    x.InAppMessage.LobbyIcon?.IsInAppTrigger != true));
            }

            Log($"[{source}] In-app Game objects have been created ({inApps.Count()})." +
                $"\n{UuidsToString(inApps)}." +
                $"\nReason: {reason}.");
        }

        /// <summary>
        ///     Removes the In-app message Game object by UUID.
        /// </summary>
        /// <param name="inAppMessageUuid">In-app message UUID.</param>
        /// <param name="source">The source of the In-app message.</param>
        /// <param name="reason">The reason of the deletion.</param>
        public void RemoveGameInApp(string inAppMessageUuid, string source, string reason)
        {
            RemoveGameInApps(new[] {inAppMessageUuid}, source, reason);
        }

        /// <summary>
        ///     Removes the In-app messages Game objects by UUIDs.
        /// </summary>
        /// <param name="inAppMessageUuids">In-app message UUIDs.</param>
        /// <param name="source">The source of the In-app message.</param>
        /// <param name="reason">The reason of the deletion.</param>
        public void RemoveGameInApps(IEnumerable<string> inAppMessageUuids, string source, string reason)
        {
            var inAppUuids = inAppMessageUuids?.ToArray();
            if (inAppUuids == null || !inAppUuids.Any()) return;

            // TODO: Remove In-apps from UI.
            GameInAppMessages.RemoveAll(msg => inAppUuids.Contains(msg.InAppMessage.Uuid));
            GameInAppDisplayQueue.RemoveAll(msg => inAppUuids.Contains(msg.InAppMessage.Uuid));

            Log($"[{source}] In-app Game objects have been removed ({inAppUuids.Count()})." +
                $"\n{UuidsToString(inAppUuids)}." +
                $"\nReason: {reason}.");
        }

        /// <summary>
        ///     Replaces the In-app messages Game objects.
        /// </summary>
        /// <param name="newInAppMessages">New In-app messages data.</param>
        /// <param name="source">The source of the In-app messages.</param>
        /// <param name="reason">The reason of the replacement.</param>
        /// <param name="addToDisplayQueue">Flag to add to the display queue.</param>
        public void ReplaceGameInApps(IEnumerable<InAppMessage> newInAppMessages, string source, string reason,
            bool addToDisplayQueue)
        {
            var newInApps = newInAppMessages?.ToArray();
            if (newInApps == null || !newInApps.Any()) return;

            var replacedUuids = newInApps
                .Select(msg => (msg.Command as InAppReplacedCommand)?.ReplacedUuid ?? msg.Uuid)
                .ToArray();

            RemoveGameInApps(replacedUuids, source, reason);
            CreateGameInApps(newInApps, source, reason, addToDisplayQueue);
        }

        /// <summary>
        ///     Clears all In-app messages Game objects.
        /// </summary>
        /// <param name="source">The source of action.</param>
        /// <param name="reason">The reason of the deletion.</param>
        public void ClearGameInApps(string source, string reason)
        {
            // TODO: Remove all In-apps from UI.
            GameInAppMessages.Clear();
            GameInAppDisplayQueue.Clear();

            Log($"[{source}] All In-app Game objects have been removed." +
                $"\nReason: {reason}.");
        }

        /// <summary>
        ///     Converts the In-app messages collection to string.
        /// </summary>
        /// <param name="inAppMessages">In-app messages collection.</param>
        /// <returns>Uuids as string.</returns>
        private string UuidsToString(IEnumerable<InAppMessage> inAppMessages)
        {
            if (inAppMessages == null) return null;

            var enumerable = inAppMessages.ToArray();
            return enumerable.Any() ? string.Join(",\n", enumerable.Select(msg => msg.Uuid)) : string.Empty;
        }

        /// <summary>
        ///     Converts the Uuids collection to string.
        /// </summary>
        /// <param name="uuids">Uuids collection.</param>
        /// <returns>Uuids as string.</returns>
        private string UuidsToString(IEnumerable<string> uuids)
        {
            if (uuids == null) return null;

            var enumerable = uuids.ToArray();
            return enumerable.Any() ? string.Join(",\n", enumerable) : string.Empty;
        }

        /// <summary>
        ///     Shows In-app messages for demonstration purposes only.
        ///     Use your custom UI interface and client notification mechanism.
        /// </summary>
        private void Log(string message, List<InAppMessage> inApps = null)
        {
            Debug.Log(message);
        }
    }
}