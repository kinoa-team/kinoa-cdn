﻿using Kinoa.Data.Messaging.InApp;
using UnityEngine;

namespace Core.Data
{
    /// <summary>
    ///     The sample class for the In-app message <see cref="InAppMessage"/> UI representation.
    /// </summary>
    public class GameInAppMessage //: TODO: MonoBehaviour
    {
        /// <summary>
        ///     The In-app message data.
        /// </summary>
        public InAppMessage InAppMessage { get; private set; }

        /// <summary>
        ///     Initialize a new instance of <see cref="GameInAppMessage"/>.
        /// </summary>
        /// <param name="inAppMessage">The In-app message data.</param>
        public GameInAppMessage(InAppMessage inAppMessage)
        {
            //TODO: Create the In-app on UI, download the In-app content, set the lobby icon, etc.
            InAppMessage = inAppMessage;
        }

        /// <summary>
        ///     Displays the Game In-app message.
        /// </summary>
        public void Display()
        {
            //TODO: Implement the In-app message display logic.
        }

        /// <summary>
        ///     Closes the Game In-app message.
        /// </summary>
        public void Close()
        {
            //TODO: Implement the In-app message close logic.
        }

        /// <summary>
        ///     Clicks the Game In-app message.
        /// </summary>
        public void Click()
        {
            //TODO: Implement the In-app message click logic.
        }
    }
}