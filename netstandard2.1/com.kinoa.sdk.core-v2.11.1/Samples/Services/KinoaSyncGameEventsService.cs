﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Core.Services;
using Kinoa.Data;
using Kinoa.Data.Enum;
using Kinoa.Data.Messaging.InApp;
using Kinoa.Data.State;
using Kinoa.Data.SyncGameEvents;

/// <summary>
///     Kinoa Synchronous Game Events service.
/// </summary>
public class KinoaSyncGameEventsService<T> : KinoaGameEventBuildingService<T, KinoaSyncGameEventsService<T>>
    where T : class, IPlayerState
{
    /// <summary>
    ///     Sends Session Start Game Event.
    /// </summary>
    /// <param name="playerState">Actualized Player State.</param>
    public async Task<Response<SyncGameEventResponse>> SendSessionStartEventAsync(T playerState)
    {
        var e = OnGameSessionStart();
        var response = await Kinoa.SyncGameEvents.SendSessionStartEventAsync(e, PlayerState);
        ProcessResponse(response, processOldInApps: true);

        return response;
    }

    /// <summary>
    ///     Sends the synchronous Progression Game Event asynchronously.
    /// </summary>
    /// <returns>The response of Synchronous Game Event sending request.</returns>
    public async Task<Response<SyncGameEventResponse>> SendProgressionEventAsync()
    {
        var e = OnProgression();
        var response = await Kinoa.SyncGameEvents.SendProgressionEventAsync(e, PlayerState);
        ProcessResponse(response);

        return response;
    }

    /// <summary>
    ///     Sends the Level Up Game Event asynchronously.
    /// </summary>
    /// <param name="customParams">Custom event custom parameters.</param>
    public async Task<Response<SyncGameEventResponse>> SendLevelUpEventAsync(
        Dictionary<string, object> customParams = null)
    {
        var e = OnLevelUp(customParams);
        var response = await Kinoa.SyncGameEvents.SendLevelUpEventAsync(e, PlayerState);
        ProcessResponse(response);

        return response;
    }

    /// <summary>
    ///     Sends the Payment Game Event asynchronously.
    /// </summary>
    /// <param name="productId">Shop product identifier in market.</param>
    /// <param name="spent">Spent real money.</param>
    /// <param name="isoCurrencyCode">Real money currency in ISO 4217 format.</param>
    /// <param name="received">Received in-game resources.</param>
    public async Task<Response<SyncGameEventResponse>> SendPaymentEventAsync(
        string productId = "play-market-product-id",
        decimal spent = 0.99m,
        string isoCurrencyCode = "USD",
        Dictionary<string, decimal> received = null)
    {
        var e = OnPayment(productId, spent, isoCurrencyCode, received);
        var response = await Kinoa.SyncGameEvents.SendPaymentEventAsync(e, PlayerState);
        ProcessResponse(response);

        return response;
    }

    /// <summary>
    ///     Sends the Watch Ad Game Event asynchronously.
    /// </summary>
    /// <param name="incomeFromAd">Real money income from ad.</param>
    /// <param name="isoCurrencyCode">Real money currency in ISO 4217 format.</param>
    /// <param name="adType">Ad type.</param>
    /// <param name="received">Received in-game resources.</param>
    public async Task<Response<SyncGameEventResponse>> SendWatchAdEventAsync(
        decimal incomeFromAd = 40m,
        string isoCurrencyCode = "UAH",
        string adType = "simple_ad",
        Dictionary<string, decimal> received = null)
    {
        var e = OnWatchAd(incomeFromAd, isoCurrencyCode, adType, received);
        var response = await Kinoa.SyncGameEvents.SendWatchAdEventAsync(e, PlayerState);
        ProcessResponse(response);

        return response;
    }

    /// <summary>
    ///     Sends the In-game purchase Game Event asynchronously.
    /// </summary>
    /// <param name="spent">Spent in-game resources.</param>
    /// <param name="received">Received in-game resources.</param>
    public async Task<Response<SyncGameEventResponse>> SendInGamePurchaseEventAsync(
        Dictionary<string, decimal> spent = null,
        Dictionary<string, decimal> received = null)
    {
        var e = OnInGamePurchase(spent, received);
        var response = await Kinoa.SyncGameEvents.SendInGamePurchaseEventAsync(e, PlayerState);
        ProcessResponse(response);

        return response;
    }

    /// <summary>
    ///     Sends the Tutorial Game Event asynchronously.
    /// </summary>
    /// <param name="action">Tutorial action.</param>
    public async Task<Response<SyncGameEventResponse>> SendTutorialEventAsync(
        TutorialAction action = TutorialAction.Finish)
    {
        var e = OnTutorial(action);
        var response = await Kinoa.SyncGameEvents.SendTutorialEventAsync(e, PlayerState);
        ProcessResponse(response);

        return response;
    }

    /// <summary>
    ///     Sends the Tutorial Game Event asynchronously.
    /// </summary>
    public async Task<Response<SyncGameEventResponse>> SendCollectedResourceEventAsync()
    {
        var e = OnCollectedResource();
        var response = await Kinoa.SyncGameEvents.SendCollectedResourceEventAsync(e, PlayerState);
        ProcessResponse(response);

        return response;
    }

    /// <summary>
    ///     Sends the Social connect Game Event asynchronously.
    /// </summary>
    public async Task<Response<SyncGameEventResponse>> SendSocialConnectEventAsync()
    {
        var e = OnSocialConnect();
        var response = await Kinoa.SyncGameEvents.SendSocialConnectEventAsync(e, PlayerState);
        ProcessResponse(response);

        return response;
    }

    /// <summary>
    ///     Sends the Social disconnect Game Event asynchronously.
    /// </summary>
    public async Task<Response<SyncGameEventResponse>> SendsSocialDisconnectEventAsync()
    {
        var e = OnSocialDisconnect();
        var response = await Kinoa.SyncGameEvents.SendSocialDisconnectEventAsync(e, PlayerState);
        ProcessResponse(response);

        return response;
    }

    /// <summary>
    ///     Sends the Social post Game Event asynchronously.
    /// </summary>
    public async Task<Response<SyncGameEventResponse>> SendSocialPostEventAsync()
    {
        var e = OnSocialPost();
        var response = await Kinoa.SyncGameEvents.SendSocialPostEventAsync(e, PlayerState);
        ProcessResponse(response);

        return response;
    }

    /// <summary>
    ///     Sends the Custom Game Event asynchronously.
    /// </summary>
    /// <param name="name">Custom event name.</param>
    /// <param name="customParams">Custom event custom parameters.</param>
    public async Task<Response<SyncGameEventResponse>> SendCustomEventAsync(string name = "custom_event",
        Dictionary<string, object> customParams = null)
    {
        var e = OnCustomEvent(name, customParams);
        var response = await Kinoa.SyncGameEvents.SendCustomEventAsync(e, PlayerState);
        ProcessResponse(response);

        return response;
    }

    /// <summary>
    ///     Sends the Cheating Game Event asynchronously.
    /// </summary>
    public async Task<Response<SyncGameEventResponse>> SendCheatingEventAsync()
    {
        var e = OnCheatingEvent();
        var response = await Kinoa.SyncGameEvents.SendCustomEventAsync(e, PlayerState);
        ProcessResponse(response);

        return response;
    }

    /// <summary>
    ///     Sends the Tester Game Event asynchronously.
    /// </summary>
    public async Task<Response<SyncGameEventResponse>> SendTesterEventAsync()
    {
        var e = OnTesterEvent();
        var response = await Kinoa.SyncGameEvents.SendCustomEventAsync(e, PlayerState);
        ProcessResponse(response);

        return response;
    }

    /// <summary>
    ///     Sends the Error Game Event asynchronously.
    /// </summary>
    /// <param name="exception">Thrown exception.</param>
    public async Task<Response<SyncGameEventResponse>> SendErrorEventAsync(Exception exception)
    {
        var e = OnError(exception);
        var response = await Kinoa.SyncGameEvents.SendErrorEventAsync(e, PlayerState);
        ProcessResponse(response);

        return response;
    }

    /// <summary>
    ///     Sends the In-app close Game Event asynchronously.
    /// </summary>
    /// <param name="inAppMessage">The closed In-app message.</param>
    /// <param name="inAppMessageID">The closed In-app message identifier.</param>
    public async Task<Response<SyncGameEventResponse>> SendInAppCloseEventAsync(InAppMessage inAppMessage,
        string inAppMessageID = null)
    {
        var e = OnCloseInApp(inAppMessage, inAppMessageID);
        var response = await Kinoa.SyncGameEvents.SendInAppCloseEventAsync(e, PlayerState);
        ProcessResponse(response);

        return response;
    }

    /// <summary>
    ///     Sends the In-app click Game Event asynchronously.
    /// </summary>
    /// <param name="inAppMessage">The clicked In-app message.</param>
    /// <param name="inAppMessageID">The clicked In-app message identifier.</param>
    /// <param name="received">Received in-game resources.</param>
    public async Task<Response<SyncGameEventResponse>> SendInAppClickEventAsync(InAppMessage inAppMessage,
        string inAppMessageID = null,
        Dictionary<string, decimal> received = null)
    {
        var e = OnClickInApp(inAppMessage, inAppMessageID, received);
        var response = await Kinoa.SyncGameEvents.SendInAppClickEventAsync(e, PlayerState);
        ProcessResponse(response);

        return response;
    }

    /// <summary>
    ///     Sends the In-app impression Game Event asynchronously.
    /// </summary>
    /// <param name="inAppMessage">The impressed In-app message.</param>
    /// <param name="inAppMessageID">The impressed In-app message identifier.</param>
    public async Task<Response<SyncGameEventResponse>> SendInAppImpressionEventAsync(InAppMessage inAppMessage,
        string inAppMessageID = null)
    {
        var e = OnInAppImpression(inAppMessage, inAppMessageID);
        var response = await Kinoa.SyncGameEvents.SendInAppImpressionEventAsync(e, PlayerState);
        ProcessResponse(response);

        return response;
    }

    /// <summary>
    ///     Process the response of Synchronous Game Event sending request.
    /// </summary>
    /// <param name="response">The response.</param>
    /// <param name="processOldInApps">Flag to process the old In-apps.
    /// Use case example: processing of old In-apps on the first Sync Game Event response - sync session start.
    /// </param>
    public void ProcessResponse(Response<SyncGameEventResponse> response, bool processOldInApps = false)
    {
        if (!response.IsSuccessful() || response.Data?.InboxDetails == null)
        {
            return;
        }

        ProcessNonInboxInApps(response.Data);
        if (processOldInApps)
        {
            ProcessNewInApps(response.Data);
            //Old In-apps contains reminders, progression score and milestones In-apps.
            ProcessOldInApps(response.Data);
        }
        else
        {
            ProcessRemovedInApps(response.Data);
            ProcessReplacedInApps(response.Data);
            ProcessNewInApps(response.Data);

            ProcessReminderInApps(response.Data);
            ProcessProgressionScoreInApps(response.Data);
            ProcessMilestonesProgressInApps(response.Data);
        }

        KinoaUiService.Instance.TryDisplayGameInApps();
    }

    /// <summary>
    ///     Process the Non-Inbox In-apps from Synchronous Game Event sending response.
    /// </summary>
    /// <param name="response">The response.</param>
    private void ProcessNonInboxInApps(SyncGameEventResponse response)
    {
        var nonInboxInApps = response.OptionalMessages
            .Select(msg => msg)
            .ToArray();

        if (nonInboxInApps.Any())
        {
            KinoaUiService.Instance.CreateGameInApps(nonInboxInApps,
                nameof(SyncGameEventResponse), nameof(nonInboxInApps), addToDisplayQueue: true);
        }
    }

    /// <summary>
    ///     Process the New Inbox In-apps from Synchronous Game Event sending response.
    /// </summary>
    /// <param name="response">The response.</param>
    private void ProcessNewInApps(SyncGameEventResponse response)
    {
        var newInApps = response.InboxMessages
            .Where(msg => response.InboxDetails.NewInApps.Contains(msg.Uuid))
            .Select(msg => msg)
            .ToArray();

        if (newInApps.Any())
        {
            KinoaUiService.Instance.CreateGameInApps(newInApps,
                nameof(SyncGameEventResponse), nameof(newInApps), addToDisplayQueue: true);
        }
    }

    /// <summary>
    ///     Process the Old Inbox In-apps from Synchronous Game Event sending response.
    /// </summary>
    /// <param name="response">The response.</param>
    private void ProcessOldInApps(SyncGameEventResponse response)
    {
        var oldInApps = response.InboxMessages
            .Where(msg => response.InboxDetails.OldInApps.Contains(msg.Uuid))
            .Select(msg => msg)
            .ToArray();

        if (oldInApps.Any())
        {
            KinoaUiService.Instance.CreateGameInApps(oldInApps,
                nameof(SyncGameEventResponse), nameof(oldInApps), addToDisplayQueue: true);
        }
    }

    /// <summary>
    ///     Process the Inbox Removed In-apps from Synchronous Game Event sending response.
    /// </summary>
    /// <param name="response">The response.</param>
    private void ProcessRemovedInApps(SyncGameEventResponse response)
    {
        var removedInApps = response.InboxDetails.RemovedInApps;
        if (removedInApps.Any())
        {
            KinoaUiService.Instance.RemoveGameInApps(removedInApps,
                nameof(SyncGameEventResponse), nameof(removedInApps));
        }
    }

    /// <summary>
    ///     Process the Inbox Replaced In-apps from Synchronous Game Event sending response.
    /// </summary>
    /// <param name="response">The response.</param>
    private void ProcessReplacedInApps(SyncGameEventResponse response)
    {
        var removedInApps = response.InboxDetails.ReplacedInApps;
        if (removedInApps.Any())
        {
            KinoaUiService.Instance.RemoveGameInApps(removedInApps,
                nameof(SyncGameEventResponse), nameof(removedInApps));
        }
    }

    /// <summary>
    ///     Process the Inbox Reminder In-apps from Synchronous Game Event sending response.
    /// </summary>
    /// <param name="response">The response.</param>
    private void ProcessReminderInApps(SyncGameEventResponse response)
    {
        var reminderInApps = response.InboxMessages
            .Where(msg => response.InboxDetails.ReminderInApps.Contains(msg.Uuid))
            .Select(msg => msg)
            .ToArray();

        if (reminderInApps.Any())
        {
            KinoaUiService.Instance.ReplaceGameInApps(reminderInApps,
                nameof(SyncGameEventResponse), nameof(reminderInApps), addToDisplayQueue: true);
        }
    }

    /// <summary>
    ///     Process the Inbox Progression Score In-apps from Synchronous Game Event sending response.
    /// </summary>
    /// <param name="response">The response.</param>
    private void ProcessProgressionScoreInApps(SyncGameEventResponse response)
    {
        var progressionInApps = response.InboxMessages
            .Where(msg => response.InboxDetails.ProgressionScoreInApps.Contains(msg.Uuid))
            .Select(msg => msg)
            .ToArray();

        if (progressionInApps.Any())
        {
            KinoaUiService.Instance.ReplaceGameInApps(progressionInApps,
                nameof(SyncGameEventResponse), nameof(progressionInApps), addToDisplayQueue: false);
        }
    }

    /// <summary>
    ///     Process the Inbox Milestones Progress In-apps from Synchronous Game Event sending response.
    /// </summary>
    /// <param name="response">The response.</param>
    private void ProcessMilestonesProgressInApps(SyncGameEventResponse response)
    {
        var milestonesProgressInApps = response.InboxMessages
            .Where(msg => response.InboxDetails.MilestonesProgressInApps.Contains(msg.Uuid))
            .Select(msg => msg)
            .ToArray();

        if (milestonesProgressInApps.Any())
        {
            KinoaUiService.Instance.ReplaceGameInApps(milestonesProgressInApps,
                nameof(SyncGameEventResponse), nameof(milestonesProgressInApps), addToDisplayQueue: false);
        }
    }
}