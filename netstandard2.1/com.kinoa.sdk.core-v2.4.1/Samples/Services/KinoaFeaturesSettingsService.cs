﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading;
using System.Threading.Tasks;
using Kinoa.Core.Callbacks;
using Kinoa.Data;
using Kinoa.Data.Enum;
using Kinoa.Data.FeaturesSettings;
using Kinoa.Data.FeaturesSettings.Enum;
using UnityEngine;

/// <summary>
///     Kinoa Features Settings service.
/// </summary>
public class KinoaFeaturesSettingsService
{
    /// <summary>
    ///     Gets the server checksums and downloads the Features Settings if the local checksums are different,
    ///     otherwise gets the cached or built-in Features Settings. Saves the downloaded Features Settings to cache.
    ///     <remarks>
    ///     The response contains the original server response information
    ///     and the Features Settings collection loaded from the server, cache, built-in, or combination of them.
    ///     </remarks>
    /// </summary>
    /// <param name="settingsRequestParams">The Features Settings request parameters.</param>
    /// <param name="cancellationToken">Cancellation token.</param>
    /// <param name="downloadProgressChangedCallback">Callback function that is invoked on download progress change.</param>
    /// <typeparam name="TData">
    ///     Represents the Feature Settings polymorphic data model type,
    ///     deserialized based on the "$type" TypeDiscriminator property,
    ///     where "$type" is the requested Feature Settings key <see cref="FeatureSettingsSmartDownloadRequestParams.Key"/>.
    ///<remarks>
    ///     Apply the <see cref="JsonPolymorphicAttribute"/> for the provided <see cref="TData"/> type
    ///     to enable the polymorphic deserialization.
    /// </remarks>
    /// </typeparam>
    /// <returns>The requested Features Settings type of <see cref="TData"/>.</returns>
    public async Task<Response<FeaturesSettingsResponse<FeatureSettingsData>>> SmartDownloadAsync<TData>(
        List<FeatureSettingsSmartDownloadRequestParams> settingsRequestParams,
        CancellationToken cancellationToken = default, ProgressChangedCallback downloadProgressChangedCallback = null)
    {
        if (settingsRequestParams == null)
        {
            settingsRequestParams = new List<FeatureSettingsSmartDownloadRequestParams>
            {
                new FeatureSettingsSmartDownloadRequestParams(key: "DailyBonus", version: 1, getDefault: false,
                    FeatureSettingsFailedDownloadStrategy.GetCachedOrBuiltIn),
                new FeatureSettingsSmartDownloadRequestParams(key: "WheelOfFortune", version: 1, getDefault: false,
                    FeatureSettingsFailedDownloadStrategy.GetCachedOrBuiltIn)
            };
        }

        if (cancellationToken == default)
        {
            var cancellationTokenSource = new CancellationTokenSource();
            cancellationTokenSource.CancelAfter(10 * 1000);
            cancellationToken = cancellationTokenSource.Token;
        }

        var response = await Kinoa.FeaturesSettings.SmartDownloadAsync<FeatureSettingsData>(
            settingsRequestParams, cancellationToken, downloadProgressChangedCallback ?? OnDownloadProgressChanged);
        LogSettingsData(response.Data?.Settings);

        return response;
    }

    /// <summary>
    ///     Downloads Features Settings by the provided request parameters asynchronously.
    /// </summary>
    /// <param name="settingsRequestParams">Features Settings request parameters.</param>
    /// <param name="cancellationToken">Cancellation token.</param>
    /// <param name="downloadProgressChangedCallback">Callback function that is invoked on download progress change.</param>
    /// <typeparam name="TData">
    ///     Represents the Feature Settings polymorphic data model type,
    ///     deserialized based on the "$type" TypeDiscriminator property,
    ///     where "$type" is the requested Feature Settings key <see cref="FeatureSettingsRequestParams.Key"/>.
    ///<remarks>
    ///     Apply the <see cref="JsonPolymorphicAttribute"/> for the provided <see cref="TData"/> type
    ///     to enable the polymorphic deserialization.
    /// </remarks>
    /// <remarks>
    ///     In this example, <see cref="FeatureSettingsData"/> is the base polymorphic type for all Feature Settings data models.
    ///     You can change the type, by providing the <see cref="TData"/> instead of hardcoded <see cref="FeatureSettingsData"/>.
    /// </remarks>
    /// </typeparam>
    /// <returns>The requested Features Settings type of <see cref="TData"/>.</returns>
    public async Task<Response<FeaturesSettingsResponse<FeatureSettingsData>>> DownloadAsync<TData>(
        List<FeatureSettingsDownloadRequestParams> settingsRequestParams,
        CancellationToken cancellationToken = default, ProgressChangedCallback downloadProgressChangedCallback = null)
    {
        if (settingsRequestParams == null)
        {
            settingsRequestParams = new List<FeatureSettingsDownloadRequestParams>
            {
                new FeatureSettingsDownloadRequestParams(key: "DailyBonus", version: 1, getDefault: false),
                new FeatureSettingsDownloadRequestParams(key: "WheelOfFortune", version: 1, getDefault: false)
            };
        }

        if (cancellationToken == default)
        {
            var cancellationTokenSource = new CancellationTokenSource();
            cancellationTokenSource.CancelAfter(10 * 1000);
            cancellationToken = cancellationTokenSource.Token;
        }

        var response = await Kinoa.FeaturesSettings.DownloadAsync<FeatureSettingsData>(
            settingsRequestParams, cancellationToken, downloadProgressChangedCallback ?? OnDownloadProgressChanged);
        if (response.IsSuccessful())
        {
            LogSettingsData(response.Data.Settings);
        }

        return response;
    }

    /// <summary>
    ///     On Features Settings download progress changed.
    /// </summary>
    /// <param name="progress">Content download progress.</param>
    private static void OnDownloadProgressChanged(decimal progress)
    {
        Debug.Log($"Features Settings download progress changed: {progress}");
    }

    /// <summary>
    ///     Gets the Features Settings Checksums by the provided request parameters asynchronously.
    /// </summary>
    /// <param name="settingsRequestParams">Features Settings request parameters.</param>
    /// <returns>The response to the requested Features Settings containing all information except the Data.</returns>
    public async Task<Response<FeaturesSettingsResponse>> GetChecksumsAsync(
        List<FeatureSettingsDownloadRequestParams> settingsRequestParams)
    {
        if (settingsRequestParams == null)
        {
            settingsRequestParams = new List<FeatureSettingsDownloadRequestParams>
            {
                new FeatureSettingsDownloadRequestParams(key: "DailyBonus", version: 1, getDefault: false),
                new FeatureSettingsDownloadRequestParams(key: "WheelOfFortune", version: 1, getDefault: false)
            };
        }

        var response = await Kinoa.FeaturesSettings.GetChecksumsAsync(settingsRequestParams);
        if (response.IsSuccessful())
        {
            LogSettings(response.Data.Settings);
        }

        return response;
    }

    /// <summary>
    ///     Gets Built-in Features Settings by the provided request parameters asynchronously.
    /// </summary>
    /// <param name="settingsRequestParams">Features Settings request parameters.</param>
    /// <typeparam name="TData">
    ///     Represents the Feature Settings polymorphic data model type,
    ///     deserialized based on the "$type" TypeDiscriminator property,
    ///     where "$type" is the requested Feature Settings key <see cref="FeatureSettingsRequestParams.Key"/>.
    ///<remarks>
    ///     Apply the <see cref="JsonPolymorphicAttribute"/> for the provided <see cref="TData"/> type
    ///     to enable the polymorphic deserialization.
    /// </remarks>
    /// </typeparam>
    /// <returns>The requested Features Settings type of <see cref="TData"/>.</returns>
    public async Task<Response<FeaturesSettingsResponse<FeatureSettingsData>>> GetBuiltInAsync<TData>(
        List<FeatureSettingsRequestParams> settingsRequestParams)
    {
        if (settingsRequestParams == null)
        {
            settingsRequestParams = new List<FeatureSettingsRequestParams>
            {
                new FeatureSettingsRequestParams(key: "DailyBonus", version: 1),
                new FeatureSettingsRequestParams(key: "WheelOfFortune", version: 1)
            };
        }

        var response = await Kinoa.FeaturesSettings.GetBuiltInAsync<FeatureSettingsData>(settingsRequestParams);
        if (response.IsSuccessful())
        {
            LogSettingsData(response.Data.Settings);
        }

        return response;
    }

    /// <summary>
    ///     Gets Cached Features Settings by the provided request parameters asynchronously.
    /// </summary>
    /// <param name="settingsRequestParams">Features Settings request parameters.</param>
    /// <typeparam name="TData">
    ///     Represents the Feature Settings polymorphic data model type,
    ///     deserialized based on the "$type" TypeDiscriminator property,
    ///     where "$type" is the requested Feature Settings key <see cref="FeatureSettingsDownloadRequestParams.Key"/>.
    ///<remarks>
    ///     Apply the <see cref="JsonPolymorphicAttribute"/> for the provided <see cref="TData"/> type
    ///     to enable the polymorphic deserialization.
    /// </remarks>
    /// </typeparam>
    /// <returns>The requested Features Settings type of <see cref="TData"/>.</returns>
    public async Task<Response<FeaturesSettingsResponse<FeatureSettingsData>>> GetCachedAsync<TData>(
        List<FeatureSettingsRequestParams> settingsRequestParams)
    {
        if (settingsRequestParams == null)
        {
            settingsRequestParams = new List<FeatureSettingsRequestParams>
            {
                new FeatureSettingsRequestParams(key: "DailyBonus", version: 1),
                new FeatureSettingsRequestParams(key: "WheelOfFortune", version: 1)
            };
        }

        var response = await Kinoa.FeaturesSettings.GetCachedAsync<FeatureSettingsData>(settingsRequestParams);
        if (response.IsSuccessful())
        {
            LogSettingsData(response.Data.Settings);
        }

        return response;
    }

    /// <summary>
    ///     Gets information about the available Features Settings by the requested Schema key.
    /// </summary>
    /// <param name="schemaKey">The Schema key/name.</param>
    /// <returns>The available Features Settings information.</returns>
    public async Task<Response<SchemaFeaturesSettingsInfo>> GetSchemaFeaturesSettingsInfoAsync(string schemaKey)
    {
        var response = await Kinoa.FeaturesSettings.GetInfoAsync(schemaKey);
        var log = new StringBuilder();
        if (response.IsSuccessful() && response.Data != null)
        {
            log.AppendFormat($"Feature Schema ID:{response.Data.Id}, Name:{response.Data.Name}.");
            if (response.Data.Settings.Any())
            {
                log.AppendFormat("\nAvailable Features Settings:");
                foreach (var featureSettings in response.Data.Settings)
                {
                    var schemaVersions = featureSettings.Versions.Any()
                        ? string.Join(", ", featureSettings.Versions)
                        : string.Empty;

                    log.AppendFormat($"\nId: {featureSettings.Id}. " +
                                     $"Key: {featureSettings.Key}. " +
                                     $"Name: {featureSettings.Name}. " +
                                     $"Versions: {schemaVersions}.");
                }
            }
            else log.AppendFormat("\nNo Features Settings available.");
        }
        else
        {
            log.AppendFormat($"{nameof(Kinoa.FeaturesSettings.GetInfoAsync)} is failed:" +
                             $"\nStatus: {response.Status.ToString()}" +
                             $"\nMessage: {response.Error?.Message}" +
                             $"\nCode: {response.Error?.Code}");
        }

        Log(log);
        return response;
    }

    /// <summary>
    ///     Logs the requested Features Settings information except the Data.
    /// </summary>
    /// <param name="settings">The Feature Settings collection.</param>
    private void LogSettings(IEnumerable<FeatureSettingsResponse> settings)
    {
        foreach (var setting in settings)
        {
            var briefLog = new StringBuilder();
            var scheduledStartTime = setting.StartTime > 0
                ? DateTimeOffset.FromUnixTimeMilliseconds(setting.StartTime.Value).DateTime
                    .ToString("yyyy-MM-dd HH:mm:ss")
                : string.Empty;
            var scheduledEndTime = setting.EndTime > 0
                ? DateTimeOffset.FromUnixTimeMilliseconds(setting.EndTime.Value).DateTime
                    .ToString("yyyy-MM-dd HH:mm:ss")
                : string.Empty;
            var audiences = string.Join(", ", setting.Audiences
                .Where(x => x.Value == true)
                .Select(x => x.Key));
            var userLists = string.Join(", ", setting.UserLists
                .Where(x => x.Value == true)
                .Select(x => x.Key));

            briefLog.AppendFormat("The requested Features Settings were received:");
            briefLog.AppendFormat($"\nStatus: {setting.Status.ToString()}. " +
                                  $"Source: {setting.Source.ToString()}. " +
                                  $"Key: {setting.Request.Key}. " +
                                  $"Version: {setting.Request.Version}. " +
                                  $"Configuration: {setting.ConfigurationName}. " +
                                  $"Checksum: {setting.Checksum}. " +
                                  $"Start Time: {scheduledStartTime}. " +
                                  $"End Time: {scheduledEndTime}. " +
                                  $"Audiences: {audiences}. " +
                                  $"User Lists: {userLists}. ");
            Log(briefLog);
        }
    }

    /// <summary>
    ///     Logs the requested Features Settings information including the Data.
    /// </summary>
    /// <param name="settings">The Feature Settings collection.</param>
    private void LogSettingsData(IEnumerable<FeatureSettingsResponse<FeatureSettingsData>> settings)
    {
        var settingsList = settings?.ToList();
        if (settingsList == null || !settingsList.Any())
        {
            return;
        }

        LogSettings(settingsList);

        var dailyBonusSettings =
            settingsList.FirstOrDefault(x => x.Request.Key == "DailyBonus");
        if (dailyBonusSettings != null && dailyBonusSettings.Status == FeatureSettingsResponseStatus.Ok &&
            dailyBonusSettings.Data.Any())
        {
            var log = new StringBuilder();
            log.AppendFormat("\tDaily Bonus Settings:");
            foreach (var filter in dailyBonusSettings.Filters)
            {
                log.AppendFormat($"\nFilter: {filter.Key} - {filter.Value}");
            }

            foreach (var data in dailyBonusSettings.Data)
            {
                log.AppendFormat($"\nCoins: {((DailyBonusSettings) data).Coins}");
            }

            Log(log);
        }

        var wheelOfFortuneSettings =
            settingsList.FirstOrDefault(x => x.Request.Key == "WheelOfFortune");
        if (wheelOfFortuneSettings != null && wheelOfFortuneSettings.Status == FeatureSettingsResponseStatus.Ok &&
            wheelOfFortuneSettings.Data.Any())
        {
            var log = new StringBuilder();
            log.AppendFormat("\tWheel of Fortune Settings:");
            foreach (var filter in wheelOfFortuneSettings.Filters)
            {
                log.AppendFormat($"\nFilter: {filter.Key} - {filter.Value}");
            }

            foreach (var data in wheelOfFortuneSettings.Data)
            {
                log.AppendFormat($"\nPrize: {((WheelOfFortuneSettings) data).Prize}");
                log.AppendFormat($"\nCoins: {((WheelOfFortuneSettings) data).Coins}");
            }

            Log(log);
        }
    }

    /// <summary>
    ///     Logs the provided message.
    /// </summary>
    /// <param name="builder">String builder.</param>
    private void Log(StringBuilder builder)
    {
        var log = builder.ToString();
        Debug.Log(log);
        builder.Clear();
    }
}