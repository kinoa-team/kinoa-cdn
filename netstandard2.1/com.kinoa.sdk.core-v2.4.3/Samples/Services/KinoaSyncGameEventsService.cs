﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Kinoa.Data;
using Kinoa.Data.Enum;
using Kinoa.Data.Messaging.InApp;
using Kinoa.Data.State;
using Kinoa.Data.SyncGameEvents;
using UnityEngine;

/// <summary>
///     Kinoa Synchronous Game Events service.
/// </summary>
public class KinoaSyncGameEventsService<TPlayerState> :
    KinoaGameEventBuildingService<KinoaSyncGameEventsService<TPlayerState>, TPlayerState>
    where TPlayerState : PlayerState
{
    /// <summary>
    ///     Sends the synchronous Progression Game Event asynchronously.
    /// </summary>
    /// <returns>The response of Synchronous Game Event sending request.</returns>
    public async Task<Response<SyncGameEventResponse>> SendProgressionEventAsync()
    {
        var e = OnProgression();
        var response = await Kinoa.SyncGameEvents.SendProgressionEventAsync(e, PlayerState);
        ProcessResponse(response);

        return response;
    }

    /// <summary>
    ///     Sends the Level Up Game Event asynchronously.
    /// </summary>
    /// <param name="customParams">Custom event custom parameters.</param>
    public async Task<Response<SyncGameEventResponse>> SendLevelUpEventAsync(
        Dictionary<string, object> customParams = null)
    {
        var e = OnLevelUp(customParams);
        var response = await Kinoa.SyncGameEvents.SendLevelUpEventAsync(e, PlayerState);
        ProcessResponse(response);

        return response;
    }

    /// <summary>
    ///     Sends the Payment Game Event asynchronously.
    /// </summary>
    /// <param name="productId">Shop product identifier in market.</param>
    /// <param name="spent">Spent real money.</param>
    /// <param name="isoCurrencyCode">Real money currency in ISO 4217 format.</param>
    /// <param name="received">Received in-game resources.</param>
    public async Task<Response<SyncGameEventResponse>> SendPaymentEventAsync(
        string productId = "play-market-product-id",
        decimal spent = 0.99m,
        string isoCurrencyCode = "USD",
        Dictionary<string, decimal> received = null)
    {
        var e = OnPayment(productId, spent, isoCurrencyCode, received);
        var response = await Kinoa.SyncGameEvents.SendPaymentEventAsync(e, PlayerState);
        ProcessResponse(response);

        return response;
    }

    /// <summary>
    ///     Sends the Watch Ad Game Event asynchronously.
    /// </summary>
    /// <param name="incomeFromAd">Real money income from ad.</param>
    /// <param name="isoCurrencyCode">Real money currency in ISO 4217 format.</param>
    /// <param name="adType">Ad type.</param>
    /// <param name="received">Received in-game resources.</param>
    public async Task<Response<SyncGameEventResponse>> SendWatchAdEventAsync(
        decimal incomeFromAd = 40m,
        string isoCurrencyCode = "UAH",
        string adType = "simple_ad",
        Dictionary<string, decimal> received = null)
    {
        var e = OnWatchAd(incomeFromAd, isoCurrencyCode, adType, received);
        var response = await Kinoa.SyncGameEvents.SendWatchAdEventAsync(e, PlayerState);
        ProcessResponse(response);

        return response;
    }

    /// <summary>
    ///     Sends the In-game purchase Game Event asynchronously.
    /// </summary>
    /// <param name="spent">Spent in-game resources.</param>
    /// <param name="received">Received in-game resources.</param>
    public async Task<Response<SyncGameEventResponse>> SendInGamePurchaseEventAsync(
        Dictionary<string, decimal> spent = null,
        Dictionary<string, decimal> received = null)
    {
        var e = OnInGamePurchase(spent, received);
        var response = await Kinoa.SyncGameEvents.SendInGamePurchaseEventAsync(e, PlayerState);
        ProcessResponse(response);

        return response;
    }

    /// <summary>
    ///     Sends the Tutorial Game Event asynchronously.
    /// </summary>
    /// <param name="action">Tutorial action.</param>
    public async Task<Response<SyncGameEventResponse>> SendTutorialEventAsync(
        TutorialAction action = TutorialAction.Finish)
    {
        var e = OnTutorial(action);
        var response = await Kinoa.SyncGameEvents.SendTutorialEventAsync(e, PlayerState);
        ProcessResponse(response);

        return response;
    }

    /// <summary>
    ///     Sends the Social connect Game Event asynchronously.
    /// </summary>
    public async Task<Response<SyncGameEventResponse>> SendSocialConnectEventAsync()
    {
        var e = OnSocialConnect();
        var response = await Kinoa.SyncGameEvents.SendSocialConnectEventAsync(e, PlayerState);
        ProcessResponse(response);

        return response;
    }

    /// <summary>
    ///     Sends the Social disconnect Game Event asynchronously.
    /// </summary>
    public async Task<Response<SyncGameEventResponse>> SendsSocialDisconnectEventAsync()
    {
        var e = OnSocialDisconnect();
        var response = await Kinoa.SyncGameEvents.SendSocialDisconnectEventAsync(e, PlayerState);
        ProcessResponse(response);

        return response;
    }

    /// <summary>
    ///     Sends the Social post Game Event asynchronously.
    /// </summary>
    public async Task<Response<SyncGameEventResponse>> SendSocialPostEventAsync()
    {
        var e = OnSocialPost();
        var response = await Kinoa.SyncGameEvents.SendSocialPostEventAsync(e, PlayerState);
        ProcessResponse(response);

        return response;
    }

    /// <summary>
    ///     Sends the Custom Game Event asynchronously.
    /// </summary>
    /// <param name="name">Custom event name.</param>
    /// <param name="customParams">Custom event custom parameters.</param>
    public async Task<Response<SyncGameEventResponse>> SendCustomEventAsync(string name = "custom_event",
        Dictionary<string, object> customParams = null)
    {
        var e = OnCustomEvent(name, customParams);
        var response = await Kinoa.SyncGameEvents.SendCustomEventAsync(e, PlayerState);
        ProcessResponse(response);

        return response;
    }

    /// <summary>
    ///     Sends the Cheating Game Event asynchronously.
    /// </summary>
    public async Task<Response<SyncGameEventResponse>> SendCheatingEventAsync()
    {
        var e = OnCheatingEvent();
        var response = await Kinoa.SyncGameEvents.SendCustomEventAsync(e, PlayerState);
        ProcessResponse(response);

        return response;
    }

    /// <summary>
    ///     Sends the Tester Game Event asynchronously.
    /// </summary>
    public async Task<Response<SyncGameEventResponse>> SendTesterEventAsync()
    {
        var e = OnTesterEvent();
        var response = await Kinoa.SyncGameEvents.SendCustomEventAsync(e, PlayerState);
        ProcessResponse(response);

        return response;
    }

    /// <summary>
    ///     Sends the Error Game Event asynchronously.
    /// </summary>
    /// <param name="exception">Thrown exception.</param>
    public async Task<Response<SyncGameEventResponse>> SendErrorEventAsync(Exception exception)
    {
        var e = OnError(exception);
        var response = await Kinoa.SyncGameEvents.SendErrorEventAsync(e, PlayerState);
        ProcessResponse(response);

        return response;
    }

    /// <summary>
    ///     Sends the In-app close Game Event asynchronously.
    /// </summary>
    /// <param name="inAppMessage">The closed In-app message.</param>
    /// <param name="inAppMessageID">The closed In-app message identifier.</param>
    public async Task<Response<SyncGameEventResponse>> SendInAppCloseEventAsync(InAppMessage inAppMessage,
        string inAppMessageID = null)
    {
        var e = OnCloseInApp(inAppMessage, inAppMessageID);
        var response = await Kinoa.SyncGameEvents.SendInAppCloseEventAsync(e, PlayerState);
        ProcessResponse(response);

        return response;
    }

    /// <summary>
    ///     Sends the In-app click Game Event asynchronously.
    /// </summary>
    /// <param name="inAppMessage">The clicked In-app message.</param>
    /// <param name="inAppMessageID">The clicked In-app message identifier.</param>
    /// <param name="received">Received in-game resources.</param>
    public async Task<Response<SyncGameEventResponse>> SendInAppClickEventAsync(InAppMessage inAppMessage,
        string inAppMessageID = null,
        Dictionary<string, decimal> received = null)
    {
        var e = OnClickInApp(inAppMessage, inAppMessageID, received);
        var response = await Kinoa.SyncGameEvents.SendInAppClickEventAsync(e, PlayerState);
        ProcessResponse(response);

        return response;
    }

    /// <summary>
    ///     Sends the In-app impression Game Event asynchronously.
    /// </summary>
    /// <param name="inAppMessage">The impressed In-app message.</param>
    /// <param name="inAppMessageID">The impressed In-app message identifier.</param>
    public async Task<Response<SyncGameEventResponse>> SendInAppImpressionEventAsync(InAppMessage inAppMessage,
        string inAppMessageID = null)
    {
        var e = OnInAppImpression(inAppMessage, inAppMessageID);
        var response = await Kinoa.SyncGameEvents.SendInAppImpressionEventAsync(e, PlayerState);
        ProcessResponse(response);

        return response;
    }

    /// <summary>
    ///     Process the response of Synchronous Game Event sending request.
    /// </summary>
    /// <param name="response">The response.</param>
    private void ProcessResponse(Response<SyncGameEventResponse> response)
    {
        if (!response.IsSuccessful() || response.Data == null)
        {
            return;
        }

        if (response.Data.InboxDetails != null)
        {
            var reminderInApps = response.Data.InboxMessages
                .Where(msg => response.Data.InboxDetails.ReminderInApps.Contains(msg.Uuid))
                .Select(msg => msg.Uuid)
                .ToArray();

            if (reminderInApps.Any())
                Log($"In-apps Reminders: \n{UuidsToString(reminderInApps)}");

            var progressionInApps = response.Data.InboxMessages
                .Where(msg => response.Data.InboxDetails.ProgressionScoreInApps.Contains(msg.Uuid))
                .Select(msg => msg.Uuid)
                .ToArray();

            if (progressionInApps.Any())
                Log($"Progression In-apps: \n{UuidsToString(progressionInApps)}");

            var newInApps = response.Data.InboxMessages
                .Where(msg => response.Data.InboxDetails.NewInApps.Contains(msg.Uuid))
                .Select(msg => msg.Uuid)
                .ToArray();
            var oldInApps = response.Data.InboxMessages
                .Where(msg => response.Data.InboxDetails.OldInApps.Contains(msg.Uuid))
                .Select(msg => msg.Uuid)
                .ToArray();

            var replacedInApps = response.Data.InboxDetails.ReplacedInApps
                .Select(uuid => uuid)
                .ToArray();
            var replacedByInApps = response.Data.InboxMessages
                .Where(msg => newInApps.Contains(msg.Uuid) && msg.Command?.GetType() == typeof(InAppReplacedCommand))
                .Select(msg => msg.Uuid)
                .ToArray();

            if (newInApps.Any() || oldInApps.Any() || replacedInApps.Any() || replacedByInApps.Any())
                Log($"New In-apps: \n{UuidsToString(newInApps)}" +
                    $"\nOld In-apps: \n{UuidsToString(oldInApps)}" +
                    $"\nReplaced In-apps: \n{UuidsToString(replacedInApps)}" +
                    $"\nReplaced By In-apps: \n{UuidsToString(replacedByInApps)}");
            
            var removedInApps = response.Data.InboxDetails.RemovedInApps
                .Select(uuid => uuid)
                .ToArray();
            
            if (removedInApps.Any())
                Log($"Removed In-apps: \n{UuidsToString(removedInApps)}");
        }

        var inboxInApps = response.Data.InboxMessages
            .Select(msg => msg.Uuid)
            .ToArray();
        var optionalInApps = response.Data.OptionalMessages
            .Select(msg => msg.Uuid)
            .ToArray();

        if (inboxInApps.Any() || optionalInApps.Any())
            Log($"Inbox Messages: \n{UuidsToString(inboxInApps)}" +
                $"\nOptional Messages: \n{UuidsToString(optionalInApps)}");
    }

    /// <summary>
    ///     Converts the Uuids collection to string.
    /// </summary>
    /// <param name="uuids">Uuids collection.</param>
    /// <returns>Uuids as string.</returns>
    private string UuidsToString(string[] uuids)
    {
        return uuids.Any() ? string.Join(",\n", uuids) : string.Empty;
    }

    /// <summary>
    ///     Logs the message to console and shows the notification.
    /// </summary>
    /// <param name="log">The log message.</param>
    private void Log(string log)
    {
        Debug.Log(log);
    }
}