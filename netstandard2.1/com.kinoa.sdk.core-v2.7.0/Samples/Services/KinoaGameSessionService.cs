﻿using System.Threading.Tasks;
using Kinoa.Core.Network.Retry;
using Kinoa.Data;
using Kinoa.Data.Network;
using Kinoa.Data.WebModels;
using UnityEngine;

/// <summary>
///     The sample Game Session Service used to manage the Game Session.
/// </summary>
public class KinoaGameSessionService : KinoaSingleton<KinoaGameSessionService>
{
    /// <summary>
    ///     The custom network configuration of the Session Open request.
    /// </summary>
    private readonly NetworkConfiguration openSessionNetworkConfig = new NetworkConfiguration(networkTimeout: 10,
        new RetryConfiguration(RetryReason.ConnectionError, RetryStrategy.Linear, maxRetryAttempts: 3));

    /// <summary>
    ///     Opens and registers a new Game Session on the Kinoa side asynchronously.
    ///     <remarks>The method doesn't sends the Session Start Game Event.
    ///     <see cref="Kinoa.GameEvents.SendSessionStartEvent{TPlayerState}"/> (Async API) ~or~
    ///     <see cref="Kinoa.SyncGameEvents.SendSessionStartEventAsync{TPlayerState}"/> (Sync API)
    ///     to trigger the Session Start Game Event.</remarks>
    /// </summary>
    /// <param name="playerState">The Player State.</param>
    /// <param name="gameSessionData">The Game Session Data.</param>
    /// <param name="networkConfiguration">The network configuration of the Session Open request.</param>
    /// <returns>The response of the Game Session Start request with the remote Player State.</returns>
    public async Task<Response<WebPlayerState<CustomPlayerState>>> OpenSessionAsync(CustomPlayerState playerState,
        GameSessionData gameSessionData, NetworkConfiguration networkConfiguration = null)
    {
        var gameSession = gameSessionData ?? new GameSessionData();
        var networkConfig = networkConfiguration ?? openSessionNetworkConfig;

        var response = await Kinoa.GameSession.OpenSessionAsync(playerState, gameSession, networkConfig);
        if (response.IsSuccessful())
        {
            Log("The new Game Session is successfully opened.");
            if (response.Data != null)
            {
                Debug.Log("The Server Player State is successfully received: " +
                          $"Player ID: \"{response.Data.PlayerState.PlayerIdentifiers.PlayerId}\".");
            }
        }
        else
        {
            Log($"The new Game Session opening failed: {response.Error?.Code}. " +
                $"\nResponse status: {response.Status.ToString()}");
        }

        return response;
    }

    /// <summary>
    ///     The sample log method shows the messages in the console and the dialog.
    /// </summary>
    /// <param name="message">The message.</param>
    private void Log(string message)
    {
        Debug.Log(message);
    }
}