﻿using System;
using System.Threading.Tasks;
using Kinoa.Data;
using Kinoa.Data.State;
using UnityEngine;

/// <summary>
///     The sample Player Service used to manage the Player data.
/// </summary>
public class KinoaPlayerStateService : KinoaSingleton<KinoaPlayerStateService>
{
    /// <summary>
    ///     The Local Player State <see cref="PlayerState"/> sample set event.
    /// </summary>
    public event Action<bool> OnLocalPlayerStateSet;

    /// <summary>
    ///     The actual Player State.
    /// </summary>
    public CustomPlayerState PlayerState { get; set; }

    /// <summary>
    ///     Initialize the new instance of <see cref="KinoaPlayerStateService"/>.
    ///     Sets player state changed by operator handler.
    /// </summary>
    public KinoaPlayerStateService()
    {
        Kinoa.Player.SetStateChangedByOperatorHandler(OnPlayerStateChangedByOperator);
    }

    /// <summary>
    ///     Gets the actual Player State of an Active Player <see cref="Kinoa.Player.ID"/> asynchronously.
    ///     <remarks>
    ///     Gets the local (source of truth) ~or~ Kinoa server Player State if the local State is empty or unavailable.
    ///     </remarks>
    /// </summary>
    /// <returns>The actual Player State object.</returns>
    public async Task<CustomPlayerState> GetPlayerStateAsync()
    {
        var localPlayerState = await GetLocalPlayerStateAsync();
        if (localPlayerState != null)
        {
            Debug.Log("The local Player State will be used to open session.");
            PlayerState = localPlayerState;
        }
        else
        {
            var response = await GetServerPlayerStateAsync(KinoaPlayerAccountService.Instance.ActivePlayerID);
            var serverPlayerState = response.Data;
            if (response.IsSuccessful() && serverPlayerState != null)
            {
                if (!serverPlayerState.IsRegistered)
                {
                    //TODO: Populate the server Player State with the local data.
                }

                Debug.Log("The Kinoa server Player State will be used to open session.");
                PlayerState = serverPlayerState;
            }
            else if (response.IsConnectionError())
            {
                //TODO: Process the failed request e.g., show an connection error dialog to the client.
                Debug.Log("The Kinoa server Player State is not available: Connection error.");
            }
            else
            {
                //TODO: Process the failed request e.g., show an error message to the client.
                Debug.Log("The Kinoa server Player State is not available: Get Player State request is failed.");
            }
        }

        OnLocalPlayerStateSet?.Invoke(true);
        return PlayerState;
    }

    /// <summary>
    ///     Gets the local Player State asynchronously.
    /// </summary>
    /// <returns>The local Player State object.</returns>
    public async Task<CustomPlayerState> GetLocalPlayerStateAsync()
    {
        //TODO: Try to get the Player State from your local storage or your server because it's always actual instead of null.
        //TODO: Kinoa server Player State should be used as a backup source if the local Player State is empty or unavailable.
        return await Task.FromResult(null as CustomPlayerState);
    }

    /// <summary>
    ///     Gets the Kinoa server Player State by the Player ID asynchronously.
    /// </summary>
    /// <param name="playerId">The Player ID.</param>
    /// <returns>The server Player State object.</returns>
    public async Task<Response<CustomPlayerState>> GetServerPlayerStateAsync(string playerId)
    {
        var response = await Kinoa.Player.GetStateAsync<CustomPlayerState>(playerId);
        LogPlayerState(response);
        return response;
    }

    /// <summary>
    ///     Logs the Player State object in console.
    /// </summary>
    /// <param name="response">The Get Player State response.</param>
    private void LogPlayerState<TPlayerState>(Response<TPlayerState> response)
        where TPlayerState : PlayerState
    {
        Debug.Log($"The actual Player State received successfully: {response.IsSuccessful()}.\n" +
                  $"Player ID: \"{response.Data?.PlayerIdentifiers.PlayerId}\". " +
                  $"Native ID: \"{response.Data?.PlayerIdentifiers.NativeId}\"." +
                  $"Facebook ID: \"{response.Data?.PlayerIdentifiers.FacebookId}\"." +
                  $"Google ID: \"{response.Data?.PlayerIdentifiers.GoogleId}\"." +
                  $"Apple ID: \"{response.Data?.PlayerIdentifiers.AppleId}\"." +
                  $"Extra ID 1: \"{response.Data?.PlayerIdentifiers.ExtraId1}\"." +
                  $"Extra ID 2: \"{response.Data?.PlayerIdentifiers.ExtraId2}\"." +
                  $"Extra ID 3: \"{response.Data?.PlayerIdentifiers.ExtraId3}\".");

        if (!response.IsSuccessful()) return;
        if (response.Data != null && !response.Data.IsRegistered)
        {
            Debug.Log("Player is not registered in Kinoa and has empty Player State. " +
                      "Player will be created in Kinoa after first session start request with provided Player State.");
        }
    }

    /// <summary>
    ///     Resets the server Player State <seealso cref="Kinoa.Player.ResetState{TPlayerState}"/> to the passed PS object.
    /// </summary>
    public Task ResetPlayerStateAsync(CustomPlayerState playerState)
    {
        return Kinoa.Player.ResetState(playerState);
    }

    /// <summary>
    ///     Approves the Player State operator changes asynchronously. Unblocks the future Player State changes.
    /// </summary>
    public async Task<Response> ApprovePlayerStateChangesAsync()
    {
        var response = await Kinoa.Player.ApproveStateChangesAsync();
        Debug.Log($"The Player State operator changes are approved: {response.IsSuccessful()}.");

        return response;
    }

    /// <summary>
    ///     Triggered on changing the Player State by an operator.
    ///     You should approve the state changes <see cref="Kinoa.Player.ApproveStateChanges"/> before sending new Game Events. 
    /// </summary>
    /// <param name="changed">Is the player state changed.</param>
    private void OnPlayerStateChangedByOperator(bool changed)
    {
        var message = $"The Player State changed by an Operator: {changed}.";
        Debug.Log(message);
    }
}