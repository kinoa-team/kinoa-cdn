﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading;
using System.Threading.Tasks;
using Kinoa.Core.Callbacks;
using Kinoa.Data;
using Kinoa.Data.Enum;
using Kinoa.Data.FeaturesSettings;
using Kinoa.Data.FeaturesSettings.Enum;
using UnityEngine;

/// <summary>
///     Kinoa Features Settings service.
/// </summary>
public class KinoaFeaturesSettingsService : KinoaSingleton<KinoaFeaturesSettingsService>
{
    /// <summary>
    ///     The sample client-side Feature Settings collection.
    /// </summary>
    public List<FeatureSettingsResponse<FeatureSettingsData>> LocalFeatureSettings { get; private set; } =
        new List<FeatureSettingsResponse<FeatureSettingsData>>();

    /// <summary>
    ///     Gets the server checksums and downloads the Features Settings if the local checksums are different,
    ///     otherwise gets the cached or built-in Features Settings. Saves the downloaded Features Settings to cache.
    ///     <remarks>
    ///     The response contains the original server response information
    ///     and the Features Settings collection loaded from the server, cache, built-in, or combination of them.
    ///     </remarks>
    /// </summary>
    /// <param name="settingsRequestParams">The Features Settings request parameters.</param>
    /// <param name="cancellationToken">Cancellation token.</param>
    /// <param name="downloadProgressChangedCallback">Callback function that is invoked on download progress change.</param>
    /// <param name="configureChecksumLongPolling">
    ///     The value indicates whether to configure the provided Features Settings checksum long polling request.
    ///     <remarks>
    ///     Avoid setting to 'true' for a single Feature Settings request e.g. <see cref="OnFeaturesSettingsChecksumChanged"/>,
    ///     this feature is designed for multiple Features Settings long pooling in a single request.
    ///     </remarks>
    /// </param>
    /// <typeparam name="TData">
    ///     Represents the Feature Settings polymorphic data model type,
    ///     deserialized based on the "$type" TypeDiscriminator property,
    ///     where "$type" is the requested Feature Settings key <see cref="FeatureSettingsSmartDownloadRequestParams.Key"/>.
    ///<remarks>
    ///     Apply the <see cref="JsonPolymorphicAttribute"/> for the provided <see cref="TData"/> type
    ///     to enable the polymorphic deserialization.
    /// </remarks>
    /// </typeparam>
    /// <returns>The requested Features Settings type of <see cref="TData"/>.</returns>
    public async Task<Response<FeaturesSettingsResponse<FeatureSettingsData>>> SmartDownloadAsync<TData>(
        List<FeatureSettingsSmartDownloadRequestParams> settingsRequestParams, bool configureChecksumLongPolling,
        CancellationToken cancellationToken = default, ProgressChangedCallback downloadProgressChangedCallback = null)
    {
        if (settingsRequestParams == null)
        {
            settingsRequestParams = new List<FeatureSettingsSmartDownloadRequestParams>
            {
                new FeatureSettingsSmartDownloadRequestParams(key: "DailyBonus", version: 1, getDefault: false,
                    FeatureSettingsFailedDownloadStrategy.GetCachedOrBuiltIn, compressData: false),
                new FeatureSettingsSmartDownloadRequestParams(key: "WheelOfFortune", version: 1, getDefault: false,
                    FeatureSettingsFailedDownloadStrategy.GetCachedOrBuiltIn, compressData: false)
            };
        }

        if (cancellationToken == default)
        {
            var cancellationTokenSource = new CancellationTokenSource();
            cancellationTokenSource.CancelAfter(10 * 1000);
            cancellationToken = cancellationTokenSource.Token;
        }

        var response = await Kinoa.FeaturesSettings.SmartDownloadAsync<FeatureSettingsData>(
            settingsRequestParams, cancellationToken, downloadProgressChangedCallback ?? OnDownloadProgressChanged);

        if (response.IsSuccessful())
        {
            LogSettingsData(response.Data?.Settings);
            ReplaceFeatureSettings(response.Data?.Settings);
            if (configureChecksumLongPolling)
            {
                ConfigureChecksumLongPolling(response.Data?.Settings);
            }
        }
        else if (response.IsConnectionError())
        {
            //TODO: Connection error handling.
        }
        else if (response.IsResponseFailed())
        {
            //TODO: Bad response handling.
        }
        else if (response.IsResponseCanceled())
        {
            //TODO: Request cancellation handling.
        }

        return response;
    }

    /// <summary>
    ///     Downloads Features Settings by the provided request parameters asynchronously.
    /// </summary>
    /// <param name="settingsRequestParams">Features Settings request parameters.</param>
    /// <param name="cancellationToken">Cancellation token.</param>
    /// <param name="downloadProgressChangedCallback">Callback function that is invoked on download progress change.</param>
    /// <param name="configureChecksumLongPolling">
    ///     The value indicates whether to configure the provided Features Settings checksum long polling request.
    ///     <remarks>
    ///     Avoid setting to 'true' for a single Feature Settings request e.g. <see cref="OnFeaturesSettingsChecksumChanged"/>,
    ///     this feature is designed for multiple Features Settings long pooling in a single request.
    ///     </remarks>
    /// </param>
    /// <typeparam name="TData">
    ///     Represents the Feature Settings polymorphic data model type,
    ///     deserialized based on the "$type" TypeDiscriminator property,
    ///     where "$type" is the requested Feature Settings key <see cref="FeatureSettingsRequestParams.Key"/>.
    ///<remarks>
    ///     Apply the <see cref="JsonPolymorphicAttribute"/> for the provided <see cref="TData"/> type
    ///     to enable the polymorphic deserialization.
    /// </remarks>
    /// <remarks>
    ///     In this example, <see cref="FeatureSettingsData"/> is the base polymorphic type for all Feature Settings data models.
    ///     You can change the type, by providing the <see cref="TData"/> instead of hardcoded <see cref="FeatureSettingsData"/>.
    /// </remarks>
    /// </typeparam>
    /// <returns>The requested Features Settings type of <see cref="TData"/>.</returns>
    public async Task<Response<FeaturesSettingsResponse<FeatureSettingsData>>> DownloadAsync<TData>(
        List<FeatureSettingsDownloadRequestParams> settingsRequestParams, bool configureChecksumLongPolling,
        CancellationToken cancellationToken = default, ProgressChangedCallback downloadProgressChangedCallback = null)
    {
        if (settingsRequestParams == null)
        {
            settingsRequestParams = new List<FeatureSettingsDownloadRequestParams>
            {
                new FeatureSettingsDownloadRequestParams(key: "DailyBonus", version: 1, getDefault: false,
                    compressData: false),
                new FeatureSettingsDownloadRequestParams(key: "WheelOfFortune", version: 1, getDefault: false,
                    compressData: false)
            };
        }

        if (cancellationToken == default)
        {
            var cancellationTokenSource = new CancellationTokenSource();
            cancellationTokenSource.CancelAfter(10 * 1000);
            cancellationToken = cancellationTokenSource.Token;
        }

        var response = await Kinoa.FeaturesSettings.DownloadAsync<FeatureSettingsData>(
            settingsRequestParams, cancellationToken, downloadProgressChangedCallback ?? OnDownloadProgressChanged);

        if (response.IsSuccessful())
        {
            LogSettingsData(response.Data.Settings);
            ReplaceFeatureSettings(response.Data.Settings);
            if (configureChecksumLongPolling)
            {
                ConfigureChecksumLongPolling(response.Data?.Settings);
            }
        }
        else if (response.IsConnectionError())
        {
            //TODO: Connection error handling.
        }
        else if (response.IsResponseFailed())
        {
            //TODO: Bad response handling.
        }
        else if (response.IsResponseCanceled())
        {
            //TODO: Request cancellation handling.
        }

        return response;
    }

    /// <summary>
    ///     On Features Settings download progress changed.
    /// </summary>
    /// <param name="progress">Content download progress.</param>
    private static void OnDownloadProgressChanged(decimal progress)
    {
        Debug.Log($"Features Settings download progress changed: {progress}");
    }

    /// <summary>
    ///     Gets the Features Settings Checksums by the provided request parameters asynchronously.
    /// </summary>
    /// <param name="settingsRequestParams">Features Settings request parameters.</param>
    /// <returns>The response to the requested Features Settings containing all information except the Data.</returns>
    public async Task<Response<FeaturesSettingsResponse>> GetChecksumsAsync(
        List<FeatureSettingsDownloadRequestParams> settingsRequestParams)
    {
        if (settingsRequestParams == null)
        {
            settingsRequestParams = new List<FeatureSettingsDownloadRequestParams>
            {
                new FeatureSettingsDownloadRequestParams(key: "DailyBonus", version: 1, getDefault: false),
                new FeatureSettingsDownloadRequestParams(key: "WheelOfFortune", version: 1, getDefault: false)
            };
        }

        var response = await Kinoa.FeaturesSettings.GetChecksumsAsync(settingsRequestParams);
        if (response.IsSuccessful())
        {
            LogSettings(response.Data.Settings);
        }
        else if (response.IsConnectionError())
        {
            //TODO: Connection error handling.
        }
        else if (response.IsResponseFailed())
        {
            //TODO: Bad response handling.
        }
        else if (response.IsResponseCanceled())
        {
            //TODO: Request cancellation handling.
        }

        return response;
    }

    /// <summary>
    ///     Sample method to track the checksum changes of the downloaded Features Settings
    ///     in a single long pooling request.
    /// </summary>
    /// <param name="featuresSettingsToTrack">The Features Settings collection to track.</param>
    private async void ConfigureChecksumLongPolling(IEnumerable<FeatureSettingsResponse> featuresSettingsToTrack)
    {
        var settingsList = featuresSettingsToTrack?.ToList();
        if (settingsList == null || !settingsList.Any()) return;

        // Create a list of the Features Settings to track.
        var trackList = settingsList
            .Where(x => x.Status == FeatureSettingsResponseStatus.Ok)
            .ToList();

        if (!trackList.Any()) return;

        // Configure the multiple Features Settings checksum long polling in a single long pooling request.
        var cts = new CancellationTokenSource();
        await ConfigureChecksumLongPollingAsync(trackList,
            (checksumChangedFs, requestedFs) =>
                OnFeaturesSettingsChecksumChanged(checksumChangedFs, requestedFs, cts), cts);
    }

    /// <summary>
    ///     Configures the multiple Features Settings checksum long polling asynchronously.
    ///     <remarks>
    ///     The long polling mechanism retrieves the checksum of the provided Features Settings collection
    ///     <see cref="featuresSettingsToTrack"/> every n seconds (tickTimeoutMs) in a single request.
    /// 
    ///     The callback <see cref="appCallback"/> is invoked when the checksum of any of the provided Features Settings changes.
    ///     The long polling is only disposed when the cancellation token (cts) is cancelled by a client.
    ///
    ///     Avoid using the same Features Settings in multiple long pooling requests to prevent redundant Kinoa API calls.
    ///     </remarks>
    /// </summary>
    /// <param name="featuresSettingsToTrack">Features Settings to track.</param>
    /// <param name="appCallback">Callback method for asynchronous handling of the checksum change.</param>
    /// <param name="cts">The long polling cancellation token source.</param>
    /// <returns>
    ///     Response with status 'Success' if the checksum tracker is created and ran successfully,
    ///     else - the Response with status 'Failure'.
    /// </returns>
    private async Task<Response> ConfigureChecksumLongPollingAsync(
        List<FeatureSettingsResponse> featuresSettingsToTrack,
        FeaturesSettingsChecksumChangedCallback appCallback,
        CancellationTokenSource cts)
    {
        // Repeat timeout between the checksum track calls in milliseconds.
        const int tickTimeoutMs = 30 * 1000;
        // Cancel the checksums long polling automatically after 90 seconds.
        cts.CancelAfter(90 * 1000);

        var response = await Kinoa.FeaturesSettings.ConfigureChecksumLongPollingAsync(
            featuresSettingsToTrack, appCallback, cts.Token, tickTimeoutMs);

        var keys = string.Join(", ", featuresSettingsToTrack.Select(x => x.Request.Key));
        Debug.Log(response.IsSuccessful()
            ? $"The '{keys}' checksum long pooling request is configured successfully."
            : $"Failed to configure the '{keys}' checksum long pooling request.");

        return response;
    }

    /// <summary>
    ///     Callback method for handling the server checksum change of an multiple Features Settings
    ///     in context of single long pooling request.
    /// </summary>
    /// <param name="checksumChangedFeaturesSettings">
    ///     The collection of Features Settings with changed checksum.
    ///     <remarks>The configuration data is not included in the collection.</remarks>
    /// </param>
    /// <param name="requestedFeaturesSettings">
    ///     The full collection of requested Features Settings with the updated checksums.
    ///     <remarks>The configuration data is not included in the collection.</remarks>
    /// </param>
    /// <param name="cts">
    ///     The long polling cancellation token source.
    /// </param>
    private async void OnFeaturesSettingsChecksumChanged(
        List<FeatureSettingsResponse> checksumChangedFeaturesSettings,
        List<FeatureSettingsResponse> requestedFeaturesSettings,
        CancellationTokenSource cts)
    {
        checksumChangedFeaturesSettings.ForEach(x =>
            Debug.Log($"The server checksum of the '{x.Request.Key}' FS is changed. " +
                      $"New checksum: {x.Checksum}."));

        var settingsRequestParams = checksumChangedFeaturesSettings
            .Select(x =>
                new FeatureSettingsDownloadRequestParams(key: x.Request.Key,
                    version: Convert.ToUInt16(x.Request.Version),
                    getDefault: false,
                    compressData: x.Request.CompressData))
            .ToList();

        //Download the updated Features Settings with the changed checksum.
        var response = await DownloadAsync<FeatureSettingsData>(
            settingsRequestParams, configureChecksumLongPolling: false);
        if (response.IsSuccessful())
        {
            // Cancel the old checksums long polling. See also ConfigureChecksumLongPollingAsync automatic cancellation.
            cts.Cancel();
            Debug.Log("The Features Settings old checksums long polling is stopped: " +
                      "The updated Features Settings with the changed checksum are downloaded successfully.");

            // Configure the new checksums long polling.
            ConfigureChecksumLongPolling(requestedFeaturesSettings);
        }
        else
        {
            // Continue the old checksums long polling.
            Debug.Log("The Features Settings old checksums long polling is continue: " +
                      "Failed to download the updated Features Settings with the changed checksum.");
        }
    }

    /// <summary>
    ///     Gets Built-in Features Settings by the provided request parameters asynchronously.
    /// </summary>
    /// <param name="settingsRequestParams">Features Settings request parameters.</param>
    /// <param name="configureChecksumLongPolling">
    ///     The value indicates whether to configure the provided Features Settings checksum long polling request.
    ///     <remarks>
    ///     Avoid setting to 'true' for a single Feature Settings request e.g. <see cref="OnFeaturesSettingsChecksumChanged"/>,
    ///     this feature is designed for multiple Features Settings long pooling in a single request.
    ///     </remarks>
    /// </param>
    /// <typeparam name="TData">
    ///     Represents the Feature Settings polymorphic data model type,
    ///     deserialized based on the "$type" TypeDiscriminator property,
    ///     where "$type" is the requested Feature Settings key <see cref="FeatureSettingsRequestParams.Key"/>.
    ///<remarks>
    ///     Apply the <see cref="JsonPolymorphicAttribute"/> for the provided <see cref="TData"/> type
    ///     to enable the polymorphic deserialization.
    /// </remarks>
    /// </typeparam>
    /// <returns>The requested Features Settings type of <see cref="TData"/>.</returns>
    public async Task<Response<FeaturesSettingsResponse<FeatureSettingsData>>> GetBuiltInAsync<TData>(
        List<FeatureSettingsRequestParams> settingsRequestParams, bool configureChecksumLongPolling)
    {
        if (settingsRequestParams == null)
        {
            settingsRequestParams = new List<FeatureSettingsRequestParams>
            {
                new FeatureSettingsRequestParams(key: "DailyBonus", version: 1),
                new FeatureSettingsRequestParams(key: "WheelOfFortune", version: 1)
            };
        }

        var response = await Kinoa.FeaturesSettings.GetBuiltInAsync<FeatureSettingsData>(settingsRequestParams);
        if (response.IsSuccessful())
        {
            LogSettingsData(response.Data.Settings);
            ReplaceFeatureSettings(response.Data.Settings);
            if (configureChecksumLongPolling)
            {
                ConfigureChecksumLongPolling(response.Data?.Settings);
            }
        }
        else if (response.IsResponseFailed())
        {
            //TODO: Bad response handling.
        }
        else if (response.IsResponseCanceled())
        {
            //TODO: Request cancellation handling.
        }

        return response;
    }

    /// <summary>
    ///     Gets Cached Features Settings by the provided request parameters asynchronously.
    /// </summary>
    /// <param name="settingsRequestParams">Features Settings request parameters.</param>
    /// <param name="configureChecksumLongPolling">
    ///     The value indicates whether to configure the provided Features Settings checksum long polling request.
    ///     <remarks>
    ///     Avoid setting to 'true' for a single Feature Settings request e.g. <see cref="OnFeaturesSettingsChecksumChanged"/>,
    ///     this feature is designed for multiple Features Settings long pooling in a single request.
    ///     </remarks>
    /// </param>
    /// <typeparam name="TData">
    ///     Represents the Feature Settings polymorphic data model type,
    ///     deserialized based on the "$type" TypeDiscriminator property,
    ///     where "$type" is the requested Feature Settings key <see cref="FeatureSettingsDownloadRequestParams.Key"/>.
    ///<remarks>
    ///     Apply the <see cref="JsonPolymorphicAttribute"/> for the provided <see cref="TData"/> type
    ///     to enable the polymorphic deserialization.
    /// </remarks>
    /// </typeparam>
    /// <returns>The requested Features Settings type of <see cref="TData"/>.</returns>
    public async Task<Response<FeaturesSettingsResponse<FeatureSettingsData>>> GetCachedAsync<TData>(
        List<FeatureSettingsRequestParams> settingsRequestParams, bool configureChecksumLongPolling)
    {
        if (settingsRequestParams == null)
        {
            settingsRequestParams = new List<FeatureSettingsRequestParams>
            {
                new FeatureSettingsRequestParams(key: "DailyBonus", version: 1),
                new FeatureSettingsRequestParams(key: "WheelOfFortune", version: 1)
            };
        }

        var response = await Kinoa.FeaturesSettings.GetCachedAsync<FeatureSettingsData>(settingsRequestParams);
        if (response.IsSuccessful())
        {
            LogSettingsData(response.Data.Settings);
            ReplaceFeatureSettings(response.Data.Settings);
            if (configureChecksumLongPolling)
            {
                ConfigureChecksumLongPolling(response.Data?.Settings);
            }
        }
        else if (response.IsResponseFailed())
        {
            //TODO: Bad response handling.
        }
        else if (response.IsResponseCanceled())
        {
            //TODO: Request cancellation handling.
        }

        return response;
    }

    /// <summary>
    ///     Gets information about the available Features Settings by the requested Schema key.
    /// </summary>
    /// <param name="schemaKey">The Schema key/name.</param>
    /// <returns>The available Features Settings information.</returns>
    public async Task<Response<SchemaFeaturesSettingsInfo>> GetSchemaFeaturesSettingsInfoAsync(string schemaKey)
    {
        var response = await Kinoa.FeaturesSettings.GetInfoAsync(schemaKey);
        var log = new StringBuilder();
        if (response.IsSuccessful() && response.Data != null)
        {
            log.AppendFormat($"Feature Schema ID:{response.Data.Id}, Name:{response.Data.Name}.");
            if (response.Data.Settings.Any())
            {
                log.AppendFormat("\nAvailable Features Settings:");
                foreach (var featureSettings in response.Data.Settings)
                {
                    var schemaVersions = featureSettings.Versions.Any()
                        ? string.Join(", ", featureSettings.Versions)
                        : string.Empty;

                    log.AppendFormat($"\nId: {featureSettings.Id}. " +
                                     $"Key: {featureSettings.Key}. " +
                                     $"Name: {featureSettings.Name}. " +
                                     $"Versions: {schemaVersions}.");
                }
            }
            else log.AppendFormat("\nNo Features Settings available.");
        }
        else
        {
            log.AppendFormat($"{nameof(Kinoa.FeaturesSettings.GetInfoAsync)} is failed:" +
                             $"\nStatus: {response.Status.ToString()}" +
                             $"\nMessage: {response.Error?.Message}" +
                             $"\nCode: {response.Error?.Code}");
        }

        Log(log);
        return response;
    }

    /// <summary>
    ///     Replaces the downloaded Features Settings in the sample local collection <see cref="LocalFeatureSettings"/>.
    /// </summary>
    /// <param name="settings">The new Features Settings collection.</param>
    private void ReplaceFeatureSettings(List<FeatureSettingsResponse<FeatureSettingsData>> settings)
    {
        if (settings == null || !settings.Any())
        {
            return;
        }

        var okSettings = settings.Where(x => x.Status == FeatureSettingsResponseStatus.Ok);
        LocalFeatureSettings.RemoveAll(x => okSettings.Any(y => y.Request.IsKeyVersionEqualTo(x.Request)));
        LocalFeatureSettings.AddRange(okSettings);

        var info = string.Join("\n", LocalFeatureSettings.Select(x =>
            $"Key: {x.Request.Key}, Version: {x.Request.Version}, Checksum: {x.Checksum}"));
        Debug.Log($"The local Features Settings collection is updated: \n {info}");
    }

    /// <summary>
    ///     Logs the requested Features Settings information except the Data.
    /// </summary>
    /// <param name="settings">The Feature Settings collection.</param>
    private void LogSettings(IEnumerable<FeatureSettingsResponse> settings)
    {
        foreach (var setting in settings)
        {
            var briefLog = new StringBuilder();
            var scheduledStartTime = setting.StartTime > 0
                ? DateTimeOffset.FromUnixTimeMilliseconds(setting.StartTime.Value).DateTime
                    .ToString("yyyy-MM-dd HH:mm:ss")
                : string.Empty;
            var scheduledEndTime = setting.EndTime > 0
                ? DateTimeOffset.FromUnixTimeMilliseconds(setting.EndTime.Value).DateTime
                    .ToString("yyyy-MM-dd HH:mm:ss")
                : string.Empty;
            var audiences = string.Join(", ", setting.Audiences?
                .Where(x => x.Value == true)
                .Select(x => x.Key) ?? Array.Empty<string>());
            var userLists = string.Join(", ", setting.UserLists?
                .Where(x => x.Value == true)
                .Select(x => x.Key) ?? Array.Empty<string>());

            briefLog.AppendFormat("The requested Features Settings were received:");
            briefLog.AppendFormat($"\nStatus: {setting.Status.ToString()}. " +
                                  $"Source: {setting.Source.ToString()}. " +
                                  $"Key: {setting.Request.Key}. " +
                                  $"Version: {setting.Request.Version}. " +
                                  $"Configuration: {setting.ConfigurationName}. " +
                                  $"Checksum: {setting.Checksum}. " +
                                  $"Start Time: {scheduledStartTime}. " +
                                  $"End Time: {scheduledEndTime}. " +
                                  $"Audiences: {audiences}. " +
                                  $"User Lists: {userLists}. ");
            Log(briefLog);
        }
    }

    /// <summary>
    ///     Logs the requested Features Settings information including the Data.
    /// </summary>
    /// <param name="settings">The Feature Settings collection.</param>
    private void LogSettingsData(List<FeatureSettingsResponse<FeatureSettingsData>> settings)
    {
        LogSettings(settings);

        var dailyBonusSettings =
            settings.FirstOrDefault(x => x.Request.Key == "DailyBonus");
        if (dailyBonusSettings != null && dailyBonusSettings.Status == FeatureSettingsResponseStatus.Ok &&
            dailyBonusSettings.Data.Any())
        {
            var log = new StringBuilder();
            log.AppendFormat("\tDaily Bonus Settings:");
            if (dailyBonusSettings.Filters != null)
            {
                foreach (var filter in dailyBonusSettings.Filters)
                {
                    log.AppendFormat($"\nFilter: {filter.Key} - {filter.Value}");
                }
            }

            foreach (var data in dailyBonusSettings.Data)
            {
                log.AppendFormat($"\nCoins: {((DailyBonusSettings) data).Coins}");
            }

            Log(log);
        }

        var wheelOfFortuneSettings =
            settings.FirstOrDefault(x => x.Request.Key == "WheelOfFortune");
        if (wheelOfFortuneSettings != null && wheelOfFortuneSettings.Status == FeatureSettingsResponseStatus.Ok &&
            wheelOfFortuneSettings.Data.Any())
        {
            var log = new StringBuilder();
            log.AppendFormat("\tWheel of Fortune Settings:");
            if (wheelOfFortuneSettings.Filters != null)
            {
                foreach (var filter in wheelOfFortuneSettings.Filters)
                {
                    log.AppendFormat($"\nFilter: {filter.Key} - {filter.Value}");
                }
            }

            foreach (var data in wheelOfFortuneSettings.Data)
            {
                var wheelOfFortuneData = (WheelOfFortuneSettings) data;
                log.AppendFormat($"\nPrize: {wheelOfFortuneData.Prize}");
                log.AppendFormat($"\nCoins: {wheelOfFortuneData.Coins}");

                var bundleKey = wheelOfFortuneData.FooBundleKey;
                log.AppendFormat($"\nFooBundle Key: {bundleKey}");

                var bundleResources = wheelOfFortuneSettings.BundleResources?
                    .FirstOrDefault(x => x.Key == bundleKey).Value;

                if (bundleResources == null) continue;

                log.AppendFormat("\nFooBundle Resources:");
                foreach (var resource in bundleResources)
                {
                    log.AppendFormat($"\nResource: {resource.ResourceKey} - {resource.Amount}");
                }
            }

            Log(log);
        }
    }

    /// <summary>
    ///     Logs the provided message.
    /// </summary>
    /// <param name="builder">String builder.</param>
    private void Log(StringBuilder builder)
    {
        var log = builder.ToString();
        Debug.Log(log);
        builder.Clear();
    }
}