#if UNITY_ANDROID
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Kinoa.PushNotifications.Android.Clients;
using Kinoa.PushNotifications.Android.Data;
using Kinoa.PushNotifications.Android.Logic.Utils;
using Unity.Notifications.Android;
using UnityEngine;

/// <summary>
///     Kinoa Android local push notification sample.
/// </summary>
public class KinoaAndroidLocalPushNotificationProvider
{
    private KinoaAndroidLocalNotificationClient _localNotificationClient;

    public async Task Initialize()
    {
        _localNotificationClient = await KinoaAndroidLocalNotificationClient.GetInstance();
        _localNotificationClient.OnLocalPushNotificationReceived += OnLocalPushNotificationReceived;
    }

    /// <summary>
    ///     Sample shows how to handle notification received.
    /// </summary>
    private void OnLocalPushNotificationReceived(KinoaAndroidNotificationWrapper notification)
    {
        Debug.Log($"[GAME] Local push notification received.");
    }

    /// <summary>
    ///     Sample shows how to schedule notification.
    /// </summary>
    public void ScheduleNotification(KinoaAndroidLocalNotificationData localNotificationData)
    {
        _localNotificationClient?.ScheduleLocalNotification(localNotificationData);
    }

    /// <summary>
    ///     Sample shows how to cancel notification.
    /// </summary>
    public void CancelNotification(int notificationId)
    {
        _localNotificationClient?.CancelLocalNotification(notificationId);
    }

    /// <summary>
    ///     Sample shows how to get notifications that are currently scheduled.
    /// </summary>
    public List<KinoaAndroidScheduledLocalNotificationData> GetScheduledNotifications()
    {
        var scheduledNotifications = _localNotificationClient?.GetScheduledNotifications();
        return scheduledNotifications ?? new List<KinoaAndroidScheduledLocalNotificationData>();
    }

    /// <summary>
    ///     Sample shows how to reschedule notification.
    /// </summary>
    public void RescheduleNotification(int notificationId, KinoaAndroidLocalNotificationData localNotificationData)
    {
        _localNotificationClient?.RescheduleLocalNotification(notificationId, localNotificationData);
    }

    /// <summary>
    ///     Sample shows how to create custom notification channel.
    /// </summary>
    private string CreateCustomNotificationChannel()
    {
        var androidNotificationChannel = new AndroidNotificationChannel(
            id: "CustomChannel",
            name: "CustomChannelName",
            description: "CustomChannelDescription",
            importance: Importance.High);
        return KinoaAndroidNotificationChannelIdProvider.CreateAndProvide(androidNotificationChannel);
    }

    /// <summary>
    ///     Sample shows how to create interval notification.
    /// </summary>
    public KinoaAndroidLocalNotificationData GetIntervalLocalNotification(
        TimeSpan timeSpan,
        bool repeatable,
        bool rescheduled = false)
    {
        return new KinoaAndroidLocalNotificationData("intervalPushSample")
            .SetTitle((rescheduled ? "Rescheduled " : "") + "Local push notification title")
            .SetBody("Local push notification text")
            .SetTimeIntervalTrigger(timeSpan, repeatable);
    }

    /// <summary>
    ///     Sample shows how to create calendar notification.
    /// </summary>
    public KinoaAndroidLocalNotificationData GetCalendarLocalNotification(DateTime dateTime)
    {
        return new KinoaAndroidLocalNotificationData("calendarPushSample")
            .SetTitle("Local push notification title")
            .SetBody("Local push notification text")
            .SetTimeCalendarTrigger(dateTime);
    }
}
#endif