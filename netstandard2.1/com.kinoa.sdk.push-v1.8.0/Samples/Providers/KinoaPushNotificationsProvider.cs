﻿#if UNITY_ANDROID || UNITY_IOS
using UnityEngine;

/// <summary>
///     Kinoa Push Notifications SDK basic methods and properties provider sample.
/// </summary>
public static class KinoaPushNotificationsProvider
{
    /// <summary>
    ///     Block player pushes.
    /// </summary>
    public static async void BlockPushes()
    {
        var response = await Kinoa.PushNotifications.SDK.BlockPushes();
        Debug.Log($"Kinoa pushes block request completed with status: {response.Status}");
    }
    
    /// <summary>
    ///     Unblock player pushes.
    /// </summary>
    public static async void UnblockPushes()
    {
        var response = await Kinoa.PushNotifications.SDK.UnblockPushes();
        Debug.Log($"Kinoa pushes unblock request completed with status: {response.Status}");
    }
    
    /// <summary>
    ///     Delete device push token.
    /// </summary>
    public static async void DeleteToken()
    {
        var token = "FCM or APN device token";
        var response = await Kinoa.PushNotifications.SDK.DeleteToken(token);
        Debug.Log($"Kinoa delete token completed with status: {response.Status}");
    }
    
    /// <summary>
    ///     Check pushes status on Kinoa.
    /// </summary>
    public static async void CheckPushStatus()
    {
        var response = await Kinoa.PushNotifications.SDK.GetPushStatus();
        if (response.IsSuccessful())
        {
            Debug.Log($"Kinoa pushes are blocked: {response.Data.Blocked}");
        }
        else
        {
            Debug.Log($"Get push status request status: {response.Status}.");
        }
    }
}
#endif